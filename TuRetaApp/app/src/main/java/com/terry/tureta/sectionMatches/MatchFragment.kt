package com.terry.tureta.sectionMatches

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.squareup.picasso.Picasso
import com.terry.tureta.R
import com.terry.tureta.customViews.CustomFragment
import com.terry.tureta.databinding.FragmentMatchBinding
import com.terry.tureta.dialogs.CustomDatePicker
import com.terry.tureta.helpers.CircleTransform
import com.terry.tureta.mediumMargin
import com.terry.tureta.sectionTeams.TeamsListFragment
import com.terry.tureta.sectionTournaments.viewModel.TournamentViewModel
import com.terry.tureta.smallMargin
import grid.apps.supplies_admin.Extensions.mediumRegular
import grid.apps.supplies_admin.Extensions.setMargins
import grid.apps.supplies_admin.Extensions.smallRegular
import java.util.*

class MatchFragment : CustomFragment() {

    private lateinit var binding: FragmentMatchBinding

    private val mViewModel by lazy {
        ViewModelProvider(requireActivity())[TournamentViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMatchBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setOnClickListeners()

        binding.buttonDate.setMargins(0, mediumMargin)
        binding.buttonHour.setMargins(0, smallMargin)
        binding.buttonField.setMargins(0, smallMargin)

        binding.imageTeam1.setMargins(0, mediumMargin)
        binding.imageTeam2.setMargins(0, mediumMargin)

        binding.buttonFinish.setMargins(mediumMargin, 0, mediumMargin, mediumMargin)

        binding.textTeam1.setMargins(0, smallMargin)
        binding.textTeam2.setMargins(0, smallMargin)
        binding.textScore1.setMargins(0, smallMargin)
        binding.textScore2.setMargins(0, smallMargin)

        binding.textTeam1.smallRegular()
        binding.textTeam2.smallRegular()

        binding.textScore1.mediumRegular()
        binding.textScore2.mediumRegular()
    }

    override fun onResume() {
        super.onResume()

        mViewModel.addObserver(this)

        binding.buttonField.updateText(mViewModel.getFieldSelectedString(requireContext()))

        if (mViewModel.getFirstTeam() != null) {
            Picasso.get()
                .load(mViewModel.getFirstTeam()!!.image + "?w=150")
                .transform(CircleTransform(requireContext(), true, true))
                .into(binding.imageTeam1)

            binding.textTeam1.text = mViewModel.getFirstTeam()!!.name
        }

        if (mViewModel.getSecondTeam() != null) {
            Picasso.get()
                .load(mViewModel.getSecondTeam()!!.image + "?w=150")
                .transform(CircleTransform(requireContext(), true, true))
                .into(binding.imageTeam2)

            binding.textTeam2.text = mViewModel.getSecondTeam()!!.name
        }

        binding.buttonHour.updateText(mViewModel.getHourSelectedString(requireContext()))

        binding.buttonDate.updateText(mViewModel.getDateSelectedString(requireContext()))

        if (mViewModel.getMatchSelected() != null) {
            binding.buttonFinish.updateText(R.string.label_update)
        } else {
            binding.buttonLess1.visibility = View.GONE
            binding.textScore1.visibility = View.GONE
            binding.buttonMore1.visibility = View.GONE
            binding.buttonLess2.visibility = View.GONE
            binding.textScore2.visibility = View.GONE
            binding.buttonMore2.visibility = View.GONE
        }
    }

    override fun onPause() {
        super.onPause()

        mViewModel.deleteObserver(this)
    }

    private fun setOnClickListeners() {
        binding.buttonDate.setOnClickListener(this)
        binding.buttonField.setOnClickListener(this)
        binding.buttonHour.setOnClickListener(this)
        binding.buttonMore1.setOnClickListener(this)
        binding.buttonLess1.setOnClickListener(this)
        binding.buttonMore2.setOnClickListener(this)
        binding.buttonLess2.setOnClickListener(this)
        binding.buttonFinish.setOnClickListener(this)
        binding.textTeam1.setOnClickListener(this)
        binding.textTeam2.setOnClickListener(this)
        binding.imageTeam1.setOnClickListener(this)
        binding.imageTeam2.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        v ?: return
        when (v) {
            binding.buttonDate -> {
                val action =
                    MatchFragmentDirections.actionMatchFragmentToCustomDatePicker(CustomDatePicker.MODE_SELECT_DATE)
                findNavController().navigate(action)
            }
            binding.buttonField -> findNavController().navigate(R.id.action_matchFragment_to_selectFieldFragment)
            binding.buttonHour -> findNavController().navigate(R.id.action_matchFragment_to_timePickerDialog)
            binding.buttonMore1 -> addScore(binding.textScore1)
            binding.buttonLess1 -> removeScore(binding.textScore1)
            binding.buttonMore2 -> addScore(binding.textScore2)
            binding.buttonLess2 -> removeScore(binding.textScore2)
            binding.textTeam1 -> goToSelectTeam(0)
            binding.textTeam2 -> goToSelectTeam(1)
            binding.imageTeam1 -> goToSelectTeam(0)
            binding.imageTeam2 -> goToSelectTeam(1)
            binding.buttonFinish -> {
                if (validateData()) {
                    val action = MatchFragmentDirections.actionMatchFragmentToLoadDialog()
                    action.message =
                        if (mViewModel.getMatchSelected() != null) getString(R.string.label_updating_match)
                        else getString(R.string.label_creating_match)
                    findNavController().navigate(action)

                    if (mViewModel.getMatchSelected() == null) {
                        mViewModel.createMatch(
                            binding.textScore1.text.toString().toInt(),
                            binding.textScore2.text.toString().toInt()
                        )
                    } else {
                        mViewModel.updateMatch(
                            binding.textScore1.text.toString().toInt(),
                            binding.textScore2.text.toString().toInt()
                        )
                    }
                }
            }
        }
    }

    private fun goToSelectTeam(teamNumber: Int) {
        val action =
            MatchFragmentDirections.actionMatchFragmentToTeamsListFragment(TeamsListFragment.MODE_SINGLE_SELECTION)
        action.teamNumber = teamNumber
        findNavController().navigate(action)
    }

    private fun addScore(textView: AppCompatTextView) {
        val num = textView.text.toString().toInt()
        textView.text = (num + 1).toString()
    }

    private fun removeScore(textView: AppCompatTextView) {
        val num = textView.text.toString().toInt()
        if (num > 0) {
            textView.text = (num - 1).toString()
        }
    }

    override fun onDateUpdated(date: Calendar) {
        binding.buttonDate.updateText(mViewModel.getDateSelectedString(requireContext()))
    }

    private fun validateData(): Boolean {
        if (mViewModel.getDateSelected() == null) {
            showToast(R.string.message_match_date_empty)
            return false
        }

        if (mViewModel.hourSelected.isEmpty()) {
            showToast(R.string.message_match_hour_empty)
            return false
        }

        if (mViewModel.fieldSelected == null) {
            showToast(R.string.message_match_field_empty)
            return false
        }

        if (mViewModel.getFirstTeam() == null || mViewModel.getSecondTeam() == null) {
            showToast(R.string.message_match_teams_empty)
            return false
        } else if (mViewModel.getFirstTeam()!!.id == mViewModel.getSecondTeam()!!.id) {
            showToast(R.string.message_match_teams_error)
            return false
        }

        return true
    }

}