package com.terry.tureta.sectionReservations

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.terry.tureta.extensions.setStyle

import com.terry.tureta.databinding.FragmentReservationsBinding
import com.terry.tureta.sectionReservations.adapters.ReservationFragmentsAdapter

class ReservationsFragment : Fragment() {

    private lateinit var binding: FragmentReservationsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentReservationsBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.viewPager.adapter = ReservationFragmentsAdapter(requireContext(), parentFragmentManager)
        binding.tabLayout.setupWithViewPager(binding.viewPager)
        binding.tabLayout.setStyle()
    }

}
