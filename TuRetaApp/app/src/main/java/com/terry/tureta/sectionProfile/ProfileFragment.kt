package com.terry.tureta.sectionProfile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController

import com.terry.tureta.R
import com.terry.tureta.api.ApiUtils
import com.terry.tureta.customViews.CustomFragment
import com.terry.tureta.databinding.FragmentProfileBinding
import com.terry.tureta.dialogs.CustomBottomDialog
import com.terry.tureta.mediumMargin
import com.terry.tureta.sectionMain.viewModel.MainViewModel
import grid.apps.supplies_admin.Extensions.largeBold
import grid.apps.supplies_admin.Extensions.largeTitleStyle
import grid.apps.supplies_admin.Extensions.setMargins
import grid.apps.supplies_admin.Extensions.smallRegular
import java.lang.Exception

class ProfileFragment : CustomFragment() {

    private lateinit var binding: FragmentProfileBinding

    private val mViewModel by lazy {
        activity?.run {
            val model = ViewModelProvider(this)[MainViewModel::class.java]
            model.setActivity(this)
            model
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.imageField.setMargins(mediumMargin, 0, mediumMargin)

        binding.textInitials.largeTitleStyle(R.color.primaryDark)

        binding.textName.largeBold()
        binding.textName.setMargins(mediumMargin, mediumMargin, mediumMargin)

        binding.textEmail.smallRegular()
        binding.textEmail.setMargins(mediumMargin, 0, mediumMargin)

        binding.scrollView.setMargins(0, mediumMargin)

        binding.buttonData.setOnClickListener(this)
        binding.buttonFavorite.setOnClickListener(this)
        binding.buttonHelp.setOnClickListener(this)
        binding.buttonLogout.setOnClickListener(this)
        binding.buttonTerms.setOnClickListener(this)
        binding.buttonMetrics.setOnClickListener(this)
        binding.buttonEstablishmentData.setOnClickListener(this)

        try {
            val info =
                requireContext().packageManager.getPackageInfo(requireContext().packageName, 0)
            val version = info.versionName
            binding.labelVersion.updateText(getString(R.string.label_version, version))
        } catch (e: Exception) {
        }
    }

    override fun onClick(v: View?) {
        v ?: return
        when (v) {
            binding.buttonData -> findNavController().navigate(R.id.action_profileFragment_to_editProfileFragment)
            binding.buttonFavorite -> findNavController().navigate(R.id.action_profileFragment_to_favoritesFragment)
            binding.buttonTerms -> {
                val action = ProfileFragmentDirections.actionProfileFragmentToTermsFragment()
                action.url = getString(R.string.key_privacy)
                findNavController().navigate(action)
            }
            binding.buttonLogout -> {
                val action = ProfileFragmentDirections.actionProfileFragmentToCustomBottomDialog(
                    CustomBottomDialog.MODE_LOGOUT
                )
                findNavController().navigate(action)
            }
            binding.buttonMetrics -> {
                val action = ProfileFragmentDirections.actionProfileFragmentToMetricsFragment()
                action.url = getString(R.string.key_metrics_url, ApiUtils.token)
                findNavController().navigate(action)
            }
            binding.buttonEstablishmentData -> {
                findNavController().navigate(R.id.action_profileFragment_to_servicesFragment)
            }
        }
    }

    override fun onResume() {
        super.onResume()

        val user = mViewModel?.getUser()
        user ?: return
        binding.textName.text = user.name
        binding.textEmail.text = user.email
        binding.textInitials.text = user.getInitials()
        binding.imageShirt.setColorFilter(user.getColor(requireContext()))
        if (user.isOwner()) {
            binding.buttonFavorite.visibility = View.GONE
        } else {
            binding.buttonEstablishmentData.visibility = View.GONE
            binding.buttonMetrics.visibility = View.GONE
        }
    }

}
