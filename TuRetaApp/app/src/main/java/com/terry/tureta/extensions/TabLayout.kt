package com.terry.tureta.extensions

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.google.android.material.tabs.TabLayout
import com.terry.tureta.poppinsRegular


fun TabLayout.setStyle() {
    for (index in 0 until this.tabCount) {
        val tab = this.getTabAt(index)
        val parent = tab!!.view as ViewGroup
        for (i in 0 until parent.childCount) {
            val child: View = parent.getChildAt(i)
            if (child is TextView) {
                child.typeface = poppinsRegular
                child.isAllCaps = false
            }
        }
    }
}