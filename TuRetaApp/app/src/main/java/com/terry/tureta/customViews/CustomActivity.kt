package com.terry.tureta.customViews

import android.annotation.SuppressLint
import android.app.Activity
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Base64
import android.util.Log
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import com.terry.tureta.helpers.SharedPreferences
import com.terry.tureta.model.*
import com.terry.tureta.sectionEstablishments.viewModel.EstablishmentViewModel
import com.terry.tureta.sectionLogin.viewModel.LoginViewModel
import com.terry.tureta.sectionReservations.viewModel.ReservationsViewModel
import com.terry.tureta.sectionMain.viewModel.MainViewModel
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

@SuppressLint("Registered")
open class CustomActivity : AppCompatActivity(), MainViewModel.IMainObserver,
    EstablishmentViewModel.IEstablishmentObserver, LoginViewModel.ILoginObserver, TextWatcher,
    ReservationsViewModel.IReservationsObserver {

    private var sharedPreference: SharedPreferences? = null
    var user: User? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        sharedPreference = SharedPreferences(this)
    }

    @SuppressLint("PackageManagerGetSignatures")
    fun printHashKey(): String {
        try {
            val info: PackageInfo = this.packageManager
                .getPackageInfo(this.packageName, PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md: MessageDigest = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                val hashKey = String(Base64.encode(md.digest(), 0))
                Log.i("KEY", "printHashKey() Hash Key: $hashKey")

                return hashKey
            }
        } catch (e: NoSuchAlgorithmException) {
            Log.e("KEY", "printHashKey()", e)
        } catch (e: Exception) {
            Log.e("KEY", "printHashKey()", e)
        }
        return ""
    }

    open fun hideKeyboard() {
        val imm: InputMethodManager? =
            getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.hideSoftInputFromWindow(window.decorView.windowToken, 0)
    }

    override fun onLogout(isSuccess: Boolean, message: Int?) {}

    override fun onLocationEnabled(isEnabled: Boolean) {}

    override fun onLocationLoaded(location: Location) {}

    override fun onSearchResult(list: List<Establishment>) {}

    override fun onCameraResult(list: List<Establishment>) {}

    override fun onFavoritesResult(list: List<Establishment>?) {}

    override fun onNameUpdated(isSuccess: Boolean) {}

    override fun onColorUpdated(isSuccess: Boolean) {}

    override fun onKeyboardVisibilityChanged(isVisible: Boolean) {}

    override fun onUpdateView() {}

    override fun onUpdateDates(datesFounded: Boolean) {}

    override fun onDetailsFailed(message: Int) {}

    override fun onFavoriteFinished(isSuccessful: Boolean) {}

    override fun onFieldDeleted(isSuccessful: Boolean, field: Field?) {}

    override fun onFieldCreated(isSuccessful: Boolean, field: Field?) {}

    override fun onFieldEdited(isSuccessful: Boolean, field: Field?) {}

    override fun onFieldAvailableFailed() {}

    override fun onReservationResult(isSuccessful: Boolean) {}

    //region login callbacks
    override fun onLoggedIn(user: User) {}

    override fun goToWalkthrough() {}

    override fun onLoginFailed(message: Int?) {}

    override fun onRegisterResult(isSuccess: Boolean, message: Int?) {}

    override fun onRestorePasswordResult(isSuccess: Boolean, message: Int) {}
    //endregion login callbacks

    override fun afterTextChanged(s: Editable?) {}

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

    override fun onReservationsSuccess(list: List<Reservation>) {}

    override fun onReservationsFailed(message: Int) {}

    override fun onReservationUpdated(isSuccessful: Boolean, message: Int, isOwner: Boolean) {}

    override fun onFilterReservations(text: String) {}

    override fun onUpdateFilterText(text: String) {}

}