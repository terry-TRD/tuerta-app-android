package com.terry.tureta.model

import com.google.gson.annotations.SerializedName

class Tournament {
    var id: Int = -1
    var name: String = ""

    @SerializedName("game_type")
    var gameType: Int = 0

    @SerializedName("match_won_points")
    var matchWonPoints: Int = 0

    @SerializedName("tied_match_points")
    var matchTiePoints: Int = 0

    @SerializedName("lost_match_points")
    var matchLosePoints: Int = 0

    @SerializedName("number_of_rounds")
    var numberOfRounds: Int = 0
    var teams: ArrayList<Team> = ArrayList()
    var deleted: Boolean = false

    var matches: ArrayList<Match> = ArrayList()
}