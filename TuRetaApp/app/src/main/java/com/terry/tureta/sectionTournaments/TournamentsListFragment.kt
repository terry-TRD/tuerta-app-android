package com.terry.tureta.sectionTournaments

import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.terry.tureta.R
import com.terry.tureta.customViews.CustomFragment
import com.terry.tureta.databinding.FragmentTournamentsListBinding
import com.terry.tureta.mediumMargin
import com.terry.tureta.model.Tournament
import com.terry.tureta.sectionEstablishments.viewModel.EstablishmentViewModel
import com.terry.tureta.sectionTournaments.adapters.TournamentsAdapter
import com.terry.tureta.sectionTournaments.viewModel.TournamentViewModel
import com.terry.tureta.sectionMain.viewModel.MainViewModel
import grid.apps.supplies_admin.Extensions.largeTitleStyle
import grid.apps.supplies_admin.Extensions.setMargins

class TournamentsListFragment : CustomFragment(), TournamentsAdapter.ITournamentAdapter {

    private lateinit var binding: FragmentTournamentsListBinding
    private val list: ArrayList<Tournament> = ArrayList()
    private val adapter by lazy { TournamentsAdapter(requireContext(), list, this) }

    private var isOwner = false

    private val mViewModel by lazy {
        activity?.run {
            val model = ViewModelProvider(this)[TournamentViewModel::class.java]
            isOwner = ViewModelProvider(this)[MainViewModel::class.java].getUser().isOwner()
            val id = ViewModelProvider(this)[EstablishmentViewModel::class.java].getEstablishment().id
            model.setEstablishmentId(id)
            model
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentTournamentsListBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recyclerView.adapter = adapter
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.addItemDecoration(object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(
                outRect: Rect,
                view: View,
                parent: RecyclerView,
                state: RecyclerView.State
            ) {
                val position = parent.getChildAdapterPosition(view)
                val size = parent.adapter!!.itemCount

                if (position == 0) {
                    outRect.top = mediumMargin
                }
                if (position == size - 1) {
                    outRect.bottom = mediumMargin
                }
            }
        })

        binding.buttonCreateTournament.setOnClickListener { findNavController().navigate(R.id.action_tournamentsListFragment_to_editTournamentFragment) }
        binding.buttonCreateTournament.setMargins(mediumMargin, 0, mediumMargin, mediumMargin)

        binding.textMessage.largeTitleStyle()
        binding.textMessage.setMargins(mediumMargin, 0, mediumMargin)
    }

    override fun onResume() {
        super.onResume()

        mViewModel?.addObserver(this)
        mViewModel?.downloadTournaments()

        adapter.isOwner(isOwner)
    }

    override fun onPause() {
        super.onPause()

        mViewModel?.deleteObserver(this)
    }

    override fun onTournamentsLoaded(list: List<Tournament>) {
        this.list.clear()
        this.list.addAll(list)
        adapter.notifyDataSetChanged()

        if (this.list.isEmpty()) {
            binding.imageView.visibility = View.VISIBLE
            binding.textMessage.visibility = View.VISIBLE

            if (isOwner) {
                binding.buttonCreateTournament.visibility = View.VISIBLE
            }
        }
    }

    override fun onTournamentsFailed(message: Int) {
        showToast(message)
    }

    override fun onTournamentSelected(tournament: Tournament) {
        mViewModel?.setTournament(tournament)
        if (isOwner) {
            findNavController().navigate(R.id.action_tournamentsListFragment_to_tournamentFragment)
        } else {
            findNavController().navigate(R.id.action_establishmentFragment_to_tournamentUserFragment)
        }
    }
}
