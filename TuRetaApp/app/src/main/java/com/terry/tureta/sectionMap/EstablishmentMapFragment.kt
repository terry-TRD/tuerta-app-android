package com.terry.tureta.sectionMap

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.terry.tureta.R
import com.terry.tureta.customViews.CustomFragment
import com.terry.tureta.databinding.FragmentEstablishmentMapBinding
import com.terry.tureta.extensions.bitmapFromVector
import com.terry.tureta.mediumFont
import com.terry.tureta.sectionEstablishments.viewModel.EstablishmentViewModel
import grid.apps.supplies_admin.Extensions.setToolbar

class EstablishmentMapFragment : CustomFragment(), OnMapReadyCallback {

    private lateinit var binding: FragmentEstablishmentMapBinding

    private val mViewModel by lazy {
        activity?.run {
            ViewModelProvider(this)[EstablishmentViewModel::class.java]
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentEstablishmentMapBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onResume() {
        super.onResume()

        val establishment = mViewModel!!.getEstablishment()
        binding.toolbar.title = establishment.name
        binding.toolbar.setToolbar(mediumFont, true)
    }

    override fun onMapReady(map: GoogleMap?) {
        map ?: return

        val establishment = mViewModel!!.getEstablishment()

        val latLng = LatLng(establishment.lat, establishment.lng)

        map.addMarker(
            MarkerOptions()
                .position(latLng)
                .title(establishment.name)
                .icon(bitmapFromVector(requireContext(), R.drawable.ic_location))
        )

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f))
    }
}