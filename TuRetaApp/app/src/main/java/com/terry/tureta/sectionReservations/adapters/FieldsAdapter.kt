package com.terry.tureta.sectionReservations.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.terry.tureta.R
import com.terry.tureta.databinding.ItemFieldBinding
import com.terry.tureta.largeMargin
import com.terry.tureta.mediumMargin
import com.terry.tureta.model.Field
import com.terry.tureta.smallMargin
import grid.apps.supplies_admin.Extensions.extraSmallRegular
import grid.apps.supplies_admin.Extensions.mediumBold
import grid.apps.supplies_admin.Extensions.setMargins
import grid.apps.supplies_admin.Extensions.smallRegular

class FieldsAdapter(
    private val context: Context,
    private val list: List<Field>,
    private val image: String,
    private val establishment: String,
    private var selectedPosition: Int,
    private val callback: IFieldsAdapter,
    private val validateSchedule: Boolean = true
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VHField(
            ItemFieldBinding.inflate(
                LayoutInflater.from(context), parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as VHField).bindData(list[position], position)
    }

    inner class VHField(private val binding: ItemFieldBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.textName.mediumBold(R.color.black)
            binding.textName.setMargins(mediumMargin, smallMargin, smallMargin)
            binding.separator.setMargins(0, smallMargin)
            binding.textLocation.text = establishment
            Picasso.get().load(image).into(binding.imageField)

            binding.iconField.setMargins(mediumMargin)
            binding.iconLocation.setMargins(mediumMargin)

            binding.textGrass.extraSmallRegular(R.color.black)
            binding.textGrass.setMargins(smallMargin, smallMargin, smallMargin)
            binding.textLocation.extraSmallRegular(R.color.black)
            binding.textLocation.setMargins(smallMargin, smallMargin, smallMargin, mediumMargin)
        }

        fun bindData(field: Field, position: Int) {
            binding.textName.text = context.getString(R.string.label_field_name, field.name)

            binding.textGrass.text = field.getGrassType(context)

            binding.cardItem.setOnClickListener {
                if (validateSchedule && field.availability.isNullOrEmpty()) {
                    Toast.makeText(
                        context,
                        R.string.message_no_schedules_available,
                        Toast.LENGTH_LONG
                    ).show()
                    return@setOnClickListener
                }

                selectedPosition = position
                callback.onClick(field, position)

                notifyDataSetChanged()
            }

            if (selectedPosition == position) {
                binding.cardStroke.setCardBackgroundColor(
                    ContextCompat.getColor(context, R.color.accent)
                )
            } else {
                binding.cardStroke.setCardBackgroundColor(
                    ContextCompat.getColor(context, R.color.grayText)
                )
            }
        }

    }

    interface IFieldsAdapter {
        fun onClick(field: Field, position: Int)
    }

}