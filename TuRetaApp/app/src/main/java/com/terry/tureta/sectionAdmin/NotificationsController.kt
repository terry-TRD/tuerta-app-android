package com.terry.tureta.sectionAdmin

import android.content.ContentValues
import android.util.Log
import com.google.firebase.messaging.FirebaseMessaging
import com.terry.tureta.model.User

object NotificationsController {

    private const val USER_LABEL = "user"

    fun subscribeToNotifications(user: User) {
        FirebaseMessaging.getInstance().subscribeToTopic(USER_LABEL + user.id.toString())
            .addOnCompleteListener {
                if (!it.isSuccessful) {
                    Log.w(ContentValues.TAG, "subscription failed", it.exception)
                    return@addOnCompleteListener
                }

            }
    }

    fun unsubscribeFromNotifications(user: User) {
        FirebaseMessaging.getInstance().unsubscribeFromTopic(USER_LABEL + user.id.toString())
            .addOnCompleteListener {
                if (!it.isSuccessful) {
                    Log.w(ContentValues.TAG, "unsubscription failed", it.exception)
                    return@addOnCompleteListener
                }

            }
    }

}