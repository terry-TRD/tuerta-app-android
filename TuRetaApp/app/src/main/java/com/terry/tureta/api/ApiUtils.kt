package com.terry.tureta.api

import com.terry.tureta.BuildConfig

object ApiUtils {

    var token: String = ""

    val apiService: APIService
        get() = ApiWorker.getClient(
            BuildConfig.BASE_URL,
            token
        ).create<APIService>(APIService::class.java)

    val apiUser: APIUser
        get() = ApiWorker.getClient(
            BuildConfig.BASE_URL,
            token
        ).create<APIUser>(APIUser::class.java)

    val apiTournaments: APITournaments
        get() = ApiWorker.getClient(
            BuildConfig.BASE_URL,
            token
        ).create<APITournaments>(APITournaments::class.java)
}