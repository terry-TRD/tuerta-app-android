package com.terry.tureta.sectionFields

import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.terry.tureta.R
import com.terry.tureta.customViews.CustomFragment
import com.terry.tureta.databinding.FragmentSelectFieldBinding
import com.terry.tureta.mediumMargin
import com.terry.tureta.model.Field
import com.terry.tureta.sectionReservations.adapters.FieldsAdapter
import com.terry.tureta.sectionTournaments.viewModel.TournamentViewModel
import com.terry.tureta.sectionMain.viewModel.MainViewModel
import grid.apps.supplies_admin.Extensions.setMargins

class SelectFieldFragment : CustomFragment(), FieldsAdapter.IFieldsAdapter {

    private lateinit var binding: FragmentSelectFieldBinding

    private val adapter by lazy {
        val establishment = mMainViewModel.getUser().getEstablishment()
        FieldsAdapter(
            requireContext(),
            establishment.fields.filter { it.isActive },
            establishment.image,
            establishment.name,
            mTournamentViewModel.fieldSelectedPosition,
            this,
            false
        )
    }

    private val mMainViewModel by lazy {
        ViewModelProvider(requireActivity())[MainViewModel::class.java]
    }

    private val mTournamentViewModel by lazy {
        ViewModelProvider(requireActivity())[TournamentViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSelectFieldBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonFinish.setOnClickListener(this)
        binding.buttonFinish.setMargins(mediumMargin, 0, mediumMargin, mediumMargin)

        binding.recyclerFields.adapter = adapter
        binding.recyclerFields.setMargins(0, 0, 0, mediumMargin)
        binding.recyclerFields.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerFields.addItemDecoration(object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(
                outRect: Rect,
                view: View,
                parent: RecyclerView,
                state: RecyclerView.State
            ) {
                val position = parent.getChildAdapterPosition(view)
                val size = parent.adapter!!.itemCount

                var top = 0
                var bottom = 0

                if (position == 0) top = mediumMargin

                if (position < size - 1) bottom = mediumMargin

                outRect.left = mediumMargin
                outRect.top = top
                outRect.right = mediumMargin
                outRect.bottom = bottom
            }
        })
    }

    override fun onClick(v: View?) {
        v ?: return

        if(mTournamentViewModel.fieldSelected != null){
            findNavController().navigate(R.id.action_selectFieldFragment_to_matchFragment)
        } else {
            showToast(R.string.label_field_selected_empty)
        }
    }

    override fun onClick(field: Field, position: Int) {
        mTournamentViewModel.fieldSelectedPosition = position
        mTournamentViewModel.fieldSelected = field
    }

}