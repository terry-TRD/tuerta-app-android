package com.terry.tureta.sectionTournaments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.terry.tureta.R
import com.terry.tureta.customViews.CustomFragment
import com.terry.tureta.databinding.FragmentTournamentStatsBinding
import com.terry.tureta.mediumMargin
import com.terry.tureta.model.TournamentStats
import com.terry.tureta.sectionTournaments.adapters.TournamentStatsAdapter
import com.terry.tureta.sectionTournaments.viewModel.TournamentViewModel
import com.terry.tureta.smallMargin
import grid.apps.supplies_admin.Extensions.extraSmallRegular
import grid.apps.supplies_admin.Extensions.setMargins
import grid.apps.supplies_admin.Extensions.smallRegular

class TournamentStatsFragment : CustomFragment() {

    private lateinit var binding: FragmentTournamentStatsBinding

    private val list: ArrayList<TournamentStats> = ArrayList()

    private val adapter by lazy {
        TournamentStatsAdapter(list, requireContext())
    }

    private val mViewModel by lazy {
        ViewModelProvider(requireActivity())[TournamentViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTournamentStatsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setExtraSmallLabels(
            binding.textGamesPlayed,
            binding.textGamesWon,
            binding.textGamesTied,
            binding.textGamesLost,
            binding.textDifference,
            binding.textPoints
        )

        setSmallLabels(
            binding.textJJ,
            binding.textJG,
            binding.textJE,
            binding.textJP,
            binding.textDIF
        )

        binding.textPTS.smallRegular(R.color.black)
        binding.textPTS.setPadding(0, smallMargin, 0, smallMargin)

        binding.textTeam.smallRegular(R.color.black)
        binding.textTeam.setPadding(smallMargin, smallMargin, 0, smallMargin)

        binding.textGamesPlayed.setPadding(smallMargin, smallMargin, smallMargin / 2, smallMargin)
        binding.textGamesWon.setPadding(smallMargin / 2, smallMargin, smallMargin / 2, smallMargin)
        binding.textGamesTied.setPadding(smallMargin / 2, smallMargin, smallMargin, smallMargin)
        binding.textGamesLost.setPadding(smallMargin, smallMargin, smallMargin / 2, smallMargin)
        binding.textDifference.setPadding(
            smallMargin / 2,
            smallMargin,
            smallMargin / 2,
            smallMargin
        )
        binding.textPoints.setPadding(smallMargin / 2, smallMargin, smallMargin, smallMargin)

        binding.textGamesLost.setMargins(0, 0, 0, mediumMargin)

        binding.recyclerStats.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerStats.adapter = adapter
    }

    override fun onResume() {
        super.onResume()

        mViewModel.addObserver(this)
        mViewModel.getTournamentStats()
    }

    override fun onPause() {
        super.onPause()

        mViewModel.deleteObserver(this)
    }

    private fun setExtraSmallLabels(vararg labels: AppCompatTextView) {
        for (label in labels) {
            label.extraSmallRegular(R.color.grayText)
        }
    }

    private fun setSmallLabels(vararg labels: AppCompatTextView) {
        for (label in labels) {
            label.smallRegular(R.color.primary)
            label.setPadding(0, smallMargin, 0, smallMargin)
        }
    }

    override fun onTournamentStatsSuccess(list: List<TournamentStats>) {
        this.list.clear()
        this.list.addAll(list)

        adapter.notifyDataSetChanged()
    }

    override fun onTournamentStatsFailed(message: Int) {
        showToast(message)
    }

}