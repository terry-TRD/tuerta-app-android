package com.terry.tureta.model

import android.content.Context
import android.graphics.Color
import com.google.gson.annotations.SerializedName
import com.terry.tureta.R
import java.io.Serializable
import java.util.*
import kotlin.collections.ArrayList

class Reservation : Serializable {
    var id: Int = -1
    var code: String = ""
    var from: String = ""
    var to: String = ""
    var user: User? = null
    var establishment: Establishment = Establishment()
    var field: Field? = null

    @SerializedName("grass_type")
    var grassType: Int = -1

    @SerializedName("game_type")
    var gameType: Int = -1
    var comments: String = ""
    var subtotal: String = ""
    var fee: String = ""
    var total: String = ""
    var status = -1

    @SerializedName("created_at")
    var createdAt = ""
    var reviews: ArrayList<Reviews> = ArrayList()

    // if reservation was created manually
    var type: Int = 1

    fun getDate(context: Context): String {
        val parts = from.split(" ")
        val dates = parts[0].split("-")
        val month = dates[1].toInt()
        return context.resources.getStringArray(R.array.date_months)[month - 1] + " ${dates[2]}, ${dates[0]}"
    }

    fun getHours(): String {
        return from.split(" ")[1] + "HRS - " + to.split(" ")[1] + "HRS"
    }

    fun getLength(): Float {
        val parts1 = getDate(from).split("-")
        val hours1 = getHours(from).split(":")
        val date1 = Date(
            parts1[0].toInt(),
            parts1[1].toInt() - 1,
            parts1[2].toInt(),
            hours1[0].toInt(),
            hours1[1].toInt()
        )

        val parts2 = getDate(to).split("-")
        val hours2 = getHours(to).split(":")
        val date2 = Date(
            parts2[0].toInt(),
            parts2[1].toInt() - 1,
            parts2[2].toInt(),
            hours2[0].toInt(),
            hours2[1].toInt()
        )

        val seconds = (date2.time - date1.time).toFloat() / 1000
        val minutes = seconds / 60
        return minutes / 60
    }

    fun getHourPrice(): String = "$$subtotal MX"

    fun getTotalPrice(): String = "$$total MX"

    fun getHoursNumber(): String {
        val sub = subtotal.toDouble()
        val total = total.toDouble()
        return (sub / total).toString()
    }

    private fun getDate(string: String): String = string.split(" ")[0]

    private fun getHours(string: String): String = string.split(" ")[1]

    fun getGrassType(context: Context): String = context.resources.getStringArray(R.array.grass_types)[grassType - 1]

    fun getReview(userId: Int): Reviews? {
        for (review in reviews) {
            if (review.createdBy == userId) {
                return review
            }
        }
        return null
    }

    fun getColor(context: Context): Int {
        val colors = context.resources.getIntArray(R.array.reservations_colors)
        return colors[status - 1]
    }

    fun isManual(): Boolean = type == 2

}