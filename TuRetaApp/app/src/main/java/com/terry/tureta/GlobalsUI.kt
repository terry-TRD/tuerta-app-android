package com.terry.tureta

import android.R
import android.content.Context
import android.content.res.Configuration
import android.graphics.Point
import android.graphics.Typeface
import android.util.TypedValue
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import grid.apps.supplies_admin.Extensions.calculateHeight
import grid.apps.supplies_admin.Extensions.calculateWidth
import grid.apps.supplies_admin.Extensions.largeBold
import grid.apps.supplies_admin.Extensions.mediumBold

/**
 * Variables and functions for the responsive design
 */

//Text sizes
var extraSmallFont: Float = 0f
var smallFont: Float = 0f
var mediumFont: Float = 0f
var largeFont: Float = 50f
var extraLargeFont: Float = 0f

//Margins sizes
var smallMargin: Int = 0
var mediumMargin: Int = 0
var largeMargin: Int = 0

var elevationHeight: Float = 0f
var buttonHeight: Int = 0

var screenWidth: Float = 0f
var screenHeight: Float = 0f

lateinit var poppinsSemiBold: Typeface
lateinit var poppinsRegular: Typeface
lateinit var nunitoBold: Typeface

var isPortrait = false
var isLargeScreen = false

var toolbarHeight = 0

fun initializeUIGlobals(activity: AppCompatActivity) {
    val size = Point()
    activity.windowManager.defaultDisplay.getSize(size)

    val width = if (size.x < size.y) {
        isPortrait = true
        size.x
    } else {
        size.y
    }

    if (activity.resources.configuration.screenLayout and (Configuration.SCREENLAYOUT_SIZE_MASK)
        == Configuration.SCREENLAYOUT_SIZE_XLARGE
    ) isLargeScreen = true

    screenWidth = width.toFloat()

    smallMargin = if (!isLargeScreen) (screenWidth * 0.017).toInt()
    else (screenWidth * 0.008).toInt()

    mediumMargin = if (!isLargeScreen) (screenWidth * 0.05).toInt()
    else (screenWidth * 0.02).toInt()

    largeMargin = if (!isLargeScreen) (screenWidth * 0.1).toInt()
    else (screenWidth * 0.05).toInt()

    poppinsSemiBold = Typeface.createFromAsset(activity.assets, "poppins_semi_bold.ttf")
    poppinsRegular = Typeface.createFromAsset(activity.assets, "poppins_regular.ttf")
    nunitoBold = Typeface.createFromAsset(activity.assets, "nunito_bold.ttf")

    val text = "Ingresa tu número de teléfono"

    val textView = AppCompatTextView(activity)
    textView.largeBold()
    textView.text = text

    val desiredWidth = width.toFloat() - (largeMargin * 2)
    while (textView.calculateWidth().toFloat() > desiredWidth) {
        largeFont--
        textView.textSize = largeFont
    }

    if (isLargeScreen) largeFont /= 2

    extraSmallFont = (largeFont * 0.6).toFloat()
    smallFont = (largeFont * 0.8).toFloat()
    mediumFont = largeFont
    extraLargeFont = (largeFont * 1.8).toFloat()

    largeFont = (largeFont * 1.3).toFloat()

    elevationHeight = (smallMargin / 2).toFloat()

    textView.mediumBold()
    buttonHeight = textView.calculateHeight() + (smallMargin * 3)

    val tv = TypedValue()
    if (activity.theme.resolveAttribute(R.attr.actionBarSize, tv, true)) {
        toolbarHeight =
            TypedValue.complexToDimensionPixelSize(tv.data, activity.resources.displayMetrics)
    }
}

fun getNewOrientation(size: Point) {
    isPortrait = size.x < size.y
}