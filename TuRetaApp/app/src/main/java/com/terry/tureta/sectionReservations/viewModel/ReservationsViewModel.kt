package com.terry.tureta.sectionReservations.viewModel

import android.util.Log
import androidx.lifecycle.ViewModel
import com.terry.tureta.api.ApiUtils
import com.terry.tureta.R
import com.terry.tureta.model.Reservation
import com.terry.tureta.model.SearchResult
import com.terry.tureta.model.User
import com.terry.tureta.sectionReservations.ReservationsListFragment
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class ReservationsViewModel : ViewModel() {

    private lateinit var mUser: User

    private val mObservers: ArrayList<IReservationsObserver> = ArrayList()

    private lateinit var mReservationDisposable: Disposable
    private lateinit var updateDisposable: Disposable
    private lateinit var reviewDisposable: Disposable

    private lateinit var mReservation: Reservation

    private var textFilterFuture = ""
    private var textFilterPast = ""

    private var mNewReservationType = RESERVATION_NORMAL

    companion object {
        const val APPROVE_RESERVATION = "approve"
        const val REJECT_RESERVATION = "reject"
        const val CANCEL_RESERVATION = "cancel"

        const val FILTER_FUTURE = "future"
        const val FILTER_PAST = "past"

        const val RESERVATION_NORMAL = 1
        const val RESERVATION_MANUAL = 2
    }

    fun getReservations() {
        if (isOwner()) getOwnerReservations(mUser.getEstablishment().id, FILTER_FUTURE)
        else getUserReservations(FILTER_FUTURE)
    }

    fun getHistoryReservations() {
        if (isOwner()) getOwnerReservations(mUser.getEstablishment().id, FILTER_PAST)
        else getUserReservations(FILTER_PAST)
    }

    private fun getUserReservations(filter: String) {
        mReservationDisposable = reservationsDisposable(
            ApiUtils.apiUser.getReservations(filter)
        )
    }

    private fun getOwnerReservations(establishmentId: Int, filter: String) {
        mReservationDisposable =
            reservationsDisposable(
                ApiUtils.apiService.getReservations(establishmentId, filter)
            )
    }

    fun createReservation(fieldId: Int, start: String, end: String, gameType: String, type: Int, price: Int) {
        mReservationDisposable = ApiUtils.apiService
            .createReservation(fieldId, start, end, gameType, type = type, price = price)
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    for (observer in mObservers) {
                        observer.onReservationResult(true)
                    }
                    mReservationDisposable.dispose()
                },
                { error ->
                    for (observer in mObservers) {
                        observer.onReservationResult(false)
                    }
                    mReservationDisposable.dispose()
                },
                { Log.d("TAG", "completed") }
            )
    }

    private fun reservationsDisposable(
        observable: Observable<SearchResult<ArrayList<Reservation>>>
    ): Disposable {
        return observable
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    for (observer in mObservers) {
                        observer.onReservationsSuccess(result.data!!)
                    }
                    mReservationDisposable.dispose()
                },
                { error ->
                    for (observer in mObservers) {
                        observer.onReservationsFailed(R.string.message_reservations_error)
                    }
                    mReservationDisposable.dispose()
                },
                { Log.d("TAG", "completed") }
            )
    }

    fun approveReservation() {
        updateReservationStatus(APPROVE_RESERVATION)
    }

    fun cancelReservation() {
        if (isOwner()) {
            updateReservationStatus(CANCEL_RESERVATION)
        } else {
            updateDisposable = ApiUtils.apiUser
                .cancelReservations(mReservation.id)
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { result ->
                        for (observer in mObservers) {
                            observer.onReservationUpdated(
                                true,
                                R.string.label_reservation_updated_success,
                                mUser.isOwner()
                            )
                        }
                    },
                    { error ->
                        for (observer in mObservers) {
                            observer.onReservationUpdated(
                                false,
                                R.string.message_reservation_cancel_fail,
                                mUser.isOwner()
                            )
                        }
                    },
                    { Log.d("TAG", "completed") }
                )
        }
    }

    fun rejectReservation() {
        updateReservationStatus(REJECT_RESERVATION)
    }

    private fun updateReservationStatus(status: String) {
        val id = mUser.establishments[0].id

        updateDisposable = ApiUtils.apiService
            .confirmReservation(id, mReservation.id, status)
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    for (observer in mObservers) {
                        when (status) {
                            APPROVE_RESERVATION -> {
                                observer.onReservationUpdated(
                                    true,
                                    R.string.message_reservation_approve_success,
                                    mUser.isOwner()
                                )
                            }
                            REJECT_RESERVATION -> {
                                observer.onReservationUpdated(
                                    true,
                                    R.string.message_reservation_reject_success,
                                    mUser.isOwner()
                                )
                            }
                            CANCEL_RESERVATION -> {
                                observer.onReservationUpdated(
                                    true,
                                    R.string.message_reservation_cancel_success,
                                    mUser.isOwner()
                                )
                            }
                        }
                    }
                },
                { error ->
                    for (observer in mObservers) {
                        when (status) {
                            APPROVE_RESERVATION -> {
                                observer.onReservationUpdated(
                                    false,
                                    R.string.message_reservation_approve_fail,
                                    mUser.isOwner()
                                )
                            }
                            REJECT_RESERVATION -> {
                                observer.onReservationUpdated(
                                    false,
                                    R.string.message_reservation_reject_fail,
                                    mUser.isOwner()
                                )
                            }
                            CANCEL_RESERVATION -> {
                                observer.onReservationUpdated(
                                    false,
                                    R.string.message_reservation_cancel_fail,
                                    mUser.isOwner()
                                )
                            }
                        }
                    }
                },
                { Log.d("TAG", "completed") }
            )
    }

    fun saveReview(
        rate: Int
    ) {
        reviewDisposable = ApiUtils.apiService
            .saveReview(mReservation.id, rate)
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    for (observer in mObservers) {
                        observer.onReservationUpdated(
                            true,
                            R.string.message_review_success,
                            mUser.isOwner()
                        )
                    }
                },
                { error ->
                    for (observer in mObservers) {
                        observer.onReservationUpdated(
                            false,
                            R.string.message_review_fail,
                            mUser.isOwner()
                        )
                    }
                },
                { Log.d("TAG", "completed") }
            )
    }

    fun filterReservations(text: String) {
        var filter = ""
        for (observer in mObservers) {
            if (observer is ReservationsListFragment) {
                filter = observer.getFilter()
                break
            }
        }

        if (filter == FILTER_FUTURE) textFilterFuture = text
        else textFilterPast = text

        for (observer in mObservers) {
            observer.onFilterReservations(text)
        }
    }

    fun clearFilters() {
        textFilterFuture = ""
        textFilterPast = ""

        for (observer in mObservers) {
            observer.onFilterReservations("")
        }
    }

    fun setReservation(reservation: Reservation) {
        this.mReservation = reservation
    }

    fun getReservation(): Reservation = this.mReservation

    fun isOwner(): Boolean = this.mUser.isOwner()

    fun setUser(user: User): ReservationsViewModel {
        this.mUser = user
        return this
    }

    fun getUser(): User = this.mUser

    fun addObserver(observer: IReservationsObserver) {
        for (mObserver in mObservers) {
            if (mObserver == observer) return
        }
        mObservers.add(observer)
    }

    fun deleteObserver(observer: IReservationsObserver) {
        mObservers.remove(observer)
    }

    fun updateFilterText(text: String){
        for(observer in mObservers){
            observer.onUpdateFilterText(text)
        }
    }

    fun getFutureFilter(): String = textFilterFuture

    fun getPastFilter(): String = textFilterPast

    interface IReservationsObserver {
        fun onReservationsSuccess(list: List<Reservation>)
        fun onReservationsFailed(message: Int)
        fun onReservationUpdated(isSuccessful: Boolean, message: Int, isOwner: Boolean)
        fun onFilterReservations(text: String)
        fun onUpdateFilterText(text: String)
        fun onReservationResult(isSuccessful: Boolean)
    }

}