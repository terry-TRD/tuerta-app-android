package com.terry.tureta.sectionMatches.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.terry.tureta.R
import com.terry.tureta.databinding.ItemMatchBinding
import com.terry.tureta.databinding.ItemTextBinding
import com.terry.tureta.helpers.CircleTransform
import com.terry.tureta.mediumMargin
import com.terry.tureta.model.Match
import com.terry.tureta.smallMargin
import grid.apps.supplies_admin.Extensions.extraSmallRegular
import grid.apps.supplies_admin.Extensions.setMargins
import grid.apps.supplies_admin.Extensions.smallRegular

class MatchesAdapter(
    private val context: Context,
    private val list: List<Any>,
    private val callback: IMatchesAdapter
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val TYPE_HEADER = 100
        const val TYPE_MATCH = 200
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_HEADER -> VHHeader(
                ItemTextBinding.inflate(
                    LayoutInflater.from(context), parent, false
                )
            )
            else -> VHMatch(
                ItemMatchBinding.inflate(
                    LayoutInflater.from(context), parent, false
                )
            )
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is VHHeader) holder.bindData(list[position] as String)
        if (holder is VHMatch) holder.bindData(list[position] as Match)
    }

    override fun getItemViewType(position: Int): Int {
        return if (list[position] is String) TYPE_HEADER else TYPE_MATCH
    }

    inner class VHHeader(private val binding: ItemTextBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.text.setBackgroundColor(context.getColor(R.color.primary))
        }

        fun bindData(date: String) {
            binding.text.updateText(date)
            binding.text.setStartIcon(ContextCompat.getDrawable(context, R.drawable.ic_calendar))
            binding.text.setStartIconTint(R.color.accent)
            binding.text.mShowSeparator = false
            binding.text.setLightMode()
        }

    }

    inner class VHMatch(private val binding: ItemMatchBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.textScore.smallRegular()
            binding.textTeam1.setMargins(smallMargin, 0, smallMargin)
            binding.textTeam1.extraSmallRegular()
            binding.textTeam2.setMargins(smallMargin, 0, smallMargin)
            binding.textTeam2.extraSmallRegular()

            binding.imageTeam1.setMargins(0, smallMargin, smallMargin, smallMargin)
            binding.imageTeam2.setMargins(smallMargin, smallMargin, 0, smallMargin)
        }

        fun bindData(match: Match) {
            binding.separator.visibility = if (match.showSeparator) View.VISIBLE else View.INVISIBLE

            val score = "${match.team1Annotations}-${match.team2Annotations}"
            binding.textScore.text = score

            if (match.teams[0] != null) {
                binding.textTeam1.text = match.teams[0].name
                Picasso.get()
                    .load(match.teams[0].image + "?w=150")
                    .transform(CircleTransform(context, true, true))
                    .into(binding.imageTeam1)
            }

            if (match.teams[1] != null) {
                binding.textTeam2.text = match.teams[1].name
                Picasso.get()
                    .load(match.teams[1].image + "?w=150")
                    .transform(CircleTransform(context, true, true))
                    .into(binding.imageTeam2)
            }

            binding.layoutItem.setOnClickListener { callback.onMatchSelected(match) }
        }

    }

    interface IMatchesAdapter {
        fun onMatchSelected(match: Match)
    }

}