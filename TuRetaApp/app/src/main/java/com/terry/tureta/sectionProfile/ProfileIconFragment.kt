package com.terry.tureta.sectionProfile

import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.terry.tureta.R
import com.terry.tureta.customViews.CustomFragment
import com.terry.tureta.databinding.FragmentProfileIconBinding
import com.terry.tureta.mediumMargin
import com.terry.tureta.sectionProfile.adapters.ProfileColorAdapter
import com.terry.tureta.sectionMain.viewModel.MainViewModel
import grid.apps.supplies_admin.Extensions.setMargins

class ProfileIconFragment : CustomFragment() {

    private lateinit var binding: FragmentProfileIconBinding

    private val mViewModel by lazy {
        activity?.run {
            ViewModelProvider(this)[MainViewModel::class.java]
        }
    }

    private val adapter by lazy {
        ProfileColorAdapter(
            requireContext(), requireContext().resources.getIntArray(
                R.array.icon_colors
            ),
            mViewModel!!.getUser().getInitials(),
            mViewModel!!.getUser().getColor(requireContext())
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentProfileIconBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonUpdate.setMargins(mediumMargin, 0, mediumMargin, mediumMargin)
        binding.buttonUpdate.setOnClickListener {
            val action = ProfileIconFragmentDirections.actionProfileIconFragmentToLoadDialog()
            action.message = getString(R.string.label_updating_profile)
            findNavController().navigate(action)

            mViewModel?.updateProfile(color = adapter.getColor())
        }

        binding.recyclerColors.setMargins(mediumMargin / 2, 0, mediumMargin / 2, mediumMargin)
        binding.recyclerColors.adapter = adapter
        binding.recyclerColors.layoutManager = GridLayoutManager(requireContext(), 3)
        binding.recyclerColors.addItemDecoration(
            object : RecyclerView.ItemDecoration() {
                override fun getItemOffsets(
                    outRect: Rect,
                    view: View,
                    parent: RecyclerView,
                    state: RecyclerView.State
                ) {
                    with(outRect) {

                        val isLeft = 0f     // 0
                        val isCenter = 1f   // 0.33
                        val isRight = 2f    // 0.66

                        val position = parent.getChildAdapterPosition(view)
                        val mod = position % 3f

                        var top = 0
                        var bottom = 0

                        val size = parent.adapter!!.itemCount

                        when (mod) {
                            isLeft -> {
                                when (position) {
                                    size - 1, size - 2, size - 3 -> {
                                        bottom = 0
                                        top = mediumMargin / 2
                                    }
                                    0 -> {
                                        top = 0
                                        bottom = mediumMargin / 2
                                    }
                                    else -> {
                                        top = mediumMargin / 2
                                        bottom = mediumMargin / 2
                                    }
                                }
                            }
                            isCenter -> {
                                when (position) {
                                    size - 1, size - 2 -> {
                                        bottom = 0
                                        top = mediumMargin / 2
                                    }
                                    1 -> {
                                        top = 0
                                        bottom = mediumMargin / 2
                                    }
                                    else -> {
                                        top = mediumMargin / 2
                                        bottom = mediumMargin / 2
                                    }
                                }
                            }
                            isRight -> {
                                when (position) {
                                    size - 1 -> {
                                        bottom = 0
                                        top = mediumMargin / 2
                                    }
                                    2 -> {
                                        top = 0
                                        bottom = mediumMargin / 2
                                    }
                                    else -> {
                                        top = mediumMargin / 2
                                        bottom = mediumMargin / 2
                                    }
                                }
                            }
                        }

                        this.top = top
                        this.left = mediumMargin / 2
                        this.right = mediumMargin / 2
                        this.bottom = bottom
                    }
                }
            }
        )
    }
}