package com.terry.tureta.model

import android.content.Context
import android.graphics.Color
import androidx.core.content.ContextCompat
import com.google.gson.annotations.SerializedName
import com.terry.tureta.R
import java.io.Serializable


class User : Serializable {
    var id: Int = -1
    var name: String = ""
    var email: String = ""
    var username: String = ""

    @SerializedName("is_verified")
    var isVerified: Boolean = false
    var establishments: ArrayList<Establishment> = ArrayList()
    var rate: Int = -1
    var color: String = ""

    @SerializedName("device_id")
    var deviceId = ""

    fun isOwner(): Boolean = this.establishments.size > 0

    fun getInitials(): String {
        var initials = ""

        val parts = name.split(" ")
        for (count in parts.indices) {
            if (count < 2)
                initials += parts[count].substring(0, 1)
        }

        return initials
    }

    fun getColor(context: Context): Int {
        val colors = context.resources.getIntArray(R.array.icon_colors)
        return if (this.color.isNotEmpty()) Color.parseColor(this.color)
        else colors[4]
    }

    fun getEstablishment(): Establishment = establishments[0]

}

data class UserResponse(
    var access_token: String = "",
    var expires_in: String = "",
    var user: User
)

data class UserResponseFailure(
    var message: String = ""
)