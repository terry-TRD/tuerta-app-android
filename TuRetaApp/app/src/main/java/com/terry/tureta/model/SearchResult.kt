package com.terry.tureta.model

import com.google.gson.annotations.SerializedName

class SearchResult<T> {
    var data: T? = null
    var links: Links = Links()
    var meta: Meta = Meta()

    inner class Links{
        var first: String? = null
        var last: String? = null
        var prev: String? = null
        var next: String? = null
    }

    inner class Meta{
        @SerializedName("current_page")
        var currentPage: Int = 0
        var from: Int = 0
        @SerializedName("last_page")
        var lastPage: Int = 0
        var path: String? = null
        @SerializedName("per_page")
        var perPage: Int = 0
        var to: Int = 0
        var total: Int = 0
    }
}