package com.terry.tureta.api

import com.terry.tureta.model.*
import io.reactivex.Observable
import okhttp3.MultipartBody
import retrofit2.http.*

interface APITournaments {

    @GET("api/v1/establishments/{establishment_id}/tournaments")
    fun getTournaments(
        @Path("establishment_id") establishmentId: Int
    ): Observable<SearchResult<List<Tournament>>>

    @Multipart
    @POST("api/v1/establishments/{establishmentId}/tournaments")
    fun createTournament(
        @Path("establishmentId") establishmentId: Int,
        @Part("name") name: String,
        @Part("game_type") gameType: Int,
        @Part("match_won_points") matchWonPoints: Int,
        @Part("tied_match_points") tiedMatchPoints: Int,
        @Part("lost_match_points") loseMatchPoints: Int,
        @Part("number_of_rounds") numberOfRounds: Int
    ): Observable<Any>

    @Multipart
    @POST("api/v1/establishments/{establishmentId}/tournaments/{tournamentId}")
    fun updateTournament(
        @Path("establishmentId") establishmentId: Int,
        @Path("tournamentId") tournamentId: Int,
        @Part("name") name: String,
        @Part("game_type") gameType: Int,
        @Part("match_won_points") matchWonPoints: Int,
        @Part("tied_match_points") tiedMatchPoints: Int,
        @Part("lost_match_points") loseMatchPoints: Int,
        @Part("number_of_rounds") numberOfRounds: Int,
        @Part("teams[]") teams: List<Int>,
        @Query("_method") method: String = "PUT"
    ): Observable<Any>

    @Multipart
    @POST("api/v1/establishments/{establishmentId}/teams")
    fun createTeam(
        @Path("establishmentId") establishmentId: Int,
        @Part("name") name: String,
        @Part("tournament_id") tournamentId: Int,
        @Part media: MultipartBody.Part
    ): Observable<Any>

//    @GET("api/v1/establishments/{establishmentId}/teams")
//    fun getTeams(
//        @Path("establishmentId") establishmentId: Int
//    ): Observable<SearchResult<List<Team>>>

    @GET("api/v1/establishments/{establishmentId}/teams")
    fun getTournamentTeams(
        @Path("establishmentId") establishmentId: Int,
        @Query("tournament_id") tournamentId: Int
    ): Observable<SearchResult<List<Team>>>

    @GET("api/v1/establishments/{establishmentId}/tournaments/{tournamentId}/matches")
    fun getMatches(
        @Path("establishmentId") establishmentId: Int,
        @Path("tournamentId") tournamentId: Int,
        @Query("round_number") roundNumber: Int
    ): Observable<SearchResult<List<Match>>>

    @Multipart
    @POST("api/v1/establishments/{establishmentId}/tournaments/{tournamentId}/matches")
    fun createMatch(
        @Path("establishmentId") establishmentId: Int,
        @Path("tournamentId") tournamentId: Int,
        @Query("date") date: String,
        @Part("round_number") roundNumber: Int,
        @Part("team_1_id") team1Id: Int,
        @Part("team_2_id") team2Id: Int,
        @Part("field_id") fieldId: Int,
        @Part("team_1_annotations") team1Annotations: Int,
        @Part("team_2_annotations") team2Annotations: Int
    ): Observable<Any>

    @PUT("api/v1/establishments/{establishmentId}/tournaments/{tournamentId}/matches/{matchId}")
    fun updateMatch(
        @Path("establishmentId") establishmentId: Int,
        @Path("tournamentId") tournamentId: Int,
        @Path("matchId") matchId: Int,
        @Query("date") date: String,
        @Query("round_number") roundNumber: Int,
        @Query("team_1_id") team1Id: Int,
        @Query("team_2_id") team2Id: Int,
        @Query("field_id") fieldId: Int,
        @Query("team_1_annotations") team1Annotations: Int,
        @Query("team_2_annotations") team2Annotations: Int
    ): Observable<Any>

    @GET("api/v1/establishments/{establishmentId}/tournaments/{tournamentId}/stats")
    fun getTournamentStats(
        @Path("establishmentId") establishmentId: Int,
        @Path("tournamentId") tournamentId: Int
    ): Observable<SearchResult<List<TournamentStats>>>

    @DELETE("api/v1/establishments/{establishmentId}/tournaments/{tournamentId}")
    fun deleteTournament(
        @Path("establishmentId") establishmentId: Int,
        @Path("tournamentId") tournamentId: Int
    ): Observable<Any>

    @DELETE("api/v1/establishments/{establishmentId}/teams/{teamId}")
    fun deleteTeam(
        @Path("establishmentId") establishmentId: Int,
        @Path("teamId") teamId: Int
    ): Observable<Any>
}