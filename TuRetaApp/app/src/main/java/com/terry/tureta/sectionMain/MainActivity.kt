package com.terry.tureta.sectionMain

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Rect
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.widget.TextView
import androidx.core.view.children
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.terry.tureta.*
import com.terry.tureta.customViews.CustomActivity
import com.terry.tureta.databinding.ActivityUserMainBinding
import com.terry.tureta.extensions.setTypeface
import com.terry.tureta.model.User
import com.terry.tureta.sectionEstablishments.viewModel.EstablishmentViewModel
import com.terry.tureta.sectionFields.TypesListFragment
import com.terry.tureta.sectionLogin.viewModel.LoginViewModel
import com.terry.tureta.sectionMain.viewModel.MainViewModel
import com.terry.tureta.sectionMap.LocationProvider
import com.terry.tureta.sectionReservations.viewModel.ReservationsViewModel
import com.terry.tureta.sectionTeams.TeamsListFragment
import com.terry.tureta.sectionTournaments.viewModel.TournamentViewModel
import grid.apps.supplies_admin.Extensions.circularHide
import grid.apps.supplies_admin.Extensions.circularReveal
import grid.apps.supplies_admin.Extensions.fadeIn
import grid.apps.supplies_admin.Extensions.setToolbar


class MainActivity : CustomActivity(), NavController.OnDestinationChangedListener,
    View.OnClickListener, OnKeyboardVisibilityListener {

    private lateinit var binding: ActivityUserMainBinding
    private val mNavController by lazy { findNavController(R.id.nav_host_fragment) }

    private val mMainViewModel by lazy {
        val model = ViewModelProvider(this)[MainViewModel::class.java]
        model.setActivity(this)
        model.addObserver(this)
        model
    }

    private val mEstablishmentViewModel by lazy {
        val model = ViewModelProvider(this)[EstablishmentViewModel::class.java]
        model.addObserver(this)
        model
    }

    private val mTournamentsViewModel by lazy {
        ViewModelProvider(this)[TournamentViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUserMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // initialize fonts, margins and text sizes
        initializeUIGlobals(this)

        ViewModelProvider(this)[LoginViewModel::class.java].addObserver(this)

        binding.navigationBottom.setupWithNavController(mNavController)

        mNavController.addOnDestinationChangedListener(this)

        binding.buttonAddField.setOnClickListener(this)
        binding.buttonAddTournament.setOnClickListener(this)
        binding.buttonSearch.setOnClickListener(this)
        binding.buttonClear.setOnClickListener(this)
        binding.buttonAddTeam.setOnClickListener(this)
        binding.buttonAddMatch.setOnClickListener(this)

        binding.textSearch.setPadding(smallMargin, smallMargin, smallMargin, smallMargin)
        binding.textSearch.addTextChangedListener(this)
        binding.textSearch.typeface = poppinsRegular

        binding.cardSearch.radius = smallMargin.toFloat()

        Handler().postDelayed({
            val width = binding.cardSearch.width
            binding.cardSearch.visibility = View.GONE
            binding.buttonClear.visibility = View.GONE
            binding.layoutToolbar.layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT
            binding.cardSearch.layoutParams.width = width
        }, 1000)

        setKeyboardVisibilityListener(this)
    }

    private fun setKeyboardVisibilityListener(callback: OnKeyboardVisibilityListener){
        val parentView =
            (findViewById<View>(android.R.id.content) as ViewGroup).getChildAt(0)
        parentView.viewTreeObserver.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
            private var alreadyOpen = false
            private val defaultKeyboardHeightDP = 100
            private val EstimatedKeyboardDP =
                defaultKeyboardHeightDP + 48
            private val rect: Rect = Rect()
            override fun onGlobalLayout() {
                val estimatedKeyboardHeight = TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    EstimatedKeyboardDP.toFloat(),
                    parentView.resources.displayMetrics
                ).toInt()
                parentView.getWindowVisibleDisplayFrame(rect)
                val heightDiff: Int =
                    parentView.rootView.height - (rect.bottom - rect.top)
                val isShown = heightDiff >= estimatedKeyboardHeight
                if (isShown == alreadyOpen) {
                    Log.i("Keyboard state", "Ignoring global layout change...")
                    return
                }
                alreadyOpen = isShown
                callback.onVisibilityChanged(isShown)
            }
        })
    }

    override fun onResume() {
        super.onResume()

        ViewModelProvider(this)[ReservationsViewModel::class.java].addObserver(this)
    }

    override fun onDestinationChanged(
        controller: NavController,
        destination: NavDestination,
        arguments: Bundle?
    ) {
        when (destination.id) {
            R.id.splashFragment -> {
                binding.toolbar.setToolbar()
            }
            R.id.mapFragment -> {
                binding.toolbar.setToolbar()
                binding.motionLayout.transitionToEnd()

                Handler().postDelayed({
                    binding.toolbar.setToolbar()
                }, 50)
            }
            R.id.reservationsFragment -> {
                if (mMainViewModel.isOwner()) {
                    binding.buttonAddField.visibility = View.GONE
                    binding.buttonAddTournament.visibility = View.GONE
                    binding.buttonSearch.visibility = View.VISIBLE
                    binding.buttonAddTeam.visibility = View.GONE
                    binding.buttonAddMatch.visibility = View.GONE
                }

                binding.toolbar.setToolbar(mediumFont, true)
            }
            R.id.webFragment -> {
                binding.toolbar.setToolbar(background = R.color.primaryDark)
            }
            R.id.profileFragment -> {
                if (mMainViewModel.isOwner()) {
                    binding.buttonAddField.visibility = View.GONE
                    binding.buttonAddTournament.visibility = View.GONE
                    binding.buttonSearch.visibility = View.GONE
                    binding.buttonAddTeam.visibility = View.GONE
                    binding.buttonAddMatch.visibility = View.GONE

                    clearSearchView(true)
                }

                binding.toolbar.setToolbar(background = R.color.primaryDark)
                binding.motionLayout.transitionToEnd()
                binding.toolbar.setTitleTextColor(getColor(R.color.primaryDark))
            }
            R.id.establishmentFragment -> {
                binding.motionLayout.transitionToStart()
            }
            R.id.favoritesFragment -> {
                binding.toolbar.setTitleTextColor(getColor(R.color.primaryDark))
                binding.motionLayout.transitionToStart()
            }
            R.id.establishmentMapFragment -> {
                binding.toolbar.title = mEstablishmentViewModel.getEstablishment().name
                binding.toolbar.setToolbar(mediumFont, true)
            }
            R.id.editProfileFragment -> {
                binding.toolbar.setToolbar(background = R.color.primaryDark)
            }
            R.id.loginFragment -> {
                binding.motionLayout.transitionToStart()
            }
            R.id.adminMainFragment -> {
                binding.buttonAddField.visibility = View.VISIBLE
                binding.buttonAddTournament.visibility = View.GONE
                binding.buttonSearch.visibility = View.GONE
                binding.buttonAddTeam.visibility = View.GONE
                binding.buttonAddMatch.visibility = View.GONE

                binding.toolbar.setToolbar(mediumFont, true)
                binding.motionLayout.transitionToEnd()

                clearSearchView(true)

                Handler().postDelayed({
                    binding.toolbar.setToolbar(mediumFont, true)
                }, 50)
            }
            R.id.tournamentsListFragment -> {
                binding.buttonAddTournament.visibility = View.VISIBLE
                binding.buttonAddField.visibility = View.GONE
                binding.buttonSearch.visibility = View.GONE
                binding.buttonAddTeam.visibility = View.GONE
                binding.buttonAddMatch.visibility = View.GONE

                binding.toolbar.setToolbar(mediumFont, true)

                clearSearchView(true)
            }
            R.id.termsAndConditionsFragment, R.id.metricsFragment -> {
                binding.toolbar.setToolbar(background = R.color.primaryDark)
            }
            R.id.fieldFragment -> {
                binding.buttonAddField.visibility = View.GONE
                destination.label = if (mEstablishmentViewModel.getField() != null)
                    getString(R.string.label_edit_field)
                else getString(R.string.label_new_field)
            }
            R.id.servicesFragment -> {
                binding.toolbar.setToolbar(mediumFont, background = R.color.accent)
            }
            R.id.tournamentFragment -> {
                binding.buttonAddTournament.visibility = View.GONE
                val name = mTournamentsViewModel.getTournament()!!.name
                destination.label = name
            }
            R.id.editTournamentFragment -> {
                binding.buttonAddTournament.visibility = View.GONE
                destination.label = if (mTournamentsViewModel.getTournament() == null)
                    getString(R.string.label_create_tournament)
                else getString(R.string.label_edit_tournament)
                binding.buttonAddTeam.visibility = View.GONE
            }
            R.id.teamsListFragment -> {
                if (arguments!!.getInt("mode") == TeamsListFragment.MODE_MULTI_SELECTION) {
                    binding.buttonAddTeam.visibility = View.VISIBLE
                    destination.label = getString(R.string.label_teams)
                } else {
                    destination.label = getString(R.string.label_pick_team)
                }
            }
            R.id.createTeamFragment -> {
                binding.buttonAddTeam.visibility = View.GONE
            }
            R.id.tournamentRulesFragment -> {
                binding.buttonAddTournament.visibility = View.GONE
            }
            R.id.calendarFragment -> {
                binding.buttonAddMatch.visibility = View.GONE
            }
            R.id.matchesListFragment -> {
                binding.buttonAddMatch.visibility = View.VISIBLE
            }
            R.id.matchFragment -> {
                binding.buttonAddMatch.visibility = View.GONE
                destination.label = mTournamentsViewModel.getMatchTitle(this)
            }
            R.id.typesListFragment -> {
                val label = when (arguments!!.getInt("mode")) {
                    TypesListFragment.MODE_GRASS_FIELD -> {
                        getString(R.string.label_grass_type)
                    }
                    else -> {
                        getString(R.string.label_game_type)
                    }
                }
                destination.label = label
            }
            R.id.tournamentStatsFragment -> {
                destination.label = mTournamentsViewModel.getTournament()!!.name
            }
        }
    }

    override fun onLoggedIn(user: User) {
        mMainViewModel.setUser(user)
        ViewModelProvider(this)[EstablishmentViewModel::class.java].setUserId(user.id)
        binding.navigationBottom.menu.clear()
        if (user.isOwner()) {
            binding.navigationBottom.inflateMenu(R.menu.menu_owner)
            binding.toolbar.setupWithNavController(
                mNavController, AppBarConfiguration(
                    setOf(
                        R.id.adminMainFragment,
                        R.id.reservationsFragment,
                        R.id.tournamentsListFragment,
                        R.id.profileFragment
                    )
                )
            )
        } else {
            binding.navigationBottom.inflateMenu(R.menu.menu_user)
            binding.toolbar.setupWithNavController(
                mNavController, AppBarConfiguration(
                    setOf(
                        R.id.mapFragment,
                        R.id.reservationsFragment,
                        R.id.webFragment,
                        R.id.profileFragment
                    )
                )
            )
        }
        binding.navigationBottom.setTypeface()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            LocationProvider.CODE_LOCATION_PERMISSION -> {
                mMainViewModel.onLocationEnabled(permissions.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            }
            TournamentViewModel.CODE_PERMISSION_READ_STORAGE, TournamentViewModel.CODE_PERMISSION_WRITE_STORAGE -> {
                ViewModelProvider(this)[TournamentViewModel::class.java]
                    .onRequestPermissionsResult(
                        requestCode,
                        permissions.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    )
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        ViewModelProvider(this)[LoginViewModel::class.java].getFacebookCallback()
            .onActivityResult(requestCode, resultCode, data)

        if (requestCode == LocationProvider.CODE_ENABLE_LOCATION) {
            mMainViewModel.onLocationEnabled(resultCode == Activity.RESULT_OK)
        }
    }

    override fun onClick(v: View?) {
        v ?: return

        when (v) {
            binding.buttonAddField -> {
                mEstablishmentViewModel.setField(null)

                mEstablishmentViewModel.gameTypeSelected = -1
                mEstablishmentViewModel.grassTypeSelected = -1

                mNavController.navigate(R.id.action_adminMainFragment_to_fieldFragment)
            }
            binding.buttonSearch -> {
                binding.buttonSearch.visibility = View.GONE
                binding.buttonClear.visibility = View.VISIBLE
                binding.cardSearch.circularReveal()

                for (view in binding.toolbar.children) {
                    if (view is TextView) {
                        view.visibility = View.INVISIBLE
                    }
                }
            }
            binding.buttonClear -> clearSearchView(false)
            binding.buttonAddTournament -> {
                mTournamentsViewModel.setTournament(null)
                mNavController.navigate(R.id.action_tournamentsListFragment_to_editTournamentFragment)
            }
            binding.buttonAddTeam -> {
                mNavController.navigate(R.id.action_teamsListFragment_to_createTeamFragment)
            }
            binding.buttonAddMatch -> {
                mTournamentsViewModel.setMatchSelected(null)
                mNavController.navigate(R.id.action_matchesListFragment_to_matchFragment)
            }
        }
    }

    private fun clearSearchView(hideSearch: Boolean) {
        hideKeyboard()
        binding.cardSearch.circularHide()
        binding.buttonClear.visibility = View.INVISIBLE

        binding.buttonSearch.visibility = if (hideSearch) View.GONE
        else View.VISIBLE

        ViewModelProvider(this)[ReservationsViewModel::class.java].clearFilters()

        Handler().postDelayed({
            for (view in binding.toolbar.children) {
                if (view is TextView) {
                    view.fadeIn()
                }
            }
            binding.buttonClear.visibility = View.GONE
        }, 400)
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        ViewModelProvider(this)[ReservationsViewModel::class.java]
            .filterReservations(s.toString().trim())
    }

    override fun onUpdateFilterText(text: String) {
        binding.textSearch.setText(text)
    }

    override fun onVisibilityChanged(isVisible: Boolean) {
        mMainViewModel.keyboardVisibilityChanged(isVisible)
    }
}

interface OnKeyboardVisibilityListener{
    fun onVisibilityChanged(isVisible: Boolean)
}
