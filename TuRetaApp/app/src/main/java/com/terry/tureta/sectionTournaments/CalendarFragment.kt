package com.terry.tureta.sectionTournaments

import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.terry.tureta.R
import com.terry.tureta.customViews.CustomFragment
import com.terry.tureta.databinding.FragmentCalendarBinding
import com.terry.tureta.mediumMargin
import com.terry.tureta.sectionTournaments.adapters.RoundsAdapter
import com.terry.tureta.sectionTournaments.viewModel.TournamentViewModel

class CalendarFragment : CustomFragment(), RoundsAdapter.IRoundsAdapter {

    private lateinit var binding: FragmentCalendarBinding

    private val adapter by lazy {
        RoundsAdapter(requireContext(), mViewModel.getTournament()!!.numberOfRounds, this)
    }

    private val mViewModel by lazy {
        ViewModelProvider(requireActivity())[TournamentViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCalendarBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recyclerRounds.layoutManager = LinearLayoutManager(context)
        binding.recyclerRounds.adapter = adapter
        binding.recyclerRounds.addItemDecoration(object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(
                outRect: Rect,
                view: View,
                parent: RecyclerView,
                state: RecyclerView.State
            ) {
                val position = parent.getChildAdapterPosition(view)
                val size = parent.adapter!!.itemCount

                var top = 0
                var bottom = 0

                if (position == 0) {
                    top = mediumMargin
                }

                if (position == size - 1) {
                    bottom = mediumMargin
                }

                outRect.run {
                    this.top = top
                    this.bottom = bottom
                }
            }
        })
    }

    override fun onRoundClicked(round: Int) {
        mViewModel.roundNumberSelected = round
        findNavController().navigate(R.id.action_calendarFragment_to_matchesListFragment)
    }

}