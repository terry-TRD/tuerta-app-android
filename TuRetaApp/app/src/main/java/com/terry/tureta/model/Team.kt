package com.terry.tureta.model

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.provider.MediaStore
import com.google.gson.annotations.SerializedName
import com.terry.tureta.BuildConfig
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.net.URI
import java.text.SimpleDateFormat
import java.util.*

class Team {
    var id: Int = -1
    var name: String = ""

    @SerializedName("media_url")
    var image: String = ""

    var selected: Boolean = false
    var path: String = ""
    var uri: Uri? = null

    fun getMultiPartData(context: Context): MultipartBody.Part {
        val file = getCompressed(context, this.path)
        return MultipartBody.Part.createFormData(
            "media", file.name,
            RequestBody.create(MediaType.parse("image/*"), file)
        )
    }

    /*
        compress the file/photo from @param <b>path</b> to a private location on the current device and return the compressed file.
        @param path = The original image path
        @param context = Current android Context
     */
    @Throws(IOException::class)
    private fun getCompressed(context: Context, path: String): File {
        //getting device external cache directory, might not be available on some devices,
        // so our code fall back to internal storage cache directory, which is always available but in smaller quantity
        var cacheDir = context.externalCacheDir
        if (cacheDir == null) //fall back
            cacheDir = context.cacheDir
        val rootDir = cacheDir!!.absolutePath + "/ImageCompressor"
        val root = File(rootDir)

        //Create ImageCompressor folder if it doesnt already exists.
        if (!root.exists()) root.mkdirs()

        //decode and resize the original bitmap from @param path.
//        val bitmap =
//            decodeImageFromFiles(path,  /* your desired width*/300,  /*your desired height*/300)
        val file = File(path)
        val bitmap = BitmapFactory.decodeFile(file.absolutePath)
            ?: MediaStore.Images.Media.getBitmap(context.contentResolver, uri!!)

        //create placeholder for the compressed image file
        val compressed =
            File(root, SimpleDateFormat("yyyyMMdd_HHmmss").format(Date()).toString() + ".jpg")

        //convert the decoded bitmap to stream
        val byteArrayOutputStream = ByteArrayOutputStream()

        /*compress bitmap into byteArrayOutputStream
            Bitmap.compress(Format, Quality, OutputStream)
            Where Quality ranges from 1 - 100.
         */
        val quality = if (BuildConfig.DEBUG) 5 else 80
        bitmap.compress(Bitmap.CompressFormat.JPEG, quality, byteArrayOutputStream)

        /*
        Right now, we have our bitmap inside byteArrayOutputStream Object, all we need next is to write it to the compressed file we created earlier,
        java.io.FileOutputStream can help us do just That!
         */
        val fileOutputStream = FileOutputStream(compressed)
        fileOutputStream.write(byteArrayOutputStream.toByteArray())
        fileOutputStream.flush()
        fileOutputStream.close()

        //File written, return to the caller. Done!
        return compressed
    }

}