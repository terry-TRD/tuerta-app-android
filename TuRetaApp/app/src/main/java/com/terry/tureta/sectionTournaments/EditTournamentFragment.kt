package com.terry.tureta.sectionTournaments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.terry.tureta.R
import com.terry.tureta.customViews.CustomFragment
import com.terry.tureta.databinding.FragmentEditTournamentBinding
import com.terry.tureta.dialogs.CustomBottomDialog
import com.terry.tureta.mediumMargin
import com.terry.tureta.sectionFields.TypesListFragment
import com.terry.tureta.sectionTeams.TeamsListFragment
import com.terry.tureta.sectionTournaments.viewModel.TournamentViewModel
import grid.apps.supplies_admin.Extensions.setMargins

class EditTournamentFragment : CustomFragment() {

    private lateinit var binding: FragmentEditTournamentBinding

    private val mViewModel by lazy {
        ViewModelProvider(requireActivity())[TournamentViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentEditTournamentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.textName.addTextChangedListener(this)
        setEditText(binding.layoutName, binding.textName)
        binding.layoutName.setMargins(mediumMargin, mediumMargin, mediumMargin)

        binding.buttonCreateTournament.setMargins(mediumMargin, 0, mediumMargin, mediumMargin)
        binding.buttonDeleteTournament.setMargins(mediumMargin, 0, mediumMargin, mediumMargin)

        binding.buttonTeams.setOnClickListener(this)
        binding.buttonGameType.setOnClickListener(this)
        binding.buttonRules.setOnClickListener(this)
        binding.buttonCreateTournament.setOnClickListener(this)
        binding.buttonDeleteTournament.setOnClickListener(this)

        binding.scrollView.setMargins(0, 0, 0, mediumMargin)
    }

    override fun onResume() {
        super.onResume()

        val tournament = mViewModel.getTournament()
        tournament ?: return

        binding.textName.setText(tournament.name)
        binding.buttonCreateTournament.updateText(R.string.label_update_tournament)
        binding.buttonDeleteTournament.visibility = View.VISIBLE
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        binding.layoutName.error = ""
    }

    override fun onClick(v: View?) {
        v ?: return
        when (v) {
            binding.buttonTeams -> {
                if (mViewModel.getTournament() == null) {
                    showToast("El torneo debe ser creado primero.")
                } else {
                    val action =
                        EditTournamentFragmentDirections.actionEditTournamentFragmentToTeamsListFragment(
                            TeamsListFragment.MODE_MULTI_SELECTION
                        )
                    findNavController().navigate(action)
                }
            }
            binding.buttonGameType -> {
                val action =
                    EditTournamentFragmentDirections.actionEditTournamentFragmentToTypesListFragment(
                        TypesListFragment.MODE_PLAYERS_TOURNAMENT
                    )
                findNavController().navigate(action)
            }
            binding.buttonRules -> {
                findNavController().navigate(R.id.action_editTournamentFragment_to_tournamentRulesFragment)
            }
            binding.buttonCreateTournament -> {
                if (validateData()) {

                    val action =
                        EditTournamentFragmentDirections.actionEditTournamentFragmentToLoadDialog()

                    if (mViewModel.getTournament() == null) {
                        mViewModel.createTournament(binding.textName.text.toString().trim())
                        action.message = getString(R.string.label_creating_tournament)
                    } else {
                        mViewModel.updateTournament(binding.textName.text.toString().trim())
                        action.message = getString(R.string.label_updating_tournament)
                    }

                    findNavController().navigate(action)
                }
            }
            binding.buttonDeleteTournament -> {
                val action =
                    EditTournamentFragmentDirections.actionEditTournamentFragmentToCustomBottomDialog(
                        CustomBottomDialog.MODE_DELETE_TOURNAMENT
                    )
                findNavController().navigate(action)
            }
        }
    }

    private fun validateData(): Boolean {
        if (binding.textName.text.toString().trim().isEmpty()) {
            binding.layoutName.error = getString(R.string.label_tournament_name_empty)
            return false
        }

//        if (mViewModel.getTeams() == null || mViewModel.getTeams()!!.size < 2) {
//            showToast(getString(R.string.message_add_two_teams_first))
//            return false
//        }

        if (mViewModel.getGameType() == -1) {
            showToast(getString(R.string.message_game_type_empty))
            return false
        }

        return true
    }

}