package com.terry.tureta.sectionMatches

import android.graphics.Rect
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.terry.tureta.R
import com.terry.tureta.customViews.CustomFragment
import com.terry.tureta.databinding.FragmentMatchesListBinding
import com.terry.tureta.mediumMargin
import com.terry.tureta.model.Match
import com.terry.tureta.sectionMatches.adapters.MatchesAdapter
import com.terry.tureta.sectionTournaments.viewModel.TournamentViewModel
import com.terry.tureta.sectionMain.viewModel.MainViewModel

class MatchesListFragment : CustomFragment(), MatchesAdapter.IMatchesAdapter {

    private lateinit var binding: FragmentMatchesListBinding

    private val mViewModel by lazy {
        ViewModelProvider(requireActivity())[TournamentViewModel::class.java]
    }

    private val list: ArrayList<Any> = ArrayList()
    private val adapter by lazy {
        MatchesAdapter(requireContext(), list, this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMatchesListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recyclerMatches.adapter = adapter
        binding.recyclerMatches.layoutManager = LinearLayoutManager(context)
        binding.recyclerMatches.addItemDecoration(object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(
                outRect: Rect,
                view: View,
                parent: RecyclerView,
                state: RecyclerView.State
            ) {
                val position = parent.getChildAdapterPosition(view)
                val size = parent.adapter!!.itemCount

                var top = 0
                var bottom = 0

                if (position == 0) {
                    top = mediumMargin
                }

                if (position == size - 1) {
                    bottom = mediumMargin
                }

                outRect.top = top
                outRect.bottom = bottom
            }
        })
    }

    override fun onResume() {
        super.onResume()

        mViewModel.addObserver(this)
        Handler().postDelayed({
            mViewModel.getMatchesList(mViewModel.roundNumberSelected)
        }, 100)
    }

    override fun onPause() {
        super.onPause()

        mViewModel.deleteObserver(this)
    }

    override fun onMatchListSuccess(list: List<Match>) {
        this.list.clear()
        if (list.isNotEmpty()) {
            var lastPosition = 0
            var headers = 1
            this.list.add(list[0].getDate(requireContext()))

            for ((count, match) in list.withIndex()) {
                if (match.getDate(requireContext()) != this.list[lastPosition]) {
                    this.list.add(match.getDate(requireContext()))
                    lastPosition = count + headers
                    headers++
                }
                if (lastPosition == count + (headers - 1)) {
                    match.showSeparator = false
                }
                this.list.add(match)
            }

            adapter.notifyDataSetChanged()
        } else {
            //TODO: display a message and a button to create a new match
        }
    }

    override fun onMatchListFailed(message: Int) {
        showToast(message)
    }

    override fun onMatchSelected(match: Match) {
        val user = ViewModelProvider(requireActivity())[MainViewModel::class.java].getUser()
        if (user.isOwner()) {
            val fields = user.getEstablishment().fields.filter { it.isActive }

            var position = -1
            for (count in fields.indices) {
                if (match.field != null && match.field!!.id == fields[count].id) {
                    position = count
                    break
                }
            }

            mViewModel.fieldSelectedPosition = position
            mViewModel.setMatchSelected(match)
            findNavController().navigate(R.id.action_matchesListFragment_to_matchFragment)
        }
    }

}