package com.terry.tureta.model

import com.google.gson.annotations.SerializedName

class TournamentStats{

    var team: Team = Team()
    @SerializedName("jj")
    var gamesPlayed: Int = 0
    @SerializedName("jg")
    var gamesWon: Int = 0
    @SerializedName("je")
    var gamesTied: Int = 0
    @SerializedName("jp")
    var gamesLost: Int = 0
    @SerializedName("dif")
    var difference: Int = 0
    @SerializedName("pts")
    var points: Int = 0

}