package com.terry.tureta.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.terry.tureta.*
import com.terry.tureta.databinding.ItemFieldFavoriteBinding
import com.terry.tureta.databinding.ItemSearchFieldBinding
import com.terry.tureta.model.Establishment
import com.terry.tureta.sectionFavorites.viewHolder.VHEstablishmentFavorite
import grid.apps.supplies_admin.Extensions.*

class EstablishmentsAdapter(
    private val context: Context,
    private val list: List<Establishment>,
    private val callback: IEstablishmentsAdapter,
    private val style: Int
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val STYLE_SEARCH = 100
        const val STYLE_FAVORITE = 200
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (style) {
            STYLE_SEARCH -> VHEstablishmentSearch(
                ItemSearchFieldBinding.inflate(
                    LayoutInflater.from(context), parent, false
                )
            )
            else -> VHEstablishmentFavorite(
                ItemFieldFavoriteBinding
                    .inflate(
                        LayoutInflater.from(context), parent, false
                    )
                , context, callback
            )
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is VHEstablishmentSearch) holder.bindData(list[position], position)
        else if (holder is VHEstablishmentFavorite) holder.bindData(list[position], position)
    }

    inner class VHEstablishmentSearch(private val binding: ItemSearchFieldBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.textName.smallRegular(R.color.black)
            binding.textName.setMargins(mediumMargin, smallMargin, mediumMargin)

            binding.textLocation.extraSmallRegular(R.color.grayText)
            binding.textLocation.setMargins(mediumMargin, 0, mediumMargin, smallMargin)

            binding.separator.setMargins(mediumMargin)
        }

        fun bindData(establishment: Establishment, position: Int) {
            binding.textName.text = establishment.name
            binding.textLocation.text = establishment.address
            binding.layoutItem.setOnClickListener { callback.onEstablishmentSelected(establishment) }
            binding.separator.visibility = if (position == 0) View.INVISIBLE else View.VISIBLE
        }

    }

    interface IEstablishmentsAdapter {
        fun onEstablishmentSelected(establishment: Establishment)
    }

}