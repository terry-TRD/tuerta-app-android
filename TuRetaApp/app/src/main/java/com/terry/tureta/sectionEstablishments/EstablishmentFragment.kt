package com.terry.tureta.sectionEstablishments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.BounceInterpolator
import android.view.animation.ScaleAnimation
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.squareup.picasso.Picasso
import com.terry.tureta.*
import com.terry.tureta.extensions.setStyle
import com.terry.tureta.customViews.CustomFragment
import com.terry.tureta.databinding.FragmentEstablishmentBinding
import com.terry.tureta.model.Establishment
import com.terry.tureta.sectionEstablishments.adapters.EstablishmentFragmentsAdapter
import com.terry.tureta.sectionEstablishments.viewModel.EstablishmentViewModel
import com.terry.tureta.sectionMain.viewModel.MainViewModel
import grid.apps.supplies_admin.Extensions.*
import io.reactivex.disposables.Disposable

class EstablishmentFragment : CustomFragment() {

    private var detailsDisposable: Disposable? = null
    private var dateDisposable: Disposable? = null

    private lateinit var binding: FragmentEstablishmentBinding
    private lateinit var establishment: Establishment

    private val mViewModel by lazy {
        activity?.run {
            val model = ViewModelProvider(this)[EstablishmentViewModel::class.java]
            model.setActivity(this)
            model
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentEstablishmentBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onResume() {
        super.onResume()

        mViewModel?.addObserver(this)
    }

    override fun onPause() {
        super.onPause()

        mViewModel?.deleteObserver(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.establishment = mViewModel!!.getEstablishment()
        setUI()
//        detailsDisposable = detailsController.initializeDetails()
    }

    private fun setUI() {
        Picasso.get().load(establishment.image).into(binding.imageCourt)

        setRating(establishment.rate)
        binding.layoutRating.setMargins(mediumMargin, 0, 0, mediumMargin)

        binding.textName.largeBold()
        binding.textName.setMargins(mediumMargin, smallMargin, mediumMargin)
        binding.textName.text = establishment.name

        binding.viewPager.adapter =
            EstablishmentFragmentsAdapter(requireContext(), childFragmentManager)

        binding.tabLayout.setupWithViewPager(binding.viewPager)
        binding.tabLayout.setStyle()

        binding.buttonFavorite.isChecked = mViewModel!!.getEstablishment().isFavorite
        binding.buttonFavorite.setOnClickListener {
            binding.buttonFavorite.isEnabled = false

            val anim = ScaleAnimation(0.8f, 1f, 0.8f, 1f)
            anim.duration = 300
            anim.interpolator = BounceInterpolator()

            binding.buttonFavorite.startAnimation(anim)

            mViewModel?.setFavorite(binding.buttonFavorite.isChecked)
        }
    }

    override fun onFavoriteFinished(isSuccessful: Boolean) {
        binding.buttonFavorite.isEnabled = true
        if(!isSuccessful){
            binding.buttonFavorite.isChecked = !binding.buttonFavorite.isChecked
            showToast(R.string.message_favorite_error)
        } else {
            establishment.isFavorite = binding.buttonFavorite.isChecked
            ViewModelProvider(requireActivity())[MainViewModel::class.java].updateFavorites(establishment)
        }
    }

    private fun setRating(rate: Int) {
        val gray = ContextCompat.getColor(requireContext(), R.color.grayText)
        val green = ContextCompat.getColor(requireContext(), R.color.accent)
        when (rate) {
            0 -> {
                binding.star1.setColorFilter(gray)
                binding.star2.setColorFilter(gray)
                binding.star3.setColorFilter(gray)
                binding.star4.setColorFilter(gray)
                binding.star5.setColorFilter(gray)
            }
            1 -> {
                binding.star1.setColorFilter(green)
                binding.star2.setColorFilter(gray)
                binding.star3.setColorFilter(gray)
                binding.star4.setColorFilter(gray)
                binding.star5.setColorFilter(gray)
            }
            2 -> {
                binding.star1.setColorFilter(green)
                binding.star2.setColorFilter(green)
                binding.star3.setColorFilter(gray)
                binding.star4.setColorFilter(gray)
                binding.star5.setColorFilter(gray)
            }
            3 -> {
                binding.star1.setColorFilter(green)
                binding.star2.setColorFilter(green)
                binding.star3.setColorFilter(green)
                binding.star4.setColorFilter(gray)
                binding.star5.setColorFilter(gray)
            }
            4 -> {
                binding.star1.setColorFilter(green)
                binding.star2.setColorFilter(green)
                binding.star3.setColorFilter(green)
                binding.star4.setColorFilter(green)
                binding.star5.setColorFilter(gray)
            }
            5 -> {
                binding.star1.setColorFilter(green)
                binding.star2.setColorFilter(green)
                binding.star3.setColorFilter(green)
                binding.star4.setColorFilter(green)
                binding.star5.setColorFilter(green)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        if (detailsDisposable != null) {
            detailsDisposable!!.dispose()
        }
        if (dateDisposable != null) {
            dateDisposable!!.dispose()
        }
    }

}
