package com.terry.tureta.sectionWalkthrough

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import com.terry.tureta.R

import com.terry.tureta.customViews.CustomFragment
import com.terry.tureta.databinding.FragmentWalkthroughBinding
import com.terry.tureta.largeMargin
import com.terry.tureta.mediumMargin
import grid.apps.supplies_admin.Extensions.setMargins

class WalkthroughFragment : CustomFragment() {

    private lateinit var binding: FragmentWalkthroughBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentWalkthroughBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonSkip.setMargins(0, mediumMargin, mediumMargin)
        binding.buttonSkip.setOnClickListener(this)

        binding.buttonSignIn.setMargins(mediumMargin, 0, mediumMargin)
        binding.buttonSignIn.setOnClickListener(this)

        binding.buttonLogin.setMargins(0, 0, 0, largeMargin)
        binding.buttonLogin.setOnClickListener(this)

        binding.recycler.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.recycler.adapter = WalkthroughAdapter(requireContext())
        PagerSnapHelper().attachToRecyclerView(binding.recycler)
        binding.scrollIndicator.attachToRecyclerView(binding.recycler)
        binding.scrollIndicator.setMargins(0, 0, 0, largeMargin)
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.buttonSkip,
            binding.buttonLogin -> {
                findNavController().navigate(R.id.action_walkthroughFragment_to_loginFragment)
            }

            binding.buttonSignIn -> {
                findNavController().navigate(R.id.action_walkthroughFragment_to_registerFragment)
            }
        }
    }

}
