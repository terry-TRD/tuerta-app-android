package com.terry.tureta.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.terry.tureta.R
import com.terry.tureta.customViews.CustomDialog
import com.terry.tureta.databinding.DialogTimePickerBinding
import com.terry.tureta.mediumMargin
import com.terry.tureta.sectionTournaments.viewModel.TournamentViewModel
import grid.apps.supplies_admin.Extensions.setMargins
import java.util.*

class TimePickerDialog : CustomDialog() {

    private lateinit var binding: DialogTimePickerBinding

    private var hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY)
    private var minute = Calendar.getInstance().get(Calendar.MINUTE)

    private val mViewModel by lazy {
        ViewModelProvider(requireActivity())[TournamentViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogTimePickerBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonContinue.setMargins(mediumMargin, mediumMargin, mediumMargin)
        binding.buttonCancel.setMargins(mediumMargin, mediumMargin * 2, mediumMargin)

        binding.buttonCancel.setOnClickListener(this)
        binding.buttonContinue.setOnClickListener(this)

        binding.cardBackground.setMargins(0, 0, 0, mediumMargin)

        binding.timePicker.setOnTimeChangedListener { _, hourOfDay, minute ->
            this.hour = hourOfDay
            this.minute = minute
        }
    }

    override fun onClick(v: View?) {
        v ?: return
        when (v) {
            binding.buttonContinue -> {
                val hour = if (this.hour < 10) "0${this.hour}" else this.hour.toString()
                val minute = if (this.minute < 10) "0${this.minute}" else this.minute.toString()
                mViewModel.hourSelected = "$hour:$minute hrs"
                findNavController().navigate(R.id.action_timePickerDialog_to_matchFragment)
            }
            binding.buttonCancel -> {
                findNavController().navigate(R.id.action_timePickerDialog_to_matchFragment)
            }
        }
    }

}