package com.terry.tureta.sectionLogin

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.terry.tureta.R
import com.terry.tureta.customViews.CustomFragment
import com.terry.tureta.model.User
import com.terry.tureta.sectionLogin.viewModel.LoginViewModel

class SplashFragment : CustomFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_splash, container, false)
    }

    override fun onResume() {
        super.onResume()

        Handler().postDelayed({
            ViewModelProvider(requireActivity())[LoginViewModel::class.java].addObserver(this@SplashFragment)
            ViewModelProvider(requireActivity())[LoginViewModel::class.java].initializeUserSession(
                requireContext()
            )
        }, 2000)
    }

    override fun onPause() {
        super.onPause()

        ViewModelProvider(requireActivity())[LoginViewModel::class.java].deleteObserver(this@SplashFragment)
    }

    override fun onLoggedIn(user: User) {
        if (user.isOwner())
            findNavController().navigate(R.id.action_splashFragment_to_adminMain)
        else
            findNavController().navigate(R.id.action_splashFragment_to_userMain)
    }

    override fun goToWalkthrough() {
        findNavController().navigate(R.id.action_splashFragment_to_walkthroughFragment)
    }

}
