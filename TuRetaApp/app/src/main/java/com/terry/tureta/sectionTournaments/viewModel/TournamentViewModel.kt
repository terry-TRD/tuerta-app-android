package com.terry.tureta.sectionTournaments.viewModel

import android.content.Context
import android.util.Log
import androidx.lifecycle.ViewModel
import com.terry.tureta.R
import com.terry.tureta.api.ApiUtils
import com.terry.tureta.model.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.*
import kotlin.collections.ArrayList

class TournamentViewModel : ViewModel() {

    companion object {
        const val CODE_PERMISSION_WRITE_STORAGE = 200
        const val CODE_PERMISSION_READ_STORAGE = 300
    }

    private val mObservers = ArrayList<ITournamentsObserver>()

    private var mTournament: Tournament? = null
    private var mMatch: Match? = null
    private var mTeams: ArrayList<Team>? = null
    private var mGameTypeSelected: Int = -1

    var mMatchWonPoints: Int = 0
    var mMatchTiePoints: Int = 0
    var mMatchLosePoints: Int = 0
    var mNumberOfRounds: Int = 0

    private var establishmentId: Int = -1

    private lateinit var tournamentsDisposable: Disposable
    private lateinit var teamsDisposable: Disposable
    private lateinit var matchDisposable: Disposable

    var fieldSelectedPosition: Int = -1
    var fieldSelected: Field? = null

    var hourSelected: String = ""
    private var dateSelected: Calendar? = null

    var roundNumberSelected: Int = -1

    private val matchTeams: Array<Team?> = Array(2) { null }

    fun downloadTournaments() {
        tournamentsDisposable = ApiUtils.apiTournaments
            .getTournaments(establishmentId)
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    for (observer in mObservers) {
                        observer.onTournamentsLoaded(result.data!!)
                    }

                    tournamentsDisposable.dispose()
                },
                { error ->
                    for (observer in mObservers) {
                        observer.onTournamentsFailed(R.string.message_tournaments_failed)
                    }
                    tournamentsDisposable.dispose()
                },
                { Log.d("TAG", "completed") }
            )
    }

//    fun downloadTeams() {
//        teamsDisposable = ApiUtils.apiTournaments
//            .getTeams(establishmentId)
//            .subscribeOn(Schedulers.io())
//            .unsubscribeOn(Schedulers.computation())
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribe(
//                { result ->
//                    for (observer in mObservers) {
//                        observer.onTeamsLoaded(result.data!!)
//                    }
//
//                    tournamentsDisposable.dispose()
//                },
//                { error ->
//                    for (observer in mObservers) {
//                        observer.onTeamsFailed(R.string.message_teams_list_failed)
//                    }
//                    tournamentsDisposable.dispose()
//                },
//                { Log.d("TAG", "completed") }
//            )
//    }

    fun downLoadTournamentTeams() {
        teamsDisposable = ApiUtils.apiTournaments
            .getTournamentTeams(establishmentId, mTournament!!.id)
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    for (observer in mObservers) {
                        observer.onTeamsLoaded(result.data!!)
                    }

                    tournamentsDisposable.dispose()
                },
                { error ->
                    for (observer in mObservers) {
                        observer.onTeamsFailed(R.string.message_teams_list_failed)
                    }
                    tournamentsDisposable.dispose()
                },
                { Log.d("TAG", "completed") }
            )
    }

    fun createTeam(team: Team, context: Context) {
        teamsDisposable = ApiUtils.apiTournaments
            .createTeam(
                establishmentId,
                team.name,
                mTournament!!.id,
                team.getMultiPartData(context)
            )
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    for (observer in mObservers) {
                        observer.onTeamCreated(true)
                    }

                    tournamentsDisposable.dispose()
                },
                { error ->
                    for (observer in mObservers) {
                        observer.onTeamCreated(false)
                    }

                    tournamentsDisposable.dispose()
                },
                { Log.d("TAG", "completed") }
            )
    }

    fun createTournament(name: String) {
        val gameType = if (mGameTypeSelected == 0) 7 else 11

        tournamentsDisposable = ApiUtils.apiTournaments
            .createTournament(
                establishmentId,
                name,
                gameType,
                mMatchWonPoints,
                mMatchTiePoints,
                mMatchLosePoints,
                mNumberOfRounds
            )
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    for (observer in mObservers) {
                        observer.onTournamentFinished(
                            true,
                            R.string.message_tournament_created_success
                        )
                    }

                    tournamentsDisposable.dispose()
                },
                { error ->
                    for (observer in mObservers) {
                        observer.onTournamentFinished(
                            false,
                            R.string.message_tournament_created_fail
                        )
                    }

                    tournamentsDisposable.dispose()
                },
                { Log.d("TAG", "completed") }
            )
    }

    fun updateTournament(name: String) {
        val teams = ArrayList<Int>()
        for (team in mTeams!!) {
            teams.add(team.id)
        }

        val gameType = if (mGameTypeSelected == 0) 7 else 11

        tournamentsDisposable = ApiUtils.apiTournaments
            .updateTournament(
                establishmentId,
                mTournament!!.id,
                name,
                gameType,
                mMatchWonPoints,
                mMatchTiePoints,
                mMatchLosePoints,
                mNumberOfRounds,
                teams
            )
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    for (observer in mObservers) {
                        observer.onTournamentFinished(
                            true,
                            R.string.message_tournament_updated_success
                        )
                    }

                    tournamentsDisposable.dispose()
                },
                { error ->
                    for (observer in mObservers) {
                        observer.onTournamentFinished(
                            false,
                            R.string.message_tournament_updated_fail
                        )
                    }

                    tournamentsDisposable.dispose()
                },
                { Log.d("TAG", "completed") }
            )
    }

    fun deleteTournament() {
        tournamentsDisposable = ApiUtils.apiTournaments
            .deleteTournament(
                establishmentId,
                mTournament!!.id
            )
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    for (observer in mObservers) {
                        observer.onTournamentFinished(
                            true,
                            R.string.message_tournament_delete_success
                        )
                    }

                    tournamentsDisposable.dispose()
                },
                { error ->
                    for (observer in mObservers) {
                        observer.onTournamentFinished(
                            false,
                            R.string.message_tournament_delete_fail
                        )
                    }

                    tournamentsDisposable.dispose()
                },
                { Log.d("TAG", "completed") }
            )
    }

    fun deleteTeam(team: Team) {
        teamsDisposable = ApiUtils.apiTournaments
            .deleteTeam(
                establishmentId,
                team.id
            )
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    for (observer in mObservers) {
                        observer.onTeamDeleted(true, R.string.message_team_deleted_success)
                    }

                    tournamentsDisposable.dispose()
                },
                { error ->
                    for (observer in mObservers) {
                        observer.onTeamDeleted(false, R.string.message_team_deleted_failed)
                    }

                    tournamentsDisposable.dispose()
                },
                { Log.d("TAG", "completed") }
            )
    }

    fun getMatchesList(round: Int) {
        matchDisposable = ApiUtils.apiTournaments
            .getMatches(establishmentId, mTournament!!.id, round)
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    mTournament!!.matches.clear()
                    mTournament!!.matches.addAll(result.data!!)

                    for (observer in mObservers) {
                        observer.onMatchListSuccess(result.data!!)
                    }

                    matchDisposable.dispose()
                },
                { error ->
                    for (observer in mObservers) {
                        observer.onMatchListFailed(R.string.message_match_list_error)
                    }

                    matchDisposable.dispose()
                },
                { Log.d("TAG", "completed") }
            )
    }

    fun createMatch(team1Annotations: Int, team2Annotations: Int) {
        val year = dateSelected!!.get(Calendar.YEAR)
        val month = dateSelected!!.get(Calendar.MONTH)
        val day = dateSelected!!.get(Calendar.DAY_OF_MONTH)

        val textDay = if (day < 10) "0$day" else day.toString()
        val monthText = if (month < 10) "0$month" else month.toString()

        val parts = hourSelected.split(' ')
        val hoursParts = parts[0].split(':')

        val date = "$year-$monthText-$textDay ${hoursParts[0]}:${hoursParts[1]}:00"

        matchDisposable = ApiUtils.apiTournaments
            .createMatch(
                establishmentId,
                mTournament!!.id,
                date,
                roundNumberSelected,
                matchTeams[0]!!.id,
                matchTeams[1]!!.id,
                fieldSelected!!.id,
                team1Annotations,
                team2Annotations
            )
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    for (observer in mObservers) {
                        observer.onMatchFinished(true, R.string.message_create_match_success)
                    }

                    matchDisposable.dispose()
                },
                { error ->
                    for (observer in mObservers) {
                        observer.onMatchFinished(false, R.string.message_create_match_fail)
                    }

                    matchDisposable.dispose()
                },
                { Log.d("TAG", "completed") }
            )
    }

    fun updateMatch(team1Annotations: Int, team2Annotations: Int) {
        val year = dateSelected!!.get(Calendar.YEAR)
        val month = dateSelected!!.get(Calendar.MONTH)
        val day = dateSelected!!.get(Calendar.DAY_OF_MONTH)

        val textDay = if (day < 10) "0$day" else day.toString()
        val monthText = if (month < 10) "0$month" else month.toString()

        val parts = hourSelected.split(' ')
        val hoursParts = parts[0].split(':')

        val date = "$year-$monthText-$textDay ${hoursParts[0]}:${hoursParts[1]}:00"

        matchDisposable = ApiUtils.apiTournaments
            .updateMatch(
                establishmentId,
                mTournament!!.id,
                mMatch!!.id,
                date,
                roundNumberSelected,
                matchTeams[0]!!.id,
                matchTeams[1]!!.id,
                fieldSelected!!.id,
                team1Annotations,
                team2Annotations
            )
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    for (observer in mObservers) {
                        observer.onMatchFinished(true, R.string.message_match_update_success)
                    }

                    matchDisposable.dispose()
                },
                { error ->
                    for (observer in mObservers) {
                        observer.onMatchFinished(false, R.string.message_match_update_fail)
                    }

                    matchDisposable.dispose()
                },
                { Log.d("TAG", "completed") }
            )
    }

    fun getTournamentStats() {
        tournamentsDisposable = ApiUtils.apiTournaments
            .getTournamentStats(
                establishmentId,
                mTournament!!.id
            )
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    for (observer in mObservers) {
                        observer.onTournamentStatsSuccess(result.data!!)
                    }

                    tournamentsDisposable.dispose()
                },
                { error ->
                    for (observer in mObservers) {
                        observer.onTournamentStatsFailed(R.string.message_tournament_stats_failed)
                    }

                    tournamentsDisposable.dispose()
                },
                { Log.d("TAG", "completed") }
            )
    }

    fun onRequestPermissionsResult(requestCode: Int, isGranted: Boolean) {
        android.os.Handler().postDelayed({
            for (observer in mObservers) {
                observer.onRequestPermissionResult(requestCode, isGranted)
            }
        }, 100)
    }

    fun setEstablishmentId(establishmentId: Int) {
        this.establishmentId = establishmentId
    }

    fun setTournament(tournament: Tournament?) {
        this.mTournament = tournament

        mTeams = if (tournament == null) null else ArrayList(mTournament!!.teams)
        mGameTypeSelected = if (tournament != null) {
            if (tournament.gameType == 7) 0 else 1
        } else -1

        if (tournament != null) {
            mMatchWonPoints = mTournament!!.matchWonPoints
            mMatchTiePoints = mTournament!!.matchTiePoints
            mMatchLosePoints = mTournament!!.matchLosePoints
            mNumberOfRounds = mTournament!!.numberOfRounds
        } else {
            mMatchWonPoints = 0
            mMatchTiePoints = 0
            mMatchLosePoints = 0
            mNumberOfRounds = 0
        }
    }

    fun getTournament(): Tournament? = this.mTournament

    fun addObserver(observer: ITournamentsObserver) {
        this.mObservers.add(observer)
    }

    fun deleteObserver(observer: ITournamentsObserver) {
        this.mObservers.remove(observer)
    }

    fun getTeams(): ArrayList<Team>? = mTeams

    fun addTeam(team: Team) {
        if (mTeams == null) mTeams = ArrayList()
        mTeams!!.add(team)
    }

    fun removeTeam(team: Team) {
        mTeams?.removeAll { it.id == team.id }
    }

    fun getGameType(): Int = mGameTypeSelected

    fun setGameType(gameType: Int) {
        this.mGameTypeSelected = gameType
    }

    fun setMatchTeam(index: Int, team: Team) {
        matchTeams[index] = team
    }

    fun getFirstTeam(): Team? = matchTeams[0]

    fun getSecondTeam(): Team? = matchTeams[1]

    fun setDateSelected(date: Calendar) {
        dateSelected = date
        for (observer in mObservers) {
            observer.onDateUpdated(date)
        }
    }

    fun getDateSelected(): Calendar? = dateSelected

    fun getMatchSelected(): Match? = mMatch

    fun getDateSelectedString(context: Context): String {
        return if (dateSelected != null) {

            val weekDay = dateSelected!!.get(Calendar.DAY_OF_WEEK)
            val monthNumber = dateSelected!!.get(Calendar.MONTH)
            val year = dateSelected!!.get(Calendar.YEAR)

            val month =
                context.resources.getStringArray(R.array.date_months)[dateSelected!!.get(Calendar.MONTH)]

            val day = context.resources.getStringArray(R.array.day_of_the_week)[weekDay - 1]

            "$day ${dateSelected!!.get(Calendar.DAY_OF_MONTH)} de $month"
        } else context.getString(R.string.label_add_match_date)
    }

    fun getHourSelectedString(context: Context): String {
        return if (hourSelected.isEmpty()) context.getString(R.string.label_add_match_hour)
        else hourSelected
    }

    fun getFieldSelectedString(context: Context): String {
        return if (fieldSelected != null) context.getString(
            R.string.label_field_name,
            fieldSelected!!.name
        ) else context.getString(R.string.label_add_match_field)
    }

    fun getMatchTitle(context: Context): String =
        if (mMatch == null) context.getString(
            R.string.label_match_title,
            mTournament!!.matches.size + 1,
            roundNumberSelected
        ) else findMatchOnList(context)

    private fun findMatchOnList(context: Context): String {
        var position = 0
        for (count in 0 until mTournament!!.matches.size) {
            if (mMatch!!.id == mTournament!!.matches[count].id) {
                position = count
                break
            }
        }
        return context.getString(
            R.string.label_match_title,
            position + 1,
            roundNumberSelected
        )
    }

    fun setMatchSelected(match: Match?) {
        if (match == null) {
            matchTeams[0] = null
            matchTeams[1] = null
            hourSelected = ""
            fieldSelected = null
            fieldSelectedPosition = -1
            dateSelected = null
            mMatch = null
        } else {
            mMatch = match
            matchTeams[0] = match.teams[0]
            matchTeams[1] = match.teams[1]
            fieldSelected = match.field

//            fieldSelectedPosition = -1

            val parts = match.date.split(' ')
            val hourParts = parts[1].split(':')
            val dateParts = parts[0].split('-')

            hourSelected = "${hourParts[0]}:${hourParts[1]} hrs"
            dateSelected = Calendar.getInstance()
            dateSelected!!.set(dateParts[0].toInt(), dateParts[1].toInt(), dateParts[2].toInt())
        }
    }

    interface ITournamentsObserver {
        fun onRequestPermissionResult(requestCode: Int, isGranted: Boolean)
        fun onTournamentsLoaded(list: List<Tournament>)
        fun onTournamentsFailed(message: Int)
        fun onTeamsLoaded(list: List<Team>)
        fun onTeamsFailed(message: Int)
        fun onTeamCreated(isSuccess: Boolean)
        fun onTournamentFinished(isSuccess: Boolean, message: Int)
        fun onMatchListSuccess(list: List<Match>)
        fun onMatchListFailed(message: Int)
        fun onDateUpdated(date: Calendar)
        fun onMatchFinished(isSuccess: Boolean, message: Int)
        fun onTournamentStatsSuccess(list: List<TournamentStats>)
        fun onTournamentStatsFailed(message: Int)
        fun onTeamDeleted(isSuccess: Boolean, message: Int)
    }

}