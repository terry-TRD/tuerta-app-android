package com.terry.tureta.model

import com.google.gson.annotations.SerializedName

class Reviews {
    var id: Int = -1
    @SerializedName("user_id")
    var userId: Int = -1
    @SerializedName("establishment_id")
    var establishmentId: Int = -1
    var rate: Int = -1
    var comments: String = ""
    @SerializedName("created_at")
    var createdAt: String = ""
    @SerializedName("created_by")
    var createdBy: Int = -1
}