package com.terry.tureta.customViews

import android.content.Context
import android.content.res.Resources
import android.graphics.*
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import android.view.ViewTreeObserver
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.graphics.drawable.toBitmap
import com.terry.tureta.*
import com.terry.tureta.extensions.resize
import java.lang.Exception

class CustomCardButton @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    companion object {
        const val NORMAL = 0
        const val SMALL = 1

        const val BOLD = 0
        const val LIGHT = 1
    }

    private var mStyle = NORMAL
    private var mTextStyle = LIGHT

    private var mText = ""
    var mShowSeparator: Boolean = true

    private var mEndIconDrawable: Drawable? = null
    private var mEndIcon: Bitmap? = null

    private var mStartIconDrawable: Drawable? = null
    private var mStartIcon: Bitmap? = null

    private val mPaintText = Paint(Paint.ANTI_ALIAS_FLAG)
    private val mPaintSeparator = Paint(Paint.ANTI_ALIAS_FLAG)

    private var mButtonHeight: Double = 0.0
    private var mButtonWidth: Double = 0.0

    private var mIconTint = Color.WHITE
    private var mEndIconTint = Color.WHITE

    init {
        if (attrs != null) {
            val ta = context.obtainStyledAttributes(attrs, R.styleable.CustomCardButton)
            val ta2 = context.obtainStyledAttributes(attrs, R.styleable.CustomButton)

            try {
                mEndIconDrawable = ta.getDrawable(R.styleable.CustomCardButton_end_icon)
            } catch (e: Exception) {
            }

            try {
                mStartIconDrawable = ta.getDrawable(R.styleable.CustomCardButton_start_icon)
            } catch (e: Exception) {
            }

            mIconTint = ta2.getColor(R.styleable.CustomButton_icon_tint, Color.WHITE)
            mEndIconTint = ta.getColor(R.styleable.CustomCardButton_end_icon_tint, mIconTint)

            val textInt = ta2.getResourceId(R.styleable.CustomButton_text, -1)

            if (textInt != -1) {
                mText = context.getString(textInt)
            } else if (ta2.getString(R.styleable.CustomButton_text) != null) {
                mText = ta2.getString(R.styleable.CustomButton_text)!!
            }

            mStyle = ta.getInt(R.styleable.CustomCardButton_card_style, NORMAL)
            mTextStyle = ta.getInt(R.styleable.CustomButton_text_style, LIGHT)

            mShowSeparator = ta.getBoolean(R.styleable.CustomCardButton_show_separator, true)

            mPaintText.typeface = if (mStyle == BOLD) poppinsSemiBold else poppinsRegular
            mPaintText.textSize = if (mStyle == NORMAL) spToPx(mediumFont) else spToPx(smallFont)

            mPaintText.color = ta.getColor(R.styleable.CustomCardButton_foreground, Color.WHITE)
            mPaintSeparator.color =
                ta.getColor(R.styleable.CustomCardButton_foreground, Color.WHITE)

            setButtonMeasures()

            ta.recycle()
            ta2.recycle()

            viewTreeObserver.addOnGlobalLayoutListener(object :
                ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    viewTreeObserver.removeOnGlobalLayoutListener(this)

                    if (height > 0) setIcons()
                }

            })
        }
    }

    fun setLightMode(){
        mPaintText.color = ContextCompat.getColor(context, R.color.white)
        mPaintSeparator.color = ContextCompat.getColor(context, R.color.white)
        postInvalidate()
    }

    fun updateText(text: String?) {
        text ?: return
        mText = text
        postInvalidate()
    }

    fun isSeparatorVisible(visible: Boolean) {
        this.mShowSeparator = visible
        postInvalidate()
    }

    fun setEndIcon(drawable: Drawable?) {
        this.mEndIconDrawable = drawable
        if (drawable == null) this.mEndIcon = null
        setIcons()
        setButtonMeasures()
        postInvalidate()
    }

    fun setEndIconTint(color: Int){
        this.mEndIconTint = context.getColor(color)
        setIcons()
        postInvalidate()
    }

    fun setStartIcon(drawable: Drawable?) {
        this.mStartIconDrawable = drawable
        if (drawable == null) this.mStartIcon = null
        setIcons()
        setButtonMeasures()
        postInvalidate()
    }

    fun setStartIconTint(color: Int){
        this.mIconTint = context.getColor(color)
        setIcons()
        postInvalidate()
    }

    private fun setButtonMeasures() {

        val textHeight = (mPaintText.fontMetrics.descent - mPaintText.fontMetrics.ascent).toDouble()

        mButtonWidth =
            (mPaintText.measureText(mText)
                    + dpToPx(mediumMargin.toFloat()) * 2
                    + dpToPx(smallMargin.toFloat())
                    + textHeight)

        mButtonHeight = textHeight + dpToPx(smallMargin.toFloat()) * 2
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        setMeasuredDimension(
            measureDimension(mButtonWidth.toInt() + 1, widthMeasureSpec),
            measureDimension(mButtonHeight.toInt() + 1, heightMeasureSpec)
        )
    }

    private fun measureDimension(desiredSize: Int, measureSpec: Int): Int {
        var result: Int
        val specMode = MeasureSpec.getMode(measureSpec)
        val specSize = MeasureSpec.getSize(measureSpec)
        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize
        } else {
            result = desiredSize
            if (specMode == MeasureSpec.AT_MOST) {
                result = result.coerceAtMost(specSize)
            }
        }
        return result
    }

    private fun setIcons() {
        val margins = dpToPx(smallMargin.toFloat()) * 2

        if (mEndIconDrawable != null) {
            DrawableCompat.setTint(
                DrawableCompat.wrap(mEndIconDrawable!!),
                mEndIconTint
            )
            mEndIcon = mEndIconDrawable!!.toBitmap(
                mEndIconDrawable!!.intrinsicWidth * 2,
                mEndIconDrawable!!.intrinsicWidth * 2
            )
            if (height > 0) {
                mEndIcon = mEndIcon!!.resize(
                    height.toFloat() - margins,
                    height.toFloat() - margins
                )
            }
        }

        if (mStartIconDrawable != null) {
            DrawableCompat.setTint(
                DrawableCompat.wrap(mStartIconDrawable!!),
                mIconTint
            )
            mStartIcon = mStartIconDrawable!!.toBitmap(
                mStartIconDrawable!!.intrinsicWidth * 2,
                mStartIconDrawable!!.intrinsicWidth * 2
            )
            if (height > 0) {
                mStartIcon = mStartIcon!!.resize(
                    height.toFloat() - margins,
                    height.toFloat() - margins
                )
            }
        }
    }

    private fun dpToPx(dp: Float): Float {
        val density = Resources.getSystem().displayMetrics.density
        return (dp * density)
    }

    private fun spToPx(sp: Float): Float {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.resources.displayMetrics)
    }

    override fun onDraw(canvas: Canvas?) {
        canvas ?: return

        val width = width.toFloat()
        val height = height.toFloat()

//        mPaintText.color = Color.RED
//        canvas.drawRect(0f, 0f, width, height, mPaintText)

        val textHeight = mPaintText.fontMetrics.descent - mPaintText.fontMetrics.ascent

        val margins = dpToPx(smallMargin.toFloat())

        if (mShowSeparator)
            canvas.drawRect(
                mediumMargin.toFloat(),
                height - 1,
                width - mediumMargin.toFloat(),
                height,
                mPaintSeparator
            )

        if (mEndIcon != null) {
            canvas.drawBitmap(
                mEndIcon!!,
                width - mediumMargin.toFloat() - textHeight,
                margins,
                null
            )
        }

        if (mStartIcon != null) {
            canvas.drawBitmap(
                mStartIcon!!,
                mediumMargin.toFloat(),
                margins,
                null
            )

            val mWidth = width - (mediumMargin * 2) - smallMargin - mStartIcon!!.width
            var newText = mText
            for (count in mText.length downTo 0) {
                if (mPaintText.measureText(newText) > mWidth && count > 2) {
                    newText = newText.substring(0, count - 1) + "..."
                } else break
            }

            canvas.drawText(
                newText,
                mediumMargin.toFloat() + smallMargin.toFloat() + mStartIcon!!.width,
                height / 2 + mPaintText.fontMetrics.descent,
                mPaintText
            )
        } else {
            canvas.drawText(
                mText,
                mediumMargin.toFloat(),
                height / 2 + mPaintText.fontMetrics.descent,
                mPaintText
            )
        }
    }

}