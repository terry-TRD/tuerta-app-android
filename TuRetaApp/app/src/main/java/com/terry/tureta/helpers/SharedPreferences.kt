package com.terry.tureta.helpers

import android.content.Context
import android.content.SharedPreferences

class SharedPreferences(val context: Context) {

    companion object {
        private const val PREFS_NAME = "kotlincodes"
    }

    private val sharedPref: SharedPreferences =
        context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

    fun save(KEY_NAME: String, text: String?) {
        val editor: SharedPreferences.Editor = sharedPref.edit()

        editor.putString(KEY_NAME, text)

        editor.apply()
    }

    fun save(KEY_NAME: Int, text: String?) {
        val editor: SharedPreferences.Editor = sharedPref.edit()

        editor.putString(context.getString(KEY_NAME), text)

        editor.apply()
    }

    fun getString(KEY_NAME: String?): String {

        return sharedPref.getString(KEY_NAME, "")!!
    }

    fun getString(KEY_NAME: Int): String {

        return sharedPref.getString(context.getString(KEY_NAME), "")!!
    }

    fun getBoolean(key: String): Boolean {
        return sharedPref.getBoolean(key, false)
    }

    fun getBoolean(key: Int): Boolean {
        return sharedPref.getBoolean(
            context.getString(key), false
        )
    }

    fun clearSharedPreference() {

        val editor: SharedPreferences.Editor = sharedPref!!.edit()

        editor.clear()
        editor.apply()
    }

    fun removeValue(KEY_NAME: String) {

        val editor: SharedPreferences.Editor = sharedPref!!.edit()

        editor.remove(KEY_NAME)
        editor.apply()
    }

    fun save(KEY_NAME: String, status: Boolean) {

        val editor: SharedPreferences.Editor = sharedPref!!.edit()

        editor.putBoolean(KEY_NAME, status)

        editor.apply()
    }

    fun save(KEY_NAME: Int, status: Boolean) {

        val editor: SharedPreferences.Editor = sharedPref!!.edit()

        editor.putBoolean(context.getString(KEY_NAME), status)

        editor.apply()
    }
}