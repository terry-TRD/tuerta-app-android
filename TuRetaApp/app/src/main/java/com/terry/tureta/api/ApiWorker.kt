package com.terry.tureta.api

import com.terry.tureta.BuildConfig
import com.terry.tureta.helpers.StringConverterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ApiWorker {

    private lateinit var retrofit: Retrofit

    fun getClient(baseUrl: String, token: String = ""): Retrofit {
        if (!ApiWorker::retrofit.isInitialized || token.isNotEmpty()) {
            //TODO While release in Google Play Change the Level to NONE
            retrofit =
                buildRetrofit(
                    buildOkHttp(token), baseUrl
                )
        }

        return retrofit

    }

    private fun buildRetrofit(client: OkHttpClient, baseUrl: String): Retrofit =
        Retrofit.Builder()
            .client(client)
            .baseUrl(baseUrl)
            .addConverterFactory(StringConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()

    private fun buildOkHttp(token: String = ""): OkHttpClient{

        val client = OkHttpClient.Builder()

        if(BuildConfig.DEBUG) {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY

            client.addNetworkInterceptor(interceptor)
        }

       return client
            .addInterceptor{chain ->
                val original = chain.request()

                // Request customization: add request headers
                val requestBuilder = original.newBuilder()
                    .header("Accept", "application/json")

                if(token.isNotEmpty()){
                    requestBuilder.header("Authorization", "Bearer $token") // <-- this is the important line
                }

                val request = requestBuilder.build()
                chain.proceed(request)
            }
            .connectTimeout(100, TimeUnit.SECONDS)
            .readTimeout(100, TimeUnit.SECONDS)
            .build()
    }
}