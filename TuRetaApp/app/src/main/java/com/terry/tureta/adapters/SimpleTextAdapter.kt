package com.terry.tureta.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.terry.tureta.R
import com.terry.tureta.databinding.ItemTextViewBinding
import com.terry.tureta.mediumMargin
import com.terry.tureta.smallMargin
import grid.apps.supplies_admin.Extensions.mediumRegular
import grid.apps.supplies_admin.Extensions.setMargins

class SimpleTextAdapter(
    private val context: Context,
    private val size: Int,
    private val callback: View.OnClickListener
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VHItem(
            ItemTextViewBinding.inflate(
                LayoutInflater.from(context), parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is VHItem) holder.bindData(position)
    }

    inner class VHItem(private val binding: ItemTextViewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.textView.mediumRegular(R.color.black)
        }

        fun bindData(position: Int) {
            binding.textView.text = position.toString()
            binding.textView.setOnClickListener(callback)
        }

    }

}