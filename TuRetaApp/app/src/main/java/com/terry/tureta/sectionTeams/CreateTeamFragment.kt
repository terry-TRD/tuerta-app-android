package com.terry.tureta.sectionTeams

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.squareup.picasso.Picasso
import com.terry.tureta.R
import com.terry.tureta.customViews.CustomFragment
import com.terry.tureta.databinding.FragmentCreateTeamBinding
import com.terry.tureta.helpers.CircleTransform
import com.terry.tureta.largeMargin
import com.terry.tureta.mediumMargin
import com.terry.tureta.model.Team
import com.terry.tureta.sectionTournaments.viewModel.TournamentViewModel
import grid.apps.supplies_admin.Extensions.setMargins
import grid.apps.supplies_admin.Extensions.smallRegular
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class CreateTeamFragment : CustomFragment() {

    companion object {
        private const val CODE_INTENT_CAMERA = 100
        private const val CODE_INTENT_GALLERY = 200
    }

    private lateinit var binding: FragmentCreateTeamBinding

    private var mNewPhoto: Uri? = null

    private val mTeam: Team = Team()

    private val mViewModel by lazy {
        ViewModelProvider(requireActivity())[TournamentViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentCreateTeamBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.imageView.setOnClickListener(this)
        binding.imageView.setMargins(0, largeMargin)

        binding.labelAddImage.smallRegular(R.color.grayText)

        setEditText(binding.layoutName, binding.textName)
        binding.layoutName.setMargins(mediumMargin, largeMargin, mediumMargin)

        binding.buttonCreateTeam.setMargins(mediumMargin, mediumMargin, mediumMargin, mediumMargin)
        binding.buttonCreateTeam.setOnClickListener(this)

        binding.textName.addTextChangedListener(this)
    }

    override fun onResume() {
        super.onResume()

        mViewModel.addObserver(this)
    }

    override fun onPause() {
        super.onPause()

        mViewModel.deleteObserver(this)
    }

    override fun onClick(v: View?) {
        v ?: return
        when (v) {
            binding.imageView -> selectImage()
            binding.buttonCreateTeam -> {
                if (validateFields()) {
                    mViewModel.createTeam(mTeam, requireContext())
                    val action = CreateTeamFragmentDirections.actionCreateTeamFragmentToLoadDialog()
                    action.message = getString(R.string.label_creating_team)
                    findNavController().navigate(action)
                }
            }
        }
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        mTeam.name = s.toString().trim()
        binding.layoutName.error = ""
    }

    private fun validateFields(): Boolean {
        if (mNewPhoto == null) {
            showToast(R.string.message_add_team_image)
            return false
        }

        if (binding.textName.text.toString().trim().isEmpty()) {
            binding.layoutName.error = getString(R.string.label_team_name_empty)
            return false
        }
        return true
    }

//    override fun onTeamCreated(isSuccess: Boolean) {
//        if(isSuccess){
//            showToast(R.string.message_team_created_success)
//            findNavController().navigate(R.id.action_createTeamFragment_to_teamsListFragment)
//        } else {
//            showToast(R.string.message_team_created_fail)
//        }
//    }

    // region images
    private fun selectImage() {
        val options = arrayOf(
            getString(R.string.label_take_photo),
            getString(R.string.label_pick_from_gallery),
            getString(R.string.label_cancel)
        )

        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle(R.string.label_select_image)

        builder.setItems(
            options
        ) { dialog, item ->
            when (item) {
                0 -> checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                1 -> checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                2 -> dialog.dismiss()
            }
        }
        builder.show()
    }

    private fun checkPermission(permission: String) {

        val code =
            if (permission == Manifest.permission.WRITE_EXTERNAL_STORAGE) TournamentViewModel.CODE_PERMISSION_WRITE_STORAGE
            else TournamentViewModel.CODE_PERMISSION_READ_STORAGE

        if (ContextCompat.checkSelfPermission(
                requireContext(),
                permission
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            when (permission) {
                Manifest.permission.WRITE_EXTERNAL_STORAGE -> openCamera()
                Manifest.permission.READ_EXTERNAL_STORAGE -> openGallery()
            }
        } else {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(permission),
                code
            )
        }
    }

    private fun openCamera() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(requireActivity().packageManager)?.also {
                val photoFile: File? = try {
                    createImageFile()
                } catch (e: IOException) {
                    null
                }
                photoFile?.also {
                    mNewPhoto = FileProvider.getUriForFile(
                        requireContext(),
                        requireActivity().applicationContext.packageName + ".provider",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mNewPhoto)
                    startActivityForResult(takePictureIntent, CODE_INTENT_CAMERA)
                }
            }
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File =
            requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            mTeam.path = absolutePath
        }
    }

    private fun openGallery() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(intent, CODE_INTENT_GALLERY)
    }

    override fun onRequestPermissionResult(requestCode: Int, isGranted: Boolean) {

        if (!isGranted) return

        when (requestCode) {
            TournamentViewModel.CODE_PERMISSION_WRITE_STORAGE -> {
                openCamera()
            }
            TournamentViewModel.CODE_PERMISSION_READ_STORAGE -> {
                openGallery()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            CODE_INTENT_CAMERA -> {
                if (resultCode == Activity.RESULT_OK) {
                    if (mNewPhoto != null) {
                        Picasso.get().load(mNewPhoto)
                            .transform(CircleTransform(requireContext(), true, false))
                            .into(binding.imageView)
                    }
                }
            }
            CODE_INTENT_GALLERY -> {
                data ?: return
                if (resultCode == Activity.RESULT_OK) {
                    mNewPhoto = data.data
                    mTeam.path = mNewPhoto!!.encodedPath!!
                    mTeam.uri = data.data
                    Picasso.get().load(mNewPhoto)
                        .transform(CircleTransform(requireContext(), true, false))
                        .into(binding.imageView)
                }
            }
        }
    }
    // endregion images
}