package com.terry.tureta.sectionReservations.adapters

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.terry.tureta.R
import com.terry.tureta.sectionReservations.ReservationsListFragment
import com.terry.tureta.sectionReservations.viewModel.ReservationsViewModel

class ReservationFragmentsAdapter(private val context: Context, manager: FragmentManager) :
    FragmentStatePagerAdapter(
        manager,
        BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
    ) {

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> context.getString(R.string.label_upcoming)
            else -> context.getString(R.string.label_previous)
        }
    }

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> ReservationsListFragment(ReservationsViewModel.FILTER_FUTURE)
            else -> ReservationsListFragment(ReservationsViewModel.FILTER_PAST)
        }
    }

    override fun getCount(): Int {
        return 2
    }

}