package com.terry.tureta.helpers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

import com.squareup.picasso.Transformation;
import com.terry.tureta.R;

public class CircleTransform implements Transformation {
    private final int BORDER_COLOR = R.color.grayText;
    private final int BORDER_RADIUS = 1;

    private Context context;
    private Boolean fullRound;
    private Boolean radius;

    public CircleTransform(Context context, Boolean fullRound, Boolean radius) {
        this.context = context;
        this.fullRound = fullRound;
        this.radius = radius;
    }

    @Override
    public Bitmap transform(Bitmap source) {
        int size = Math.min(source.getWidth(), source.getHeight());

        int x = (source.getWidth() - size) / 2;
        int y = (source.getHeight() - size) / 2;

        Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
        if (squaredBitmap != source) {
            source.recycle();
        }

        Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());

        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        BitmapShader shader = new BitmapShader(squaredBitmap, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
        paint.setShader(shader);
        paint.setAntiAlias(true);

        float r = size / 2f;

        // Prepare the background
        Paint paintBg = new Paint();
        paintBg.setColor(context.getResources().getColor(BORDER_COLOR));
        paintBg.setAntiAlias(true);

        float border = radius ? BORDER_RADIUS : dpToPx(10f);
        if (fullRound) {
            // Draw the background circle
            canvas.drawCircle(r, r, r, paintBg);

            // Draw the image smaller than the background so a little border will be seen
            canvas.drawCircle(r, r, r - border, paint);
        } else {
            r = r / 2.8f;

//            RectF borderRect = new RectF(0f, 0f, size, size);
//            canvas.drawRoundRect(borderRect, r, r, paintBg);

            r = r - border;
            RectF imageRect = new RectF(0f, 0f, size, size);
            canvas.drawRoundRect(imageRect, r, r, paint);
        }

        squaredBitmap.recycle();
        return bitmap;
    }

    private float dpToPx(float dp) {
        float density = context.getResources().getSystem().getDisplayMetrics().density;
        return (dp * density);
    }

    @Override
    public String key() {
        return "circle";
    }
}