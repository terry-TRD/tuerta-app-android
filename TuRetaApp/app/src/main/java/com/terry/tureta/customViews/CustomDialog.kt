package com.terry.tureta.customViews

import android.location.Location
import android.view.View
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.terry.tureta.R
import com.terry.tureta.model.*
import com.terry.tureta.sectionEstablishments.viewModel.EstablishmentViewModel
import com.terry.tureta.sectionLogin.viewModel.LoginViewModel
import com.terry.tureta.sectionReservations.viewModel.ReservationsViewModel
import com.terry.tureta.sectionTournaments.viewModel.TournamentViewModel
import com.terry.tureta.sectionMain.viewModel.MainViewModel
import java.util.*

open class CustomDialog : DialogFragment(), EstablishmentViewModel.IEstablishmentObserver,
    ReservationsViewModel.IReservationsObserver, MainViewModel.IMainObserver,
    LoginViewModel.ILoginObserver, TournamentViewModel.ITournamentsObserver, View.OnClickListener {

    override fun onStart() {
        super.onStart()

        dialog?.window?.setBackgroundDrawableResource(R.color.transparent)
    }

    fun showToast(text: Int, duration: Int = Toast.LENGTH_LONG) {
        Toast.makeText(context, text, duration).show()
    }

    fun showToast(text: String, duration: Int = Toast.LENGTH_LONG) {
        Toast.makeText(context, text, duration).show()
    }

    override fun onUpdateView() {}

    override fun onUpdateDates(datesFounded: Boolean) {}

    override fun onDetailsFailed(message: Int) {}

    override fun onFavoriteFinished(isSuccessful: Boolean) {}

    override fun onFieldDeleted(isSuccessful: Boolean, field: Field?) {}

    override fun onFieldCreated(isSuccessful: Boolean, field: Field?) {}

    override fun onFieldEdited(isSuccessful: Boolean, field: Field?) {}

    override fun onFieldAvailableFailed() {}

    override fun onReservationsSuccess(list: List<Reservation>) {}

    override fun onReservationsFailed(message: Int) {}

    override fun onReservationUpdated(isSuccessful: Boolean, message: Int, isOwner: Boolean) {}

    override fun onFilterReservations(text: String) {}

    override fun onUpdateFilterText(text: String) {}

    override fun onReservationResult(isSuccessful: Boolean) {}

    override fun onLogout(isSuccess: Boolean, message: Int?) {}

    override fun onLocationEnabled(isEnabled: Boolean) {}

    override fun onLocationLoaded(location: Location) {}

    override fun onSearchResult(list: List<Establishment>) {}

    override fun onCameraResult(list: List<Establishment>) {}

    override fun onFavoritesResult(list: List<Establishment>?) {}

    override fun onNameUpdated(isSuccess: Boolean) {}

    override fun onColorUpdated(isSuccess: Boolean) {}

    override fun onKeyboardVisibilityChanged(isVisible: Boolean) {}

    override fun onLoggedIn(user: User) {}

    override fun goToWalkthrough() {}

    override fun onLoginFailed(message: Int?) {}

    override fun onRegisterResult(isSuccess: Boolean, message: Int?) {}

    override fun onRestorePasswordResult(isSuccess: Boolean, message: Int) {}

    override fun onRequestPermissionResult(requestCode: Int, isGranted: Boolean) {}

    override fun onTournamentsLoaded(list: List<Tournament>) {}

    override fun onTournamentsFailed(message: Int) {}

    override fun onTeamsLoaded(list: List<Team>) {}

    override fun onTeamsFailed(message: Int) {}

    override fun onTeamCreated(isSuccess: Boolean) {}

    override fun onTournamentFinished(isSuccess: Boolean, message: Int) {}

    override fun onMatchListSuccess(list: List<Match>) {}

    override fun onMatchListFailed(message: Int) {}

    override fun onDateUpdated(date: Calendar) {}

    override fun onMatchFinished(isSuccess: Boolean, message: Int) {}

    override fun onTournamentStatsSuccess(list: List<TournamentStats>) {}

    override fun onTournamentStatsFailed(message: Int) {}

    override fun onTeamDeleted(isSuccess: Boolean, message: Int) {}

    override fun onClick(v: View?) {}

}