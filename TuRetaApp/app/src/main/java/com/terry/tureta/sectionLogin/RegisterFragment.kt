package com.terry.tureta.sectionLogin

import android.os.Bundle
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.terry.tureta.R

import com.terry.tureta.customViews.CustomFragment
import com.terry.tureta.databinding.FragmentRegisterBinding
import com.terry.tureta.largeMargin
import com.terry.tureta.mediumMargin
import com.terry.tureta.sectionLogin.viewModel.LoginViewModel
import com.terry.tureta.smallMargin
import grid.apps.supplies_admin.Extensions.setMargins
import grid.apps.supplies_admin.Extensions.smallRegular

class RegisterFragment : CustomFragment() {

    private lateinit var binding: FragmentRegisterBinding
    private val mViewModel by lazy {
        activity?.run {
            ViewModelProvider(this)[LoginViewModel::class.java]
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setEditText(binding.layoutName, binding.textName)
        setEditText(binding.layoutEmail, binding.textEmail)
        setEditText(binding.layoutPassword, binding.textPassword)
        setEditText(binding.layoutConfirmPassword, binding.textConfirmPassword)
        binding.layoutName.setMargins(mediumMargin, largeMargin, mediumMargin)
        binding.layoutEmail.setMargins(mediumMargin, 0, mediumMargin)
        binding.layoutPassword.setMargins(mediumMargin, 0, mediumMargin)
        binding.layoutConfirmPassword.setMargins(mediumMargin, 0, mediumMargin)

        binding.textName.addTextChangedListener(this)
        binding.textEmail.addTextChangedListener(this)
        binding.textPassword.addTextChangedListener(this)
        binding.textConfirmPassword.addTextChangedListener(this)

        binding.switchTerms.setMargins(mediumMargin, 0)
        binding.labelAccept.smallRegular()
        binding.labelAccept.setMargins(smallMargin, 0)
        binding.buttonAccept.setMargins(smallMargin, 0)

        binding.buttonRegister.setMargins(mediumMargin, smallMargin, mediumMargin)
        binding.buttonRegister.setOnClickListener(this)

        binding.buttonFacebook.setMargins(mediumMargin, smallMargin, mediumMargin, mediumMargin)
        binding.buttonFacebook.setOnClickListener(this)

        binding.buttonAccept.setOnClickListener(this)

        binding.labelAccount.smallRegular()
        binding.labelAccount.setMargins(mediumMargin, smallMargin)
        binding.labelRegister.smallRegular()
        binding.labelRegister.setPadding(mediumMargin, mediumMargin, mediumMargin, mediumMargin)
        binding.separator.setMargins(mediumMargin, 0, mediumMargin)

        binding.buttonLogin.setMargins(smallMargin)
        binding.buttonLogin.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.buttonFacebook -> mViewModel?.loginWithFacebook(requireActivity())
            binding.buttonLogin -> findNavController().navigate(R.id.action_registerFragment_to_loginFragment)
            binding.buttonAccept -> {
                val action =
                    RegisterFragmentDirections.actionRegisterFragmentToWebFragment()
                action.url = getString(R.string.key_privacy)
                findNavController().navigate(action)
            }
            binding.buttonRegister -> {
                if (validateData()) onRegister()
            }
        }
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        when (s.hashCode()) {
            binding.textName.text.hashCode() -> {
                binding.layoutName.error = ""
            }
            binding.textEmail.text.hashCode() -> {
                binding.layoutEmail.error = ""
            }
            binding.textPassword.text.hashCode() -> {
                binding.layoutPassword.error = ""
            }
            binding.textConfirmPassword.text.hashCode() -> {
                binding.layoutConfirmPassword.error = ""
            }
        }
    }

    private fun validateData(): Boolean {
        var count = 0

        if (binding.textName.text.toString().trim().isEmpty()) {
            count++
            binding.layoutName.error = getString(R.string.label_field_empty)
        }

        if (binding.textEmail.text.toString().trim().isEmpty()) {
            count++
            binding.layoutEmail.error = getString(R.string.label_field_empty)
        } else if (
            !Patterns.EMAIL_ADDRESS.matcher(
                binding.textEmail.text.toString().trim()
            ).matches()
        ) {
            count++
            binding.layoutEmail.error = getString(R.string.label_email_invalid)
        }

        if (binding.textPassword.text.toString().trim().isEmpty()) {
            count++
            binding.layoutPassword.error = getString(R.string.label_field_empty)
        }

        if (binding.textConfirmPassword.text.toString().trim().isEmpty()) {
            count++
            binding.layoutConfirmPassword.error = getString(R.string.label_field_empty)
        } else if (
            binding.textPassword.text.toString().trim()
            != binding.textConfirmPassword.text.toString().trim()
        ) {
            count++
            binding.layoutConfirmPassword.error = getString(R.string.label_passwords_dont_match)
        }

        if (count == 0 && !binding.switchTerms.isChecked) {
            count++
            showToast(R.string.message_terms_error)
        }

        return count == 0
    }

    private fun onRegister() {
        val action =
            LoginFragmentDirections.actionLoginFragmentToLoadDialog()
        action.message = getString(R.string.label_registering)
        findNavController().navigate(action)

        mViewModel?.register(
            binding.textEmail.text.toString().trim(),
            binding.textPassword.text.toString().trim(),
            binding.textName.text.toString().trim(),
            binding.textEmail.text.toString().trim()
        )
    }

}
