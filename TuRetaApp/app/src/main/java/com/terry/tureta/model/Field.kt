package com.terry.tureta.model

import android.content.Context
import com.google.gson.annotations.SerializedName
import com.terry.tureta.R
import java.io.Serializable

class Field() : Serializable {
    var id: Int = 0
    var name: String = ""

    @SerializedName("game_types")
    var gameTypes: String = ""

    @SerializedName("grass_type")
    var grassType: Int = 0

    @SerializedName("is_active")
    var isActive: Boolean = false
    var availability: ArrayList<Establishment.Schedules> = ArrayList()
    var prices: ArrayList<Price> = ArrayList()

    var priceType: String = ""

    class Price : Serializable {
        @SerializedName("price_per_hour")
        var perHour: String = ""

        @SerializedName("price_per_half")
        var perHalf: String = ""

        @SerializedName("game_type")
        var gameType: Int = -1
    }

    constructor(field: Field) : this() {
        this.id = field.id
        this.name = field.name
        this.gameTypes = field.gameTypes
        this.grassType = field.grassType
        this.isActive = field.isActive
        this.availability = field.availability
        this.prices = ArrayList(field.prices)
    }

    fun getGameType(index: Int): String = gameTypes.split(",")[index]

    fun getGameTypeSelected(): String =
        gameTypes.split(",")[0]

    fun getGrassType(context: Context): String = context.resources.getStringArray(R.array.grass_types)[grassType - 1]
}