package com.terry.tureta.api

import com.terry.tureta.model.Establishment
import com.terry.tureta.model.Reservation
import com.terry.tureta.model.SearchResult
import io.reactivex.Observable
import retrofit2.http.*

interface APIUser {

    @GET("api/v1/establishments")
    fun getEstablishments(
        @Query("center_lat") centerLat: Double,
        @Query("center_lng") centerLng: Double,
        @Query("border_lat") borderLat: Double = 0.0,
        @Query("border_lng") borderLng: Double = 0.0,
        @Query("search") search: String = ""
    ): Observable<SearchResult<ArrayList<Establishment>>>

    @GET("api/v1/users/{user_id}/favorites")
    fun getFavorites(
        @Path("user_id") userId: Int
    ): Observable<SearchResult<ArrayList<Establishment>>>

    @GET("api/v1/reservations")
    fun getReservations(
        @Query("filter") filter: String
    ): Observable<SearchResult<ArrayList<Reservation>>>

    @DELETE("api/v1/reservations/{reservation_id}/cancel")
    fun cancelReservations(
        @Path("reservation_id") reservationId: Int
    ): Observable<Any>

    @POST("api/v1/password")
    @FormUrlEncoded
    fun restorePassword(
        @Field("email") email: String
    ): Observable<Any>

    @POST("api/v1/users/{user_id}/favorites")
    @FormUrlEncoded
    fun addFavoriteField(
        @Path("user_id") userId: Int,
        @Field("establishment_id") establishmentId: Int
    ): Observable<Any>

    @DELETE("api/v1/users/{user_id}/favorites/{establishment_id}")
    fun deleteFavoriteField(
        @Path("user_id") userId: Int,
        @Path("establishment_id") establishmentId: Int
    ): Observable<Any>
}