package grid.apps.supplies_admin.Extensions

import android.graphics.Paint
import android.graphics.Rect
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import com.terry.tureta.*

fun AppCompatTextView.calculateHeight(): Int {
    val paint = Paint()
    val bounds = Rect()

    paint.typeface = this.typeface
    val textSize = this.textSize
    paint.textSize = textSize
    val text = this.text.toString()
    paint.getTextBounds(text, 0, text.length, bounds)
    return bounds.height()
}

fun AppCompatTextView.calculateWidth(): Int {
    val paint = Paint()
    val bounds = Rect()

    paint.typeface = this.typeface
    val textSize = this.textSize
    paint.textSize = textSize
    val text = this.text.toString()
    paint.getTextBounds(text, 0, text.length, bounds)
    return bounds.width()
}

fun AppCompatTextView.largeTitleStyle(color: Int = R.color.accent, textSize: Float = extraLargeFont) {
    this.textSize = textSize
    this.typeface = nunitoBold
    this.setTextColor(ContextCompat.getColor(context, color))
}

fun AppCompatTextView.largeBold(color: Int = R.color.white) {
    this.textSize = largeFont
    this.typeface = poppinsSemiBold
    this.setTextColor(ContextCompat.getColor(context, color))
}

fun AppCompatTextView.largeRegular(color: Int = R.color.white) {
    this.textSize = largeFont
    this.typeface = poppinsRegular
    this.setTextColor(ContextCompat.getColor(context, color))
}

fun AppCompatTextView.mediumBold(color: Int = R.color.white) {
    this.textSize = mediumFont
    this.typeface = poppinsSemiBold
    this.setTextColor(ContextCompat.getColor(context, color))
}

fun AppCompatTextView.mediumRegular(color: Int = R.color.white) {
    this.textSize = mediumFont
    this.typeface = poppinsRegular
    this.setTextColor(ContextCompat.getColor(context, color))
}

fun AppCompatTextView.smallBold(color: Int = R.color.white) {
    this.textSize = smallFont
    this.typeface = poppinsSemiBold
    this.setTextColor(ContextCompat.getColor(context, color))
}

fun AppCompatTextView.smallRegular(color: Int = R.color.white) {
    this.textSize = smallFont
    this.typeface = poppinsRegular
    this.setTextColor(ContextCompat.getColor(context, color))
}

fun AppCompatTextView.extraSmallBold(color: Int = R.color.white) {
    this.textSize = extraSmallFont
    this.typeface = poppinsSemiBold
    this.setTextColor(ContextCompat.getColor(context, color))
}

fun AppCompatTextView.extraSmallRegular(color: Int = R.color.white) {
    this.textSize = extraSmallFont
    this.typeface = poppinsRegular
    this.setTextColor(ContextCompat.getColor(context, color))
}