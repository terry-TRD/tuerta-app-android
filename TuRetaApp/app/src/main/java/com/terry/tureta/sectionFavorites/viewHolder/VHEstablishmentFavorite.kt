package com.terry.tureta.sectionFavorites.viewHolder

import android.content.Context
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.terry.tureta.R
import com.terry.tureta.databinding.ItemFieldFavoriteBinding
import com.terry.tureta.mediumMargin
import com.terry.tureta.model.Establishment
import com.terry.tureta.adapters.EstablishmentsAdapter
import com.terry.tureta.smallMargin
import grid.apps.supplies_admin.Extensions.mediumBold
import grid.apps.supplies_admin.Extensions.setMargins
import grid.apps.supplies_admin.Extensions.smallRegular

class VHEstablishmentFavorite(
    private val binding: ItemFieldFavoriteBinding,
    private val context: Context,
    private val callback: EstablishmentsAdapter.IEstablishmentsAdapter
) :
    RecyclerView.ViewHolder(binding.root) {

    init {
        binding.textName.mediumBold(R.color.black)
        binding.textName.setMargins(mediumMargin, smallMargin, mediumMargin)

        binding.imageLocation.setMargins(mediumMargin)

        binding.textLocation.setMargins(smallMargin, smallMargin, mediumMargin, smallMargin)
        binding.textLocation.smallRegular(R.color.black)

        binding.separator.setMargins(0, smallMargin)

        binding.layoutRating.setMargins(mediumMargin, 0, mediumMargin)
        binding.cardBackground.setMargins(mediumMargin, 0, mediumMargin, mediumMargin)
    }

    fun bindData(establishment: Establishment, position: Int) {
        Picasso.get().load(establishment.image).into(binding.imageField)
        binding.textName.text = establishment.name
        binding.textLocation.text = establishment.address
        setRating(establishment.rate)

        binding.cardItem.setOnClickListener { callback.onEstablishmentSelected(establishment) }
    }

    private fun setRating(rate: Int) {
        val gray = ContextCompat.getColor(context, R.color.grayText)
        val green = ContextCompat.getColor(context, R.color.accent)
        when (rate) {
            0 -> {
                binding.star1.setColorFilter(gray)
                binding.star2.setColorFilter(gray)
                binding.star3.setColorFilter(gray)
                binding.star4.setColorFilter(gray)
                binding.star5.setColorFilter(gray)
            }
            1 -> {
                binding.star1.setColorFilter(green)
                binding.star2.setColorFilter(gray)
                binding.star3.setColorFilter(gray)
                binding.star4.setColorFilter(gray)
                binding.star5.setColorFilter(gray)
            }
            2 -> {
                binding.star1.setColorFilter(green)
                binding.star2.setColorFilter(green)
                binding.star3.setColorFilter(gray)
                binding.star4.setColorFilter(gray)
                binding.star5.setColorFilter(gray)
            }
            3 -> {
                binding.star1.setColorFilter(green)
                binding.star2.setColorFilter(green)
                binding.star3.setColorFilter(green)
                binding.star4.setColorFilter(gray)
                binding.star5.setColorFilter(gray)
            }
            4 -> {
                binding.star1.setColorFilter(green)
                binding.star2.setColorFilter(green)
                binding.star3.setColorFilter(green)
                binding.star4.setColorFilter(green)
                binding.star5.setColorFilter(gray)
            }
            5 -> {
                binding.star1.setColorFilter(green)
                binding.star2.setColorFilter(green)
                binding.star3.setColorFilter(green)
                binding.star4.setColorFilter(green)
                binding.star5.setColorFilter(green)
            }
        }
    }

}