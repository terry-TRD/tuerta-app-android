package com.terry.tureta.sectionReservations

import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.terry.tureta.R
import com.terry.tureta.customViews.CustomFragment
import com.terry.tureta.databinding.FragmentReservationsListBinding
import com.terry.tureta.mediumMargin
import com.terry.tureta.model.Reservation
import com.terry.tureta.sectionEstablishments.viewModel.EstablishmentViewModel
import com.terry.tureta.sectionReservations.adapters.ReservationsAdapter
import com.terry.tureta.sectionReservations.viewModel.ReservationsViewModel
import com.terry.tureta.sectionMain.viewModel.MainViewModel
import grid.apps.supplies_admin.Extensions.largeTitleStyle
import grid.apps.supplies_admin.Extensions.setMargins
import java.util.*
import kotlin.collections.ArrayList

class ReservationsListFragment() : CustomFragment(), ReservationsAdapter.IReservations {

    private lateinit var binding: FragmentReservationsListBinding
    private val list: ArrayList<Reservation> = ArrayList()
    private val sortedList: ArrayList<Reservation> = ArrayList()

    private val mViewModel by lazy {
        activity?.run {
            val viewModel = ViewModelProvider(this)[ReservationsViewModel::class.java]
            val mainViewModel = ViewModelProvider(this)[MainViewModel::class.java]
            viewModel.setUser(mainViewModel.getUser())
        }
    }
    private val adapter by lazy {
        ReservationsAdapter(
            requireContext(),
            sortedList,
            mViewModel!!.isOwner(),
            this
        )
    }

    private var filter: String = ""
    private var textFilter = ""

    constructor(
        filter: String
    ) : this() {
        this.filter = filter
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentReservationsListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recyclerReservations.layoutManager = LinearLayoutManager(context)
        binding.recyclerReservations.adapter = adapter

        binding.swipeToRefresh.setColorSchemeResources(R.color.accent)
        binding.swipeToRefresh.setOnRefreshListener {
            if (filter == ReservationsViewModel.FILTER_FUTURE) mViewModel?.getReservations()
            else mViewModel?.getHistoryReservations()
        }

        binding.textMessage.largeTitleStyle()
        binding.textMessage.setMargins(mediumMargin, 0, mediumMargin)

        binding.recyclerReservations.addItemDecoration(object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(
                outRect: Rect,
                view: View,
                parent: RecyclerView,
                state: RecyclerView.State
            ) {
                val position = parent.getChildAdapterPosition(view)
                val size = parent.adapter!!.itemCount

                var bottom =
                    if (position == size - 1 && mViewModel!!.isOwner() && filter == ReservationsViewModel.FILTER_FUTURE) 0
                    else mediumMargin

                if (position != size - 1) bottom = 0

                outRect.run {
                    left = mediumMargin
                    top = mediumMargin
                    right = mediumMargin
                    this.bottom = bottom
                }
            }
        })

        if (mViewModel!!.isOwner() && filter == ReservationsViewModel.FILTER_FUTURE) {
            binding.buttonManualReservation.visibility = View.VISIBLE
            binding.buttonManualReservation.setOnClickListener(this)
            binding.buttonManualReservation.setMargins(
                mediumMargin,
                mediumMargin,
                mediumMargin,
                mediumMargin
            )
        }
    }

    override fun onResume() {
        super.onResume()

        mViewModel?.addObserver(this)

        textFilter = if (filter == ReservationsViewModel.FILTER_FUTURE) {
            mViewModel?.getReservations()
            mViewModel!!.getFutureFilter()
        } else {
            mViewModel?.getHistoryReservations()
            mViewModel!!.getPastFilter()
        }

        mViewModel?.updateFilterText(textFilter)
    }

    override fun onPause() {
        super.onPause()

        mViewModel?.deleteObserver(this)
    }

    override fun onReservationSelected(reservation: Reservation) {
        mViewModel?.setReservation(reservation)
        findNavController().navigate(R.id.action_reservationsFragment_to_reservationDetailsFragment)
    }

    override fun onFilterReservations(text: String) {
        textFilter = text
        sortedList.clear()
        if (text.isNotEmpty()) {
            sortedList.addAll(list.filter {
                it.code.contains(text.toUpperCase(Locale.ROOT))
            })
        } else {
            sortedList.addAll(list)
        }
        adapter.notifyDataSetChanged()
    }

    override fun onReservationsSuccess(list: List<Reservation>) {
        binding.swipeToRefresh.isRefreshing = false

        this.list.clear()
        this.list.addAll(list)

        for(reservation in this.list){
            if(reservation.type == ReservationsViewModel.RESERVATION_MANUAL){
                reservation.user = mViewModel!!.getUser()
            }
        }

        onFilterReservations(textFilter)

        if (this.list.isEmpty() && context != null) {
            binding.imageView.visibility = View.VISIBLE
            binding.textMessage.visibility = View.VISIBLE
        } else if (context != null) {
            binding.imageView.visibility = View.INVISIBLE
            binding.textMessage.visibility = View.INVISIBLE
        }
    }

    override fun onReservationsFailed(message: Int) {
        binding.swipeToRefresh.isRefreshing = false
        if (isVisible) showToast(message)
    }

    override fun onClick(v: View?) {
        v ?: return
        when (v) {
            binding.buttonManualReservation -> {
                ViewModelProvider(requireActivity())[EstablishmentViewModel::class.java]
                    .setEstablishment(mViewModel!!.getUser().getEstablishment())
                findNavController().navigate(R.id.action_reservationsFragment_to_customDatePicker)
            }
        }
    }

    fun getFilter(): String = filter
}
