package com.terry.tureta.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.terry.tureta.R
import com.terry.tureta.customViews.CustomDialog
import com.terry.tureta.databinding.DialogLoadBinding
import com.terry.tureta.model.*
import com.terry.tureta.sectionEstablishments.viewModel.EstablishmentViewModel
import com.terry.tureta.sectionLogin.viewModel.LoginViewModel
import com.terry.tureta.sectionReservations.viewModel.ReservationsViewModel
import com.terry.tureta.sectionTournaments.viewModel.TournamentViewModel
import com.terry.tureta.sectionMain.viewModel.MainViewModel
import com.terry.tureta.sectionTeams.TeamsListFragment
import grid.apps.supplies_admin.Extensions.mediumBold

class LoadDialog : CustomDialog() {

    private lateinit var binding: DialogLoadBinding
    private val args: LoadDialogArgs by navArgs()

    private val mViewModel by lazy {
        activity?.run {
            ViewModelProvider(this)[MainViewModel::class.java]
        }
    }

    override fun onStart() {
        super.onStart()

        dialog!!.setCanceledOnTouchOutside(false)
    }

    override fun onResume() {
        super.onResume()

        activity?.run {
            ViewModelProvider(this)[EstablishmentViewModel::class.java].addObserver(this@LoadDialog)
            ViewModelProvider(this)[ReservationsViewModel::class.java].addObserver(this@LoadDialog)
            ViewModelProvider(this)[MainViewModel::class.java].addObserver(this@LoadDialog)
            ViewModelProvider(this)[LoginViewModel::class.java].addObserver(this@LoadDialog)
            ViewModelProvider(this)[TournamentViewModel::class.java].addObserver(this@LoadDialog)
        }
    }

    override fun onPause() {
        super.onPause()

        activity?.run {
            ViewModelProvider(this)[EstablishmentViewModel::class.java].deleteObserver(this@LoadDialog)
            ViewModelProvider(this)[ReservationsViewModel::class.java].deleteObserver(this@LoadDialog)
            ViewModelProvider(this)[MainViewModel::class.java].deleteObserver(this@LoadDialog)
            ViewModelProvider(this)[LoginViewModel::class.java].deleteObserver(this@LoadDialog)
            ViewModelProvider(this)[TournamentViewModel::class.java].deleteObserver(this@LoadDialog)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DialogLoadBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.textMessage.mediumBold(R.color.white)
        binding.textMessage.text = args.message
    }

    override fun onRestorePasswordResult(isSuccess: Boolean, message: Int) {
        showToast(message)
        if (isSuccess) {
            findNavController().navigate(R.id.action_loadDialog_to_loginFragment)
        } else {
            findNavController().navigate(R.id.action_loadDialog_to_restorePasswordFragment)
        }
    }

    override fun onFieldDeleted(isSuccessful: Boolean, field: Field?) {
        if (isSuccessful) {
            mViewModel?.getUser()?.getEstablishment()?.fields?.removeAll { it.id == field?.id }
            showToast(R.string.message_delete_field_sucess)
            findNavController().navigate(R.id.action_loadDialog_to_adminMainFragment)
        } else {
            showToast(R.string.message_error_delete_field)
            findNavController().navigate(R.id.action_loadDialog_to_customBottomDialog)
        }
    }

    override fun onFieldCreated(isSuccessful: Boolean, field: Field?) {
        if (isSuccessful) {
            val fields = mViewModel!!.getUser().getEstablishment().fields

            fields.add(field!!)

            showToast(R.string.message_field_created_success)
            findNavController().navigate(R.id.action_loadDialog_to_adminMainFragment)
        } else {
            showToast(R.string.message_field_created_fail)
            findNavController().navigate(R.id.action_loadDialog_to_fieldFragment)
        }
    }

    override fun onFieldEdited(isSuccessful: Boolean, field: Field?) {
        if (isSuccessful) {
            val fields = mViewModel!!.getUser().getEstablishment().fields

            for (count in 0 until fields.size) {
                if (fields[count].id == field?.id) {
                    fields[count] = field
                }
            }

            showToast(R.string.message_field_edit_success)
            findNavController().navigate(R.id.action_loadDialog_to_adminMainFragment)
        } else {
            showToast(R.string.message_field_edit_fail)
            findNavController().navigate(R.id.action_loadDialog_to_fieldFragment)
        }
    }

    override fun onFieldAvailableFailed() {}

    override fun onReservationResult(isSuccessful: Boolean) {
        val user = ViewModelProvider(requireActivity())[MainViewModel::class.java].getUser()
        if (!user.isOwner()) {
            findNavController().navigate(
                LoadDialogDirections.actionLoadDialogToReservationResultFragment(
                    args.hasDates,
                    isSuccessful
                )
            )
        } else {
            showToast(R.string.label_reservation_created)
            findNavController().navigate(R.id.action_loadDialog_to_reservationsFragment)
        }
    }

    override fun onReservationUpdated(isSuccessful: Boolean, message: Int, isOwner: Boolean) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show()

        if (isSuccessful) {
            if (isOwner)
                findNavController().navigate(R.id.action_loadDialog_to_reservationsFragment)
            else
                findNavController().navigate(R.id.action_loadDialog_to_mapFragment)
        } else {
            findNavController().navigate(R.id.action_loadDialog_to_reservationDetailsFragment)
        }
    }

    override fun onLogout(isSuccess: Boolean, message: Int?) {
        if (isSuccess) {
            findNavController().navigate(R.id.action_loadDialog_to_loginFragment)
        } else {
            showToast(message!!)
            val action =
                LoadDialogDirections.actionLoadDialogToCustomBottomDialog(CustomBottomDialog.MODE_LOGOUT)
            findNavController().navigate(action)
        }
    }

    override fun onNameUpdated(isSuccess: Boolean) {
        if (isSuccess) {
            binding.progressBar.visibility = View.INVISIBLE
            binding.textMessage.visibility = View.INVISIBLE

            val action =
                LoadDialogDirections.actionLoadDialogToCustomBottomDialog(CustomBottomDialog.MODE_PROFILE_RESULT)
            findNavController().navigate(action)
        } else {
            Toast.makeText(
                requireContext(),
                R.string.message_update_profile_fail,
                Toast.LENGTH_LONG
            ).show()

            findNavController().navigate(R.id.action_loadDialog_to_editProfileFragment)
        }
    }

    override fun onColorUpdated(isSuccess: Boolean) {
        if (isSuccess) {
            binding.progressBar.visibility = View.INVISIBLE
            binding.textMessage.visibility = View.INVISIBLE

            val action =
                LoadDialogDirections.actionLoadDialogToCustomBottomDialog(CustomBottomDialog.MODE_PROFILE_RESULT)
            findNavController().navigate(action)
        } else {
            Toast.makeText(
                requireContext(),
                R.string.message_update_profile_fail,
                Toast.LENGTH_LONG
            ).show()

            findNavController().navigate(R.id.action_loadDialog_to_profileIconFragment)
        }
    }

    override fun onLoggedIn(user: User) {
        if (user.isOwner())
            findNavController().navigate(R.id.action_loadDialog_to_adminMainFragment)
        else
            findNavController().navigate(R.id.action_loadDialog_to_mapFragment)
    }

    override fun goToWalkthrough() {}

    override fun onLoginFailed(message: Int?) {
        if (message != null) showToast(message)
        findNavController().navigate(R.id.action_loadDialog_to_loginFragment)
    }

    // region tournaments

    override fun onTeamCreated(isSuccess: Boolean) {
        if (isSuccess) {
            showToast(R.string.message_team_created_success)
            val action =
                LoadDialogDirections.actionLoadDialogToTeamsListFragment(TeamsListFragment.MODE_SINGLE_SELECTION)
            findNavController().navigate(action)
        } else {
            showToast(R.string.message_team_created_fail)
            findNavController().navigate(R.id.action_loadDialog_to_createTeamFragment)
        }
    }

    override fun onTeamDeleted(isSuccess: Boolean, message: Int) {
        showToast(message)
        val action =
            LoadDialogDirections.actionLoadDialogToTeamsListFragment(TeamsListFragment.MODE_SINGLE_SELECTION)
        findNavController().navigate(action)
    }

    override fun onTournamentFinished(isSuccess: Boolean, message: Int) {
        showToast(message)
        if (isSuccess) {
            findNavController().navigate(R.id.action_loadDialog_to_tournamentsListFragment)
        } else {
            findNavController().navigate(R.id.action_loadDialog_to_editTournamentFragment)
        }
    }

    override fun onMatchFinished(isSuccess: Boolean, message: Int) {
        showToast(message)
        if (isSuccess) {
            findNavController().navigate(R.id.action_loadDialog_to_matchesListFragment)
        } else {
            findNavController().navigate(R.id.action_loadDialog_to_matchFragment)
        }
    }
    // endregion tournaments

}
