package com.terry.tureta.extensions

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Matrix
import android.graphics.RectF
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.terry.tureta.largeMargin

fun Bitmap.resize(width: Float, height: Float): Bitmap {
    val matrix = Matrix()

    val src = RectF(0f, 0f, this.width.toFloat(), this.height.toFloat())
    val dst = RectF(0f, 0f, width, height)

    matrix.setRectToRect(src, dst, Matrix.ScaleToFit.CENTER)

    return Bitmap.createBitmap(this, 0, 0, this.width, this.height, matrix, true)
}

fun bitmapFromVector(context: Context, resource: Int): BitmapDescriptor{
    val icon = context.getDrawable(resource)
    icon!!.setBounds(0, 0, largeMargin, largeMargin)
    val bitmap = Bitmap.createBitmap(largeMargin, largeMargin, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(bitmap)
    icon.draw(canvas)
    return BitmapDescriptorFactory.fromBitmap(bitmap)
}