package com.terry.tureta.sectionFields

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.terry.tureta.R
import com.terry.tureta.customViews.CustomFragment

import com.terry.tureta.databinding.FragmentFieldBinding
import com.terry.tureta.dialogs.CustomBottomDialog
import com.terry.tureta.mediumMargin
import com.terry.tureta.sectionEstablishments.viewModel.EstablishmentViewModel
import com.terry.tureta.smallMargin
import grid.apps.supplies_admin.Extensions.*

class FieldFragment : CustomFragment() {

    private lateinit var binding: FragmentFieldBinding

    private val mViewModel by lazy {
        ViewModelProvider(requireActivity())[EstablishmentViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentFieldBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setEditText(binding.layoutFieldName, binding.textFieldName)
        setEditText(binding.layoutHalfHourCost, binding.textHalfHourCost)
        setEditText(binding.layoutHourCost, binding.textHourCost)

        binding.labelField.smallRegular(R.color.white)
        binding.labelField.setMargins(mediumMargin)

        binding.layoutFieldName.setMargins(mediumMargin, 0, mediumMargin)
        binding.layoutHalfHourCost.setMargins(mediumMargin, 0, mediumMargin, mediumMargin)
        binding.layoutHourCost.setMargins(mediumMargin, smallMargin, mediumMargin)
        binding.switchEnabled.setMargins(0, mediumMargin, mediumMargin)

        binding.buttonGrassType.setMargins(0, smallMargin)

        binding.buttonCreateField.setMargins(mediumMargin, 0, mediumMargin, mediumMargin)
        binding.buttonCreateField.setOnClickListener(this)

        binding.scrollView.setMargins(0, 0, 0, mediumMargin)

        binding.buttonGameType.setOnClickListener(this)
        binding.buttonGrassType.setOnClickListener(this)
    }

    override fun onResume() {
        super.onResume()

        val field = mViewModel.getField()

        if (field != null) {

            binding.textFieldName.setText(field.name)
            if (field.prices.size > 0) {
                binding.textHourCost.setText(field.prices[0].perHour)
                binding.textHalfHourCost.setText(field.prices[0].perHalf)
            }

            binding.buttonGrassType.updateText(field.getGrassType(requireContext()))
            binding.buttonGameType.updateText(field.getGameType(0) + " jugadores")

            binding.buttonCreateField.updateText(R.string.label_update)
            binding.switchEnabled.isChecked = field.isActive

            binding.buttonDeleteField.setMargins(mediumMargin, 0, mediumMargin, mediumMargin)
            binding.buttonDeleteField.setOnClickListener(this)

            mViewModel.gameTypeSelected = if (field.getGameTypeSelected() == "7") 0 else 1
            mViewModel.grassTypeSelected =
                if (field.getGrassType(requireContext()) == "Pasto natural") 0 else 1
        } else {
            binding.buttonDeleteField.visibility = View.GONE
        }

        setAdapters()
    }

    private fun setAdapters() {
//        if (!::playersAdapterSelect.isInitialized)
//            playersAdapterSelect = SelectTextAdapter(
//                requireContext(),
//                resources.getStringArray(R.array.game_types).toList(),
//                object : SelectTextAdapter.ITextAdapter {
//                    override fun onTextClick(text: String, position: Int) {
//                        binding.buttonGameType.updateText(text)
//                        gameTypeSelected = position
//
//                        gameType = if (position == 0) "7" else "11"
//                    }
//                }, gameTypeSelected, true
//            )
//
//        if (!::grassAdapterSelect.isInitialized)
//            grassAdapterSelect = SelectTextAdapter(
//                requireContext(),
//                resources.getStringArray(R.array.grass_types).toList(),
//                object : SelectTextAdapter.ITextAdapter {
//                    override fun onTextClick(text: String, position: Int) {
//                        binding.buttonGrassType.updateText(text)
//                        grassTypeSelected = position
//
//                        grassType = (position + 1).toString()
//                    }
//                }, grassTypeSelected, true
//            )
    }

    override fun onClick(v: View?) {
        v ?: return
        when (v) {
            binding.buttonCreateField -> {

                if (!validateData()) {
                    showToast(R.string.message_fields_empty)
                    return
                }

                val action =
                    FieldFragmentDirections.actionFieldFragmentToLoadDialog()

                val gameType = if (mViewModel.gameTypeSelected == 0) "7" else "11"

                if (mViewModel.getField() == null) {

                    action.message = getString(R.string.label_creating_field)

                    mViewModel.createField(
                        binding.textFieldName.text.toString().trim(),
                        gameType.toInt(),
                        mViewModel.grassTypeSelected,
                        binding.switchEnabled.isChecked,
                        binding.textHourCost.text.toString().trim().toInt(),
                        binding.textHalfHourCost.text.toString().trim().toInt()
                    )

                    mViewModel.gameTypeSelected = -1
                    mViewModel.grassTypeSelected = -1
                } else {

                    action.message = getString(R.string.label_updating_field)

                    mViewModel.updateField(
                        mViewModel.getField()!!,
                        binding.textFieldName.text.toString().trim(),
                        gameType,
                        (mViewModel.grassTypeSelected + 1).toString(),
                        binding.switchEnabled.isChecked,
                        gameType,
                        binding.textHourCost.text.toString().trim(),
                        binding.textHalfHourCost.text.toString().trim()
                    )

                    mViewModel.gameTypeSelected = -1
                    mViewModel.grassTypeSelected = -1
                }

                findNavController().navigate(action)
            }
            binding.buttonDeleteField -> {
                val action =
                    FieldFragmentDirections.actionFieldFragmentToCustomBottomDialog(
                        CustomBottomDialog.MODE_DELETE_FIELD
                    )
                findNavController().navigate(action)
            }
            binding.buttonGameType -> {
                val action =
                    FieldFragmentDirections.actionFieldFragmentToTypesListFragment(
                        TypesListFragment.MODE_PLAYERS_FIELD
                    )
                findNavController().navigate(action)
//                binding.recyclerSelection.adapter = playersAdapterSelect
//
//                binding.labelSelection.setText(R.string.label_game_type)
//                binding.motionLayout.transitionToEnd()
//                transition.setEnable(true)
            }
            binding.buttonGrassType -> {
                val action =
                    FieldFragmentDirections.actionFieldFragmentToTypesListFragment(
                        TypesListFragment.MODE_GRASS_FIELD
                    )
                findNavController().navigate(action)
//                binding.recyclerSelection.adapter = grassAdapterSelect
//
//                binding.labelSelection.setText(R.string.label_grass_type)
//                binding.motionLayout.transitionToEnd()
//                transition.setEnable(true)
            }
        }
    }

    private fun validateData(): Boolean {
        if (binding.textFieldName.text.isNullOrEmpty()) return false
        if (binding.textHourCost.text.isNullOrEmpty()) return false
        if (binding.textHalfHourCost.text.isNullOrEmpty()) return false
        if (mViewModel.gameTypeSelected == -1) return false
        if (mViewModel.grassTypeSelected == -1) return false
        return true
    }

}
