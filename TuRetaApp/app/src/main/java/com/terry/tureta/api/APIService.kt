package com.terry.tureta.api

import com.terry.tureta.BuildConfig
import com.terry.tureta.model.*
import io.reactivex.Observable
import retrofit2.http.*
import retrofit2.http.Field

interface APIService {

    @POST("api/v1/login")
    @FormUrlEncoded
    fun loginWS(
        @Field("email") email: String,
        @Field("password") password: String = "",
        @Field("device_id") device_id: String,
        @Field("grant_type") grantType: String = "password",
        @Field("provider") provider: String = "facebook",
        @Field("access_token") access_token: String = ""
    ): Observable<SearchResult<UserResponse>>

    @POST("api/v1/register")
    @FormUrlEncoded
    fun registerWS(
        @Field("email") email: String,
        @Field("password") password: String,
        @Field("password_confirmation") passwordConfirmation: String,
        @Field("device_id") device_id: String,
        @Field("name") name: String,
        @Field("username") username: String
    ): Observable<SearchResult<UserResponse>>

    @GET("api/v1/profile")
    fun getProfileWS(): Observable<SearchResult<User>>

    @POST("api/v1/logout")
    fun logOut(): Observable<Any>

    @GET("api/v1/establishments/{court_id}")
    fun getEstablishmentDetails(
        @Path("court_id") courtId: Int
    ): Observable<SearchResult<Establishment>>

    @GET("api/v1/establishments/{field_id}/availability")
    fun getEstablishmentAvailability(
        @Path("field_id") courtId: Int,
        @Query("date") date: String
    ): Observable<SearchResult<ArrayList<com.terry.tureta.model.Field>>>

    @PUT("api/v1/establishments/{establishment_id}/fields/{field_id}")
    @FormUrlEncoded
    fun updateField(
        @Path("establishment_id") establishmentId: Int,
        @Path("field_id") fieldId: Int,
        @Field("name") name: String? = null,
        @Field("game_types[]") gameTypes: String? = null,
        @Field("grass_type") grassType: String? = null,
        @Field("is_active") isActive: Int? = null,
        @Field("price_type[]") priceType: String? = null,
        @Field("price_hour[]") priceHour: String? = null,
        @Field("price_half_hour[]") priceHalfHour: String? = null
    ): Observable<SearchResult<com.terry.tureta.model.Field>>

    @GET("api/v1/establishments/{establishment_id}/reservations")
    fun getReservations(
        @Path("establishment_id") establishmentId: Int,
        @Query("filter") filter: String,
        @Query("code") code: String? = null
    ): Observable<SearchResult<ArrayList<Reservation>>>

    @POST("api/v1/establishments/{establishment_id}/reservations/{reservation_id}/{type}")
    fun confirmReservation(
        @Path("establishment_id") establishmentId: Int,
        @Path("reservation_id") reservationId: Int,
        @Path("type") type: String
    ): Observable<Any>

    @POST("api/v1/reservations/{reservation_id}/reviews")
    fun saveReview(
        @Path("reservation_id") reservationId: Int,
        @Query("rate") rate: Int
    ): Observable<Any>

    @GET("api/v1/services")
    fun getServices(): Observable<SearchResult<ArrayList<Establishment.Services>>>

    @POST("api/v1/establishments/{establishment_id}/services")
    fun activeService(
        @Path("establishment_id") establishmentId: Int,
        @Query("service_id") serviceId: Int
    ): Observable<Any>

    @DELETE("api/v1/establishments/{establishment_id}/services/{service_id}")
    fun deleteService(
        @Path("establishment_id") establishmentId: Int,
        @Path("service_id") serviceId: Int
    ): Observable<Any>

    @Multipart
    @POST("api/v1/establishments/{establishment_id}/fields")
    fun createField(
        @Path("establishment_id") establishmentId: Int,
        @Part("name") name: String,
        @Part("game_types[]") gameTypes: Int,
        @Part("grass_type") grassType: Int,
        @Part("is_active") isActive: Int,
        @Part("price_type[]") priceType: Int,
        @Part("price_hour[]") priceHour: Int,
        @Part("price_half_hour[]") priceHalfHour: Int
    ): Observable<SearchResult<com.terry.tureta.model.Field>>

    @DELETE("api/v1/establishments/{establishment_id}/fields/{field_id}")
    fun deleteField(
        @Path("establishment_id") establishmentId: Int,
        @Path("field_id") fieldId: Int
    ): Observable<Any>

    @POST("api/v1/reservations")
    @FormUrlEncoded
    fun createReservation(
        @Field("field_id") fieldId: Int,
        @Field("from") from: String,
        @Field("to") to: String,
        @Field("game_type") game_type: String = "11",
        @Field("comments") comments: String = "",
        @Field("type") type: Int = 1,
        @Field("price") price: Int = 0
    ): Observable<Any>

    @POST("api/v1/profile")
    fun updateProfile(
        @Query("name") name: String? = null,
        @Query("color") color: String? = null
    ): Observable<Any>
}