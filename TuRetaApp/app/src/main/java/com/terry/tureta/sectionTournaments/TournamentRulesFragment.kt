package com.terry.tureta.sectionTournaments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.terry.tureta.R
import com.terry.tureta.customViews.CustomFragment
import com.terry.tureta.databinding.FragmentTournamentRulesBinding
import com.terry.tureta.dialogs.NumberPickerDialog
import com.terry.tureta.mediumMargin
import com.terry.tureta.sectionTournaments.viewModel.TournamentViewModel
import grid.apps.supplies_admin.Extensions.setMargins
import grid.apps.supplies_admin.Extensions.smallRegular

class TournamentRulesFragment : CustomFragment() {

    private lateinit var binding: FragmentTournamentRulesBinding

    private val mViewModel by lazy {
        ViewModelProvider(requireActivity())[TournamentViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentTournamentRulesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonMatchWon.setMargins(0, mediumMargin)
        binding.buttonMatchWon.setOnClickListener(this)
        binding.buttonMatchLose.setOnClickListener(this)
        binding.buttonMatchTie.setOnClickListener(this)
        binding.buttonRounds.setOnClickListener(this)

        setLabels(
            binding.textMatchWon,
            binding.textMatchLose,
            binding.textMatchTie,
            binding.textRounds
        )
    }

    override fun onResume() {
        super.onResume()

        binding.textMatchWon.text = mViewModel.mMatchWonPoints.toString()
        binding.textMatchTie.text = mViewModel.mMatchTiePoints.toString()
        binding.textMatchLose.text = mViewModel.mMatchLosePoints.toString()
        binding.textRounds.text = mViewModel.mNumberOfRounds.toString()
    }

    override fun onClick(v: View?) {
        v ?: return

        val mode = when (v) {
            binding.buttonMatchWon -> NumberPickerDialog.MODE_WON
            binding.buttonMatchTie -> NumberPickerDialog.MODE_TIE
            binding.buttonMatchLose -> NumberPickerDialog.MODE_LOSE
            else -> NumberPickerDialog.MODE_ROUNDS
        }

        val action =
            TournamentRulesFragmentDirections.actionTournamentRulesFragmentToNumberPickerDialog(mode)
        findNavController().navigate(action)
    }

    private fun setLabels(vararg textViews: AppCompatTextView) {
        for (textView in textViews) {
            textView.setMargins(0, 0, mediumMargin)
            textView.smallRegular(R.color.grayText)
        }
    }
}