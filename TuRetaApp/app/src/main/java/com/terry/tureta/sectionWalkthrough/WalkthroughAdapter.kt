package com.terry.tureta.sectionWalkthrough

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.terry.tureta.R
import com.terry.tureta.databinding.ItemWalkthroughBinding
import com.terry.tureta.largeMargin
import grid.apps.supplies_admin.Extensions.largeTitleStyle
import grid.apps.supplies_admin.Extensions.setMargins

class WalkthroughAdapter(
    private val context: Context
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VHWalkthrough(
            ItemWalkthroughBinding.inflate(
                LayoutInflater.from(context), parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return 3
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as VHWalkthrough).bindData(position)
    }

    inner class VHWalkthrough(private val binding: ItemWalkthroughBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.textTitle.largeTitleStyle()
            binding.imageIcon.setMargins(0,0,0, largeMargin)
        }

        fun bindData(position: Int) {
            when (position) {
                0 -> {
                    binding.imageIcon.setImageDrawable(
                        ContextCompat.getDrawable(context, R.drawable.ic_walkthrough_1)
                    )
                    binding.textTitle.setText(R.string.label_walkthrough_1)
                }
                1 -> {
                    binding.imageIcon.setImageDrawable(
                        ContextCompat.getDrawable(context, R.drawable.ic_walkthrough_2)
                    )
                    binding.textTitle.setText(R.string.label_walkthrough_2)
                }
                2 -> {
                    binding.imageIcon.setImageDrawable(
                        ContextCompat.getDrawable(context, R.drawable.ic_walkthrough_3)
                    )
                    binding.textTitle.setText(R.string.label_walkthrough_3)
                }
            }
        }

    }

}