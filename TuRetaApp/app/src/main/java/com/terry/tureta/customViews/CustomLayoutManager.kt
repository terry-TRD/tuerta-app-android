package com.terry.tureta.customViews

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlin.math.abs
import kotlin.math.sqrt

class CustomLayoutManager(
    context: Context?,
    private val recyclerView: RecyclerView,
    private val callback: OnItemSelectedListener
) :
    LinearLayoutManager(context) {

    override fun onScrollStateChanged(state: Int) {
        super.onScrollStateChanged(state)

        // When scroll stops we notify on the selected item
        if (state == RecyclerView.SCROLL_STATE_IDLE) {

            // Find the closest child to the recyclerView center --> this is the selected item.
            val recyclerViewCenterY = getRecyclerViewCenterY()
            var minDistance = recyclerView.width
            var position = -1
            for (i in 0 until recyclerView.childCount) {
                val child = recyclerView.getChildAt(i)
                val childCenterY =
                    getDecoratedTop(child) + (getDecoratedBottom(child) - getDecoratedTop(child)) / 2
                val childDistanceFromCenter = abs(childCenterY - recyclerViewCenterY)
                if (childDistanceFromCenter < minDistance) {
                    minDistance = childDistanceFromCenter
                    position = recyclerView.getChildLayoutPosition(child)
                }
            }

            // Notify on the selected item
            callback.onItemSelected(position)
        }
    }

    private fun getRecyclerViewCenterY(): Int {
        return (recyclerView.bottom - recyclerView.top) / 2
    }

    override fun onLayoutChildren(recycler: RecyclerView.Recycler?, state: RecyclerView.State) {
        super.onLayoutChildren(recycler, state)
        scaleDownView()
    }

    override fun scrollVerticallyBy(
        dy: Int,
        recycler: RecyclerView.Recycler?,
        state: RecyclerView.State?
    ): Int {
        return if (orientation == VERTICAL) {
            val scrolled = super.scrollVerticallyBy(dy, recycler, state)
            scaleDownView()
            scrolled
        } else {
            0
        }
    }

    private fun scaleDownView() {
        val mid = height / 2.0f
        for (i in 0 until childCount) {

            // Calculating the distance of the child from the center
            val child = getChildAt(i)
            child ?: return
            val childMid = (getDecoratedTop(child) + getDecoratedBottom(child)) / 2.0f
            val distanceFromCenter = abs(mid - childMid)

            // The scaling formula
            val scale = 1 - sqrt((distanceFromCenter / height).toDouble()).toFloat() * 0.7f

            // Set scale to view
//            child.scaleX = scale
            child.alpha = scale
            child.scaleY = scale
        }
    }

    interface OnItemSelectedListener {
        fun onItemSelected(layoutPosition: Int)
    }
}