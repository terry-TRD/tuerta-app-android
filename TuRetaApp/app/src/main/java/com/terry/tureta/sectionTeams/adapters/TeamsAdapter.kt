package com.terry.tureta.sectionTeams.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.terry.tureta.databinding.ItemTeamBinding
import com.terry.tureta.helpers.CircleTransform
import com.terry.tureta.mediumMargin
import com.terry.tureta.model.Team
import com.terry.tureta.smallMargin
import grid.apps.supplies_admin.Extensions.setMargins
import grid.apps.supplies_admin.Extensions.smallRegular

class TeamsAdapter(
    private val list: List<Team>,
    private val context: Context,
    private val callback: ITeamsAdapter,
    private val multipleSelection: Boolean
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VHItem(
            ItemTeamBinding.inflate(
                LayoutInflater.from(context), parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as VHItem).bindData(list[position], position)
    }

    inner class VHItem(private val binding: ItemTeamBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            if (multipleSelection) binding.textName.setMargins(smallMargin, 0, smallMargin)
            else binding.textName.setMargins(
                smallMargin,
                smallMargin * 2,
                smallMargin,
                smallMargin * 2
            )
            binding.textName.smallRegular()

            binding.imageTeam.setMargins(mediumMargin, smallMargin * 2, 0, smallMargin * 2)

//            if (!multipleSelection) binding.switchSelected.visibility = View.GONE
//            binding.switchSelected.setMargins(0, smallMargin, mediumMargin, smallMargin)
            if (!multipleSelection) binding.buttonDeleteTeam.visibility = View.GONE
            binding.buttonDeleteTeam.setMargins(0, smallMargin * 2, mediumMargin, smallMargin * 2)
            binding.buttonDeleteTeam.updateSelected(true)

            binding.separator.setMargins(mediumMargin, 0, mediumMargin)
        }

        fun bindData(team: Team, position: Int) {
            binding.separator.visibility = if (position == 0) View.GONE
            else View.VISIBLE

            binding.textName.text = team.name

            Picasso.get()
                .load(team.image + "?w=150")
                .transform(CircleTransform(context, true, true))
                .into(binding.imageTeam)

            binding.buttonDeleteTeam.setOnClickListener {
                callback.onDeleteTeam(team)
            }

//            binding.switchSelected.isChecked = team.selected
//            binding.switchSelected.setOnClickListener {
//                callback.onTeamSelected(team, binding.switchSelected.isChecked)
//            }
//
            binding.layoutItem.setOnClickListener {
                callback.onTeamSelected(team, true)
            }
        }

    }

    interface ITeamsAdapter {
        fun onTeamSelected(team: Team, isSelected: Boolean)
        fun onDeleteTeam(team: Team)
    }

}