package com.terry.tureta.sectionWeb

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebViewClient
import androidx.navigation.fragment.navArgs

import com.terry.tureta.R
import kotlinx.android.synthetic.main.fragment_web.view.*


class WebFragment : Fragment() {

    companion object {
        const val BASE_URL = "http://tureta.com.mx/"
        const val PDF_READER = "http://docs.google.com/viewer?url="
    }

    private val args: WebFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_web, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.webView.webViewClient = WebViewClient()

        val isPdf = args.url == getString(R.string.key_privacy)

        val newUrl = if (!isPdf) BASE_URL + args.url else PDF_READER + BASE_URL + args.url
        view.webView.settings.javaScriptEnabled = true
        view.webView.loadUrl(newUrl)
//        view.webView.loadUrl("http://google.com")
    }

}
