package com.terry.tureta.sectionLogin

import android.os.Bundle
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController

import com.terry.tureta.R
import com.terry.tureta.customViews.CustomFragment
import com.terry.tureta.databinding.FragmentRestorePasswordBinding
import com.terry.tureta.largeMargin
import com.terry.tureta.mediumMargin
import com.terry.tureta.sectionLogin.viewModel.LoginViewModel
import grid.apps.supplies_admin.Extensions.*
import io.reactivex.disposables.Disposable

class RestorePasswordFragment : CustomFragment() {

    private lateinit var binding: FragmentRestorePasswordBinding
    private lateinit var disposable: Disposable

    private val mViewModel by lazy {
            ViewModelProvider(requireActivity() )[LoginViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentRestorePasswordBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.labelRestorePassword.largeBold()
        binding.labelRestorePassword.setMargins(mediumMargin, largeMargin, mediumMargin)

        binding.labelMessageRestore.smallRegular()
        binding.labelMessageRestore.setMargins(mediumMargin, mediumMargin, mediumMargin)

        setEditText(binding.layoutEmail, binding.textEmail)
        binding.layoutEmail.setMargins(mediumMargin, largeMargin * 2, mediumMargin)
        binding.textEmail.addTextChangedListener(this)

        binding.buttonSend.setMargins(mediumMargin, largeMargin * 2, mediumMargin, largeMargin)
        binding.buttonSend.setOnClickListener {
            if (validateData())
                restorePassword(
                    binding.textEmail.text.toString().trim()
                )
        }
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        when (s.hashCode()) {
            binding.textEmail.text.hashCode() -> {
                binding.layoutEmail.error = ""
            }
        }
    }

    private fun validateData(): Boolean {
        var count = 0

        if (binding.textEmail.text.toString().trim().isEmpty()) {
            count++
            binding.layoutEmail.error = getString(R.string.label_field_empty)
        } else if (
            !Patterns.EMAIL_ADDRESS.matcher(
                binding.textEmail.text.toString().trim()
            ).matches()
        ) {
            count++
            binding.layoutEmail.error = getString(R.string.label_email_invalid)
        }

        return count == 0
    }

    private fun restorePassword(email: String) {
        val action =
            RestorePasswordFragmentDirections.actionRestorePasswordFragmentToLoadDialog()
//        action.loadMessage = getString(R.string.label_restoring_password)
        findNavController().navigate(action)

        mViewModel.restorePassword(email)
    }

}
