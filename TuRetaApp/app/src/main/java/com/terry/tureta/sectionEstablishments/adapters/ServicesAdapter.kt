package com.terry.tureta.sectionEstablishments.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.terry.tureta.*
import com.terry.tureta.api.ApiUtils
import com.terry.tureta.databinding.ItemServiceBinding
import com.terry.tureta.databinding.ItemServiceEditableBinding
import com.terry.tureta.model.Establishment
import grid.apps.supplies_admin.Extensions.mediumRegular
import grid.apps.supplies_admin.Extensions.setMargins
import grid.apps.supplies_admin.Extensions.smallRegular
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.item_service.view.iconService
import kotlinx.android.synthetic.main.item_service.view.textName

class ServicesAdapter(
    private val context: Context,
    private val list: List<Establishment.Services>,
    private val mEditable: Boolean = false,
    private val establishmentId: Int = 0,
    private val callback: IServicesAdapter? = null
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (!mEditable)
            VHService(
                ItemServiceBinding.inflate(
                    LayoutInflater.from(context), parent, false
                )
            )
        else VHServiceEditable(
            ItemServiceEditableBinding.inflate(
                LayoutInflater.from(context), parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is VHService) holder.bindData(list[position], position, list.size)
        else if (holder is VHServiceEditable) holder.bindData(list[position], position, itemCount)
    }

    inner class VHService(private val binding: ItemServiceBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.textName.smallRegular(R.color.grayText)

            binding.iconService.setMargins(0, elevationHeight.toInt(), 0, elevationHeight.toInt())
        }

        fun bindData(service: Establishment.Services, pos: Int, size: Int) {
            Picasso.get().load(service.icon).into(binding.iconService)

            binding.textName.text = service.name
            binding.textName.setMargins(smallMargin, smallMargin, mediumMargin, smallMargin)
        }

    }

    inner class VHServiceEditable(private val binding: ItemServiceEditableBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private lateinit var disposable: Disposable

        init {
            binding.iconService.setMargins(mediumMargin, smallMargin, 0, smallMargin)

            binding.textName.mediumRegular()
            binding.textName.setMargins(mediumMargin, smallMargin, mediumMargin, smallMargin)

            binding.switchEnabled.setMargins(0, 0, mediumMargin)

            binding.separator.setMargins(mediumMargin, 0, mediumMargin)
        }

        fun bindData(service: Establishment.Services, pos: Int, size: Int) {
            if (pos == 0) {
                binding.separator.visibility = View.INVISIBLE
            }
            Picasso.get().load(service.icon).into(itemView.iconService)

            binding.textName.text = service.name
            binding.switchEnabled.isChecked = service.selected
            binding.switchEnabled.setOnClickListener {
                binding.switchEnabled.isEnabled = false
                val query = if (binding.switchEnabled.isChecked) {
                    ApiUtils.apiService
                        .activeService(establishmentId, service.id)

                } else {
                    ApiUtils.apiService
                        .deleteService(establishmentId, service.id)
                }

                disposable = query
                    .subscribeOn(Schedulers.io())
                    .unsubscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                        {
                            service.selected = binding.switchEnabled.isChecked
                            binding.switchEnabled.isEnabled = true

                            callback?.onServiceEnabled(service.selected, service)

                            disposable.dispose()
                        },
                        { error ->
                            Log.e("ERROR", error.message!!)
                            Toast.makeText(
                                context,
                                R.string.message_service_error,
                                Toast.LENGTH_LONG
                            )
                                .show()
                            binding.switchEnabled.isEnabled = true
                            binding.switchEnabled.isChecked =
                                !binding.switchEnabled.isChecked
                            disposable.dispose()
                        },
                        { Log.d("TAG", "completed") }
                    )
            }
        }
    }

    interface IServicesAdapter {
        fun onServiceEnabled(isEnable: Boolean, service: Establishment.Services)
    }

}