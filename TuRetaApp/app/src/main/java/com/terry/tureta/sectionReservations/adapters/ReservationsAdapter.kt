package com.terry.tureta.sectionReservations.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.terry.tureta.R
import com.terry.tureta.databinding.ItemReservationBinding
import com.terry.tureta.elevationHeight
import com.terry.tureta.mediumMargin
import com.terry.tureta.model.Reservation
import com.terry.tureta.smallMargin
import grid.apps.supplies_admin.Extensions.*

class ReservationsAdapter(
    private val context: Context,
    private val list: List<Reservation>,
    private val isAdmin: Boolean,
    private val callback: IReservations
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VHItem(
            ItemReservationBinding.inflate(
                LayoutInflater.from(context), parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as VHItem).bindData(list[position], position)
    }

    inner class VHItem(private val binding: ItemReservationBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.textName.smallBold(R.color.black)
            binding.textName.setMargins(smallMargin, smallMargin, smallMargin)
            binding.textDate.setMargins(smallMargin, smallMargin)
            binding.textHours.setMargins(smallMargin, smallMargin, 0, smallMargin)
            binding.separator.setMargins(smallMargin, smallMargin)
            binding.textCode.smallBold(R.color.grayText)
            binding.textCode.setMargins(0, smallMargin, smallMargin)
            binding.textUserName.setMargins(smallMargin, smallMargin, mediumMargin)
            binding.textStatus.setMargins(smallMargin)
            binding.textLocation.setMargins(smallMargin, smallMargin, smallMargin)

            setLabels(
                binding.textHours,
                binding.textDate,
                binding.textUserName,
                binding.textLocation
            )

            setImages(
                binding.imageUser,
                binding.imageLocation,
                binding.imageCalendar,
                binding.imageClock
            )
        }

        private fun setLabels(vararg textViews: AppCompatTextView) {
            for (text in textViews) {
                text.compoundDrawablePadding = smallMargin
                text.extraSmallRegular(R.color.black)
            }
        }

        private fun setImages(vararg imageViews: AppCompatImageView) {
            for (image in imageViews) {
                image.setMargins(smallMargin, 0, 0, elevationHeight.toInt())
            }
        }

        fun bindData(reservation: Reservation, position: Int) {
            if (isAdmin) {
                binding.textCode.visibility = View.VISIBLE
                binding.textLocation.visibility = View.INVISIBLE
                binding.imageField.visibility = View.GONE
//                binding.layoutRating.visibility = View.INVISIBLE
                binding.textUserName.visibility = View.VISIBLE
                binding.imageLocation.visibility = View.INVISIBLE
                binding.imageUser.visibility = View.VISIBLE
            } else {
                binding.textCode.visibility = View.GONE
                binding.textLocation.visibility = View.VISIBLE
                binding.imageField.visibility = View.VISIBLE
                binding.textUserName.visibility = View.INVISIBLE
//                binding.layoutRating.visibility = View.VISIBLE
                binding.imageLocation.visibility = View.VISIBLE
                binding.imageUser.visibility = View.INVISIBLE
            }

            binding.textLocation.text = reservation.establishment.address

            binding.textStatus.text =
                context.resources.getStringArray(R.array.reservation_status)[reservation.status - 1]
            binding.textStatus.extraSmallRegular()
            binding.textStatus.setTextColor(reservation.getColor(context))

            if (reservation.establishment.image.isNotEmpty())
                Picasso.get().load(reservation.establishment.image).into(binding.imageField)

            if (reservation.field != null)
                binding.textName.text =
                    context.getString(R.string.label_field_name, reservation.field!!.name)
            binding.textCode.text = reservation.code

            if (reservation.user != null)
                binding.textUserName.text = reservation.user!!.name

            binding.labelManual.layoutParams.width = if (reservation.type == 2) smallMargin else 0

            binding.textDate.text = reservation.getDate(context)
            binding.textHours.text = reservation.getHours()

            binding.cardItem.setOnClickListener { callback.onReservationSelected(reservation) }
        }

    }

    interface IReservations {
        fun onReservationSelected(reservation: Reservation)
    }

}