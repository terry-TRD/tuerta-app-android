package com.terry.tureta.api

interface AsyncResult {
    fun onSuccess(any: Any, code: Int)
    fun onFailure(message: String?, code: Int)
}