package com.terry.tureta.sectionTournaments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.terry.tureta.R
import com.terry.tureta.customViews.CustomFragment
import com.terry.tureta.databinding.FragmentTournamentBinding
import com.terry.tureta.mediumMargin
import grid.apps.supplies_admin.Extensions.setMargins

class TournamentFragment : CustomFragment() {

    private lateinit var binding: FragmentTournamentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTournamentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonCalendar.setMargins(0, mediumMargin)
        binding.buttonCalendar.setOnClickListener(this)
        binding.buttonStatistics.setOnClickListener(this)
        binding.buttonEdit.setOnClickListener(this)
        binding.buttonEdit.setMargins(mediumMargin, 0, mediumMargin, mediumMargin)
    }

    override fun onClick(v: View?) {
        v ?: return
        when (v) {
            binding.buttonCalendar -> {
                findNavController().navigate(R.id.action_tournamentFragment_to_calendarFragment)
            }
            binding.buttonStatistics -> {
                findNavController().navigate(R.id.action_tournamentFragment_to_tournamentStatsFragment)
            }
            binding.buttonEdit -> {
                findNavController().navigate(R.id.action_tournamentFragment_to_editTournamentFragment)
            }
        }
    }

}