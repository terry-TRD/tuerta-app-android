package com.terry.tureta.sectionProfile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.terry.tureta.R
import com.terry.tureta.customViews.CustomFragment
import com.terry.tureta.databinding.FragmentEditProfileBinding
import com.terry.tureta.mediumMargin
import com.terry.tureta.sectionMain.viewModel.MainViewModel
import grid.apps.supplies_admin.Extensions.largeTitleStyle
import grid.apps.supplies_admin.Extensions.setMargins

class EditProfileFragment : CustomFragment() {

    private lateinit var binding: FragmentEditProfileBinding

    private val mViewModel by lazy {
        activity?.run {
            val model = ViewModelProvider(this)[MainViewModel::class.java]
            model
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentEditProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.textInitials.largeTitleStyle(R.color.primaryDark)

        setEditText(binding.layoutName, binding.textName)
        setEditText(binding.layoutEmail, binding.textEmail)

        binding.layoutName.setMargins(mediumMargin, mediumMargin, mediumMargin)
        binding.layoutEmail.setMargins(mediumMargin, 0, mediumMargin)

        binding.buttonUpdate.setMargins(mediumMargin, 0, mediumMargin, mediumMargin)
        binding.buttonUpdate.setOnClickListener {
            val action = EditProfileFragmentDirections.actionEditProfileFragmentToLoadDialog()
            action.message = getString(R.string.label_updating_profile)
            findNavController().navigate(action)

            mViewModel?.updateProfile(binding.textName.text.toString().trim())
        }

        binding.buttonChange.setOnClickListener { findNavController().navigate(R.id.action_editProfileFragment_to_profileIconFragment) }
        binding.imageShirt.setOnClickListener { findNavController().navigate(R.id.action_editProfileFragment_to_profileIconFragment) }
    }

    override fun onResume() {
        super.onResume()

        if (mViewModel == null) return
        binding.textName.setText(mViewModel!!.getUser().name)
        binding.textEmail.setText(mViewModel!!.getUser().email)
        binding.textInitials.text = mViewModel!!.getUser().getInitials()
        binding.imageShirt.setColorFilter(mViewModel!!.getUser().getColor(requireContext()))
    }
}