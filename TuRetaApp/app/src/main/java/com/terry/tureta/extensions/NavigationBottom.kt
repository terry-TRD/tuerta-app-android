package com.terry.tureta.extensions

import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.terry.tureta.poppinsRegular

fun BottomNavigationView.setTypeface() {
    val nav = (this.getChildAt(0) as ViewGroup)
    for (count in 0 until nav.childCount) {
        val tab = nav.getChildAt(count)
        if (tab is ViewGroup) {
            for (i in 0 until tab.childCount) {
                val child = tab.getChildAt(i)
                if (child is ViewGroup) {
                    for (x in 0 until child.childCount) {
                        val text = child.getChildAt(x)
                        if (text is AppCompatTextView) {
                            text.typeface = poppinsRegular
                        }
                    }
                }
            }
        }
    }
}