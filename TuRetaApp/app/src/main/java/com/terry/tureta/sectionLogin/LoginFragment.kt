package com.terry.tureta.sectionLogin

import android.os.Bundle
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.terry.tureta.R

import com.terry.tureta.customViews.CustomFragment
import com.terry.tureta.databinding.FragmentLoginBinding
import com.terry.tureta.mediumMargin
import com.terry.tureta.sectionLogin.viewModel.LoginViewModel
import grid.apps.supplies_admin.Extensions.*

class LoginFragment : CustomFragment() {

    private lateinit var binding: FragmentLoginBinding
    private val mViewModel by lazy {
        activity?.run {
            ViewModelProvider(this)[LoginViewModel::class.java]
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setEditText(binding.layoutEmail, binding.textEmail)
        binding.layoutEmail.setMargins(mediumMargin, mediumMargin, mediumMargin)
        binding.textEmail.addTextChangedListener(this)

        setEditText(binding.layoutPassword, binding.textPassword)
        binding.layoutPassword.setMargins(mediumMargin, 0, mediumMargin)
        binding.textPassword.addTextChangedListener(this)

        binding.buttonLogin.setMargins(mediumMargin, 0, mediumMargin)
        binding.buttonLogin.setOnClickListener(this)

        binding.buttonFacebook.setMargins(mediumMargin, 0, mediumMargin)
        binding.buttonFacebook.setOnClickListener(this)

        binding.buttonForgotPassword.setMargins(0, 0, mediumMargin)
        binding.buttonForgotPassword.setOnClickListener(this)

        binding.labelRegister.smallRegular()
        binding.labelRegister.setMargins(mediumMargin)

        binding.buttonRegister.setOnClickListener(this)
        binding.buttonRegister.setMargins(mediumMargin)

        binding.labelLoginInstead.smallRegular()
        binding.labelLoginInstead.setPadding(mediumMargin, 0, mediumMargin, 0)
        binding.separator.setMargins(mediumMargin, 0, mediumMargin)
    }

    override fun onResume() {
        super.onResume()

        binding.textEmail.setText(mViewModel!!.getEmail())
        binding.textPassword.setText(mViewModel!!.getPassword())
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.buttonLogin -> if (validateData()) onLogin()
            binding.buttonRegister -> findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
            binding.buttonForgotPassword -> findNavController().navigate(R.id.action_loginFragment_to_restorePasswordFragment)
            binding.buttonFacebook -> {
                val action =
                    LoginFragmentDirections.actionLoginFragmentToLoadDialog()
//                action.loadMessage = getString(R.string.label_logging_in)
                findNavController().navigate(action)

                mViewModel?.loginWithFacebook(requireActivity())
            }
        }
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        when (s.hashCode()) {
            binding.textEmail.text.hashCode() -> {
                mViewModel!!.setEmail(s.toString())
                binding.layoutEmail.error = ""
            }
            binding.textPassword.text.hashCode() -> {
                mViewModel!!.setPassword(s.toString())
                binding.layoutPassword.error = ""
            }
        }
    }

    private fun validateData(): Boolean {
        var count = 0

        if (binding.textEmail.text.toString().trim().isEmpty()) {
            count++
            binding.layoutEmail.error = getString(R.string.label_field_empty)
        } else if (
            !Patterns.EMAIL_ADDRESS.matcher(
                binding.textEmail.text.toString().trim()
            ).matches()
        ) {
            count++
            binding.layoutEmail.error = getString(R.string.label_email_invalid)
        }

        if (binding.textPassword.text.toString().trim().isEmpty()) {
            count++
            binding.layoutPassword.error = getString(R.string.label_field_empty)
        }

        return count == 0
    }

    private fun onLogin() {
        val action =
            LoginFragmentDirections.actionLoginFragmentToLoadDialog()
//        action.loadMessage = getString(R.string.label_logging_in)
        findNavController().navigate(action)

        mViewModel?.login(
            binding.textEmail.text.toString().trim(),
            binding.textPassword.text.toString().trim()
        )
    }

}
