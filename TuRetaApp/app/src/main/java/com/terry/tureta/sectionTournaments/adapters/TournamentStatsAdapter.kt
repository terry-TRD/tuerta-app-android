package com.terry.tureta.sectionTournaments.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.terry.tureta.R
import com.terry.tureta.databinding.ItemTeamStatsBinding
import com.terry.tureta.helpers.CircleTransform
import com.terry.tureta.model.TournamentStats
import com.terry.tureta.smallMargin
import grid.apps.supplies_admin.Extensions.setMargins
import grid.apps.supplies_admin.Extensions.smallRegular

class TournamentStatsAdapter(
    private val list: List<TournamentStats>,
    private val context: Context
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VHItem(
            ItemTeamStatsBinding.inflate(
                LayoutInflater.from(context), parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is VHItem) holder.bindData(list[position], position)
    }

    inner class VHItem(private val binding: ItemTeamStatsBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.textTeam.setMargins(smallMargin, 0, smallMargin)

            setSmallLabels(
                binding.textPosition,
                binding.textGamesPlayed,
                binding.textGamesWon,
                binding.textGamesTied,
                binding.textGamesLost,
                binding.textDifference
            )

            binding.textPoints.smallRegular()
            binding.textTeam.smallRegular()
            binding.textPosition.setMargins(smallMargin)

            binding.imageTeam.setMargins(smallMargin, smallMargin, 0, smallMargin)
        }

        fun bindData(stats: TournamentStats, position: Int) {
            binding.textPosition.text = "${position + 1}."
            binding.textTeam.text = stats.team.name

            if (stats.team.image.isNotEmpty())
                Picasso.get()
                    .load(stats.team.image + "?w=100")
                    .transform(CircleTransform(context, true, true))
                    .into(binding.imageTeam)

            binding.textGamesPlayed.text = stats.gamesPlayed.toString()
            binding.textGamesWon.text = stats.gamesWon.toString()
            binding.textGamesTied.text = stats.gamesTied.toString()
            binding.textGamesLost.text = stats.gamesLost.toString()
            binding.textDifference.text = stats.difference.toString()
            binding.textPoints.text = stats.points.toString()

            binding.separator.visibility = if (position == 0) View.INVISIBLE else View.VISIBLE
        }

        private fun setSmallLabels(vararg labels: AppCompatTextView) {
            for (label in labels) {
                label.smallRegular(R.color.grayText)
            }
        }

    }

}