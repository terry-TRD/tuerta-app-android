package com.terry.tureta.sectionReservations.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.terry.tureta.R
import com.terry.tureta.databinding.ItemDateBinding
import com.terry.tureta.elevationHeight
import com.terry.tureta.mediumMargin
import com.terry.tureta.model.Establishment
import com.terry.tureta.smallMargin
import grid.apps.supplies_admin.Extensions.setMargins
import grid.apps.supplies_admin.Extensions.smallBold
import java.lang.IndexOutOfBoundsException

class DatesAdapter(
    private val context: Context,
    private val list: List<Establishment.Schedules>,
    private val isStart: Boolean,
    private val callback: IDatesAdapter
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var startPosition: Int = -1
    private var endPosition: Int = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return SchedulesVH(
            ItemDateBinding.inflate(
                LayoutInflater.from(context), parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as SchedulesVH).bindData(list[position], position, itemCount)
    }

    fun setStart(position: Int) {
        if (position == startPosition) {
            startPosition = -1
            notifyDataSetChanged()
            callback.onDatesChanged(getTotalHours())
            return
        }

        if (position < endPosition || endPosition == -1) {
            startPosition = position
            notifyDataSetChanged()
            callback.onDatesChanged(getTotalHours())
        }
    }

    fun clearPositions() {
        startPosition = -1
        endPosition = -1
        notifyDataSetChanged()
        callback.onDatesChanged(0f)
    }

    fun setEnd(position: Int) {
        if (position == endPosition) {
            endPosition = -1
            notifyDataSetChanged()
            callback.onDatesChanged(getTotalHours())
            return
        }

        if (position > 0 && position > startPosition) {
            if (validateHalfHours(position)) {
                if(position - startPosition > 1) {
                    endPosition = position
                    notifyDataSetChanged()
                    callback.onDatesChanged(getTotalHours())
                }
            }
        }
    }

    fun validateStart(): Boolean = startPosition != -1

    fun validateEnd(): Boolean = endPosition != -1

    fun getStart(): String = list[startPosition].from

    fun getEnd(): String = list[endPosition].from

    private fun validateHalfHours(newPosition: Int): Boolean {
        for (count in startPosition until newPosition) {
            if (count > -1)
                if (count < list.size - 2 && list[count].to != list[count + 1].from) return false
        }

        return true
    }

    private fun getTotalHours(): Float {
        if (startPosition == -1 || endPosition == -1) return 0f
        return (endPosition - startPosition) / 2f
    }

    inner class SchedulesVH(private val binding: ItemDateBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.textDate.setMargins(0, smallMargin, 0, smallMargin)
            binding.cardItem.radius = elevationHeight
        }

        fun bindData(schedule: Establishment.Schedules, position: Int, size: Int) {

            if (isStart) {
                schedule.selected = position == startPosition
            } else {
                schedule.selected = position == endPosition
            }

            binding.textDate.text = schedule.getFromHour()
            binding.cardItem.setCardBackgroundColor(
                ContextCompat.getColor(context, schedule.getBackColor())
            )

            if (schedule.selected) {
                binding.textDate.smallBold(R.color.white)
            } else {
                binding.textDate.smallBold(R.color.grayTextDark)
            }

            binding.cardItem.isEnabled = schedule.isEnabled

            when (position) {
                0 -> {
                    binding.cardItem.setMargins(mediumMargin, 0, smallMargin)
                }
                size - 1 -> {
                    binding.cardItem.setMargins(0, 0, mediumMargin)
                }
                else -> {
                    binding.cardItem.setMargins(0, 0, smallMargin)
                }
            }

            binding.cardItem.setOnClickListener { callback.onDateSelected(position, isStart) }
        }

    }

    interface IDatesAdapter {
        fun onDateSelected(position: Int, isStart: Boolean)
        fun onDatesChanged(hours: Float)
    }

}