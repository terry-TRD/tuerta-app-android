package com.terry.tureta.sectionReservations

import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.terry.tureta.*

import com.terry.tureta.adapters.SelectTextAdapter
import com.terry.tureta.databinding.FragmentCreateReservationBinding
import com.terry.tureta.model.Establishment
import com.terry.tureta.model.Field
import com.terry.tureta.sectionEstablishments.viewModel.EstablishmentViewModel
import com.terry.tureta.sectionReservations.adapters.FieldsAdapter
import com.terry.tureta.sectionReservations.adapters.DatesAdapter
import com.terry.tureta.sectionReservations.viewModel.ReservationsViewModel
import com.terry.tureta.sectionMain.viewModel.MainViewModel
import grid.apps.supplies_admin.Extensions.*
import java.text.DecimalFormat
import kotlin.collections.ArrayList

class CreateReservationFragment : BottomSheetDialogFragment(),
    DatesAdapter.IDatesAdapter, MotionLayout.TransitionListener {

    private lateinit var binding: FragmentCreateReservationBinding

    private val playersList: ArrayList<String> = ArrayList()

    private val args: CreateReservationFragmentArgs by navArgs()

    private val mViewModel by lazy {
        activity?.run {
            val model = ViewModelProvider(this)[EstablishmentViewModel::class.java]
            model.setActivity(this)
            model
        }
    }

    private val mReservationsViewModel by lazy {
        activity?.run {
            ViewModelProvider(this)[ReservationsViewModel::class.java]
        }
    }

    private lateinit var fieldsAdapter: FieldsAdapter
    private lateinit var playersAdapterSelect: SelectTextAdapter

    private var fieldSelected: Int = 0
    private var gameTypeSelected: Int = 0

    private lateinit var startDateAdapter: DatesAdapter
    private lateinit var endDateAdapter: DatesAdapter

    private val startDateList: ArrayList<Establishment.Schedules> = ArrayList()
    private val endDateList: ArrayList<Establishment.Schedules> = ArrayList()

    private lateinit var establishment: Establishment

    private lateinit var itemDecoration: RecyclerView.ItemDecoration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //assign style with transparent background
        setStyle(STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentCreateReservationBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.cardBackground.radius = smallMargin.toFloat()

        binding.labelError.largeTitleStyle(R.color.grayTextDark)
        binding.labelError.setMargins(largeMargin, 0, largeMargin)

        binding.layoutError.setMargins(0, mediumMargin, 0, mediumMargin)

        binding.textTitle.largeBold(R.color.black)
        binding.textTitle.setMargins(mediumMargin, mediumMargin, mediumMargin)

        binding.layoutRating.setMargins(mediumMargin)

        binding.textDay.setMargins(0, smallMargin)

        binding.labelPickField.mediumBold(R.color.black)

        setLabels(
            0, mediumMargin,
            binding.labelHourCost,
            binding.labelTotalHours,
            binding.labelTotal,
            binding.labelStart,
            binding.labelEnd
        )

        setLabels(
            mediumMargin, 0,
            binding.textHourCost,
            binding.textTotalHours,
            binding.textTotal
        )

        binding.separator1.setMargins(mediumMargin, mediumMargin, mediumMargin)

        binding.buttonPickField.setOnClickListener {
            binding.labelPickField.setText(R.string.label_pick_field)
            if (binding.recyclerFields.adapter != fieldsAdapter)
                binding.recyclerFields.addItemDecoration(itemDecoration)
            binding.motionLayout.getTransition(R.id.transition_create_reservation).setEnable(true)
            binding.motionLayout.transitionToEnd()
            binding.recyclerFields.adapter = fieldsAdapter
        }

        binding.buttonNumberPlayers.setOnClickListener {
            binding.labelPickField.setText(R.string.label_number_of_players)
            binding.recyclerFields.removeItemDecoration(itemDecoration)
            binding.motionLayout.getTransition(R.id.transition_create_reservation).setEnable(true)
            binding.motionLayout.transitionToEnd()
            binding.recyclerFields.adapter = playersAdapterSelect
        }

        binding.buttonContinue.setOnClickListener {
            if (args.hasDates) {
                if (validateReservation()) createReservation()
            } else {
                findNavController().navigate(R.id.action_createReservationFragment_to_customDatePicker)
            }
        }

        binding.recyclerFields.layoutManager = LinearLayoutManager(requireContext())
        itemDecoration = object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(
                outRect: Rect,
                view: View,
                parent: RecyclerView,
                state: RecyclerView.State
            ) {
                val position = parent.getChildAdapterPosition(view)
                val size = parent.adapter!!.itemCount

                var top = mediumMargin

                if (position == 0) top = 0

                outRect.run {
                    this.top = top
                    right = mediumMargin
                    left = mediumMargin
                }
            }
        }

        binding.recyclerStart.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        binding.recyclerEnd.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)

        if (binding.motionLayout.progress == 0f)
            binding.motionLayout.getTransition(R.id.transition_create_reservation).setEnable(false)
        binding.motionLayout.setTransitionListener(this)
    }

    override fun onResume() {
        super.onResume()

        if (mViewModel == null) return

        establishment = mViewModel!!.getEstablishment()
        binding.textTitle.text = establishment.name
        setRating(establishment.rate)
        binding.textDay.updateText(mViewModel?.getDateLabel())

        if (args.hasDates) {
            binding.buttonContinue.updateText(R.string.label_reserve)

            val user = ViewModelProvider(requireActivity())[MainViewModel::class.java].getUser()
            if (user.isOwner()) {
                binding.layoutReservationCost.typeface = poppinsRegular
                binding.textReservationCost.typeface = poppinsRegular
                binding.layoutReservationCost.setMargins(mediumMargin, 0, mediumMargin)
                binding.switchReservationCost.setMargins(0, 0, mediumMargin)
            } else {
                binding.layoutReservationCost.visibility = View.GONE
                binding.switchReservationCost.visibility = View.GONE
            }
        } else {
            binding.layoutError.visibility = View.VISIBLE

            binding.buttonPickField.visibility = View.GONE
            binding.textSchedule.visibility = View.GONE
            binding.labelStart.visibility = View.GONE
            binding.recyclerStart.visibility = View.GONE
            binding.labelEnd.visibility = View.GONE
            binding.recyclerEnd.visibility = View.GONE
            binding.separator1.visibility = View.GONE
            binding.buttonNumberPlayers.visibility = View.GONE
            binding.labelSummary.visibility = View.GONE
            binding.labelHourCost.visibility = View.GONE
            binding.textHourCost.visibility = View.GONE
            binding.labelTotalHours.visibility = View.GONE
            binding.textTotalHours.visibility = View.GONE
            binding.labelTotal.visibility = View.GONE
            binding.textTotal.visibility = View.GONE

            binding.layoutReservationCost.visibility = View.GONE
            binding.switchReservationCost.visibility = View.GONE

            binding.buttonContinue.updateText(R.string.label_pick_another_date)
        }

        binding.buttonConfirmField.setOnClickListener { binding.motionLayout.transitionToStart() }

        binding.textHourCost.text = establishment.getHourLabel(fieldSelected, gameTypeSelected)

        setAdapters(establishment)
    }

    private fun setAdapters(establishment: Establishment) {

        fieldsAdapter = FieldsAdapter(
            requireContext(),
            establishment.getFields().filter { it.availability.isNotEmpty() },
            establishment.image,
            establishment.name,
            fieldSelected,
            object : FieldsAdapter.IFieldsAdapter {
                override fun onClick(field: Field, position: Int) {
                    fieldSelected = position
                    gameTypeSelected = 0
                    binding.buttonPickField.updateText(
                        getString(R.string.label_field_name, field.name)
                    )
                    binding.buttonNumberPlayers.updateText(getString(R.string.label_number_of_players))

                    binding.textHourCost.text =
                        establishment.getHourLabel(fieldSelected, gameTypeSelected)

                    updateDates(field.availability)

                    playersList.clear()
                    playersList.addAll(establishment.getPlayers(fieldSelected))
                    playersAdapterSelect.notifyDataSetChanged()
                }
            }
        )

        playersList.clear()
        playersList.addAll(establishment.getPlayers(fieldSelected))

        playersAdapterSelect = SelectTextAdapter(
            requireContext(),
            playersList,
            object : SelectTextAdapter.ITextAdapter {
                override fun onTextClick(text: String, position: Int) {
                    binding.buttonNumberPlayers.updateText("$text jugadores")
                    gameTypeSelected = position
                }
            }, gameTypeSelected, false
        )

        startDateAdapter = DatesAdapter(requireContext(), startDateList, true, this)
        binding.recyclerStart.adapter = startDateAdapter

        endDateAdapter = DatesAdapter(requireContext(), endDateList, false, this)
        binding.recyclerEnd.adapter = endDateAdapter

        updateDates(establishment.fields[fieldSelected].availability)
    }

    private fun updateDates(list: List<Establishment.Schedules>) {
        startDateList.clear()
        startDateList.addAll(list)
        startDateAdapter.clearPositions()

        endDateList.clear()
        endDateList.addAll(list)
        endDateAdapter.clearPositions()
    }

    private fun setLabels(
        rightMargin: Int, bottomMargin: Int, vararg textViews: AppCompatTextView
    ) {
        for (textView in textViews) {
            textView.setMargins(mediumMargin, smallMargin, rightMargin, bottomMargin)
            textView.smallRegular(R.color.black)
        }
    }

    private fun setRating(rate: Int) {
        val gray = ContextCompat.getColor(requireContext(), R.color.grayText)
        val green = ContextCompat.getColor(requireContext(), R.color.accent)
        when (rate) {
            0 -> {
                binding.star1.setColorFilter(gray)
                binding.star2.setColorFilter(gray)
                binding.star3.setColorFilter(gray)
                binding.star4.setColorFilter(gray)
                binding.star5.setColorFilter(gray)
            }
            1 -> {
                binding.star1.setColorFilter(green)
                binding.star2.setColorFilter(gray)
                binding.star3.setColorFilter(gray)
                binding.star4.setColorFilter(gray)
                binding.star5.setColorFilter(gray)
            }
            2 -> {
                binding.star1.setColorFilter(green)
                binding.star2.setColorFilter(green)
                binding.star3.setColorFilter(gray)
                binding.star4.setColorFilter(gray)
                binding.star5.setColorFilter(gray)
            }
            3 -> {
                binding.star1.setColorFilter(green)
                binding.star2.setColorFilter(green)
                binding.star3.setColorFilter(green)
                binding.star4.setColorFilter(gray)
                binding.star5.setColorFilter(gray)
            }
            4 -> {
                binding.star1.setColorFilter(green)
                binding.star2.setColorFilter(green)
                binding.star3.setColorFilter(green)
                binding.star4.setColorFilter(green)
                binding.star5.setColorFilter(gray)
            }
            5 -> {
                binding.star1.setColorFilter(green)
                binding.star2.setColorFilter(green)
                binding.star3.setColorFilter(green)
                binding.star4.setColorFilter(green)
                binding.star5.setColorFilter(green)
            }
        }
    }

    private fun validateReservation(): Boolean {
        if (!startDateAdapter.validateStart() || !endDateAdapter.validateEnd()) {
            Toast
                .makeText(requireContext(), R.string.message_select_dates_error, Toast.LENGTH_LONG)
                .show()
            return false
        }
        return true
    }

    private fun createReservation() {

        val actions =
            CreateReservationFragmentDirections.actionCreateReservationFragmentToLoadDialog()
        actions.hasDates = args.hasDates
        actions.message = getString(R.string.label_creating_reservation)
        findNavController().navigate(actions)

        val field = establishment.fields[fieldSelected]

        val user = ViewModelProvider(requireActivity())[MainViewModel::class.java].getUser()

        val type = if (user.isOwner()) ReservationsViewModel.RESERVATION_MANUAL
        else ReservationsViewModel.RESERVATION_NORMAL

        val textPrice = binding.textReservationCost.text.toString()
        val price = if (binding.switchReservationCost.isChecked)
            if (textPrice.isNotEmpty()) textPrice.toInt()
            else 0
        else 0

        mReservationsViewModel?.createReservation(
            field.id,
            startDateAdapter.getStart(),
            endDateAdapter.getEnd(),
            field.getGameType(gameTypeSelected),
            type,
            price
        )
    }

    override fun onDateSelected(position: Int, isStart: Boolean) {
        if (isStart) {
            startDateAdapter.setStart(position)
            endDateAdapter.setStart(position)
        } else {
            startDateAdapter.setEnd(position)
            endDateAdapter.setEnd(position)
        }
    }

    override fun onDatesChanged(hours: Float) {
        binding.textTotalHours.text = hours.toString()

        val price = mViewModel!!.getEstablishment().getHourPrice(fieldSelected, gameTypeSelected)
        val formatter = DecimalFormat("###,###,##0.00")
        val total = price * hours
        binding.textTotal.text = "$" + formatter.format(total) + " MX"
    }

    override fun onTransitionTrigger(p0: MotionLayout?, p1: Int, p2: Boolean, p3: Float) {}

    override fun onTransitionStarted(p0: MotionLayout?, p1: Int, p2: Int) {}

    override fun onTransitionChange(p0: MotionLayout?, p1: Int, p2: Int, progress: Float) {
        if (progress == 0f) {
            binding.motionLayout.getTransition(R.id.transition_create_reservation)
                .setEnable(false)
        }
    }

    override fun onTransitionCompleted(p0: MotionLayout?, p1: Int) {}

}
