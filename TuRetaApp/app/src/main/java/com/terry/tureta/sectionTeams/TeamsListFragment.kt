package com.terry.tureta.sectionTeams

import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.terry.tureta.R
import com.terry.tureta.customViews.CustomFragment
import com.terry.tureta.databinding.FragmentTeamsListBinding
import com.terry.tureta.mediumMargin
import com.terry.tureta.model.Team
import com.terry.tureta.model.Tournament
import com.terry.tureta.sectionTeams.adapters.TeamsAdapter
import com.terry.tureta.sectionTournaments.viewModel.TournamentViewModel
import grid.apps.supplies_admin.Extensions.largeTitleStyle
import grid.apps.supplies_admin.Extensions.setMargins

class TeamsListFragment : CustomFragment(), TeamsAdapter.ITeamsAdapter {

    companion object {
        const val MODE_MULTI_SELECTION = 100
        const val MODE_SINGLE_SELECTION = 200
    }

    private lateinit var binding: FragmentTeamsListBinding

    private val args: TeamsListFragmentArgs by navArgs()

    private val listTeams: ArrayList<Team> = ArrayList()
    private val adapter by lazy {
        TeamsAdapter(listTeams, requireContext(), this, args.mode == MODE_MULTI_SELECTION)
    }

    private val mViewModel by lazy {
        ViewModelProvider(requireActivity())[TournamentViewModel::class.java]
    }

    private var mTournament: Tournament? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentTeamsListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.textMessage.largeTitleStyle()
        binding.textMessage.setMargins(mediumMargin, 0, mediumMargin)

        binding.buttonCreateTeam.setMargins(mediumMargin, 0, mediumMargin, mediumMargin)
        binding.buttonCreateTeam.setOnClickListener(this)

        binding.recyclerTeams.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerTeams.adapter = adapter
        binding.recyclerTeams.addItemDecoration(object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(
                outRect: Rect,
                view: View,
                parent: RecyclerView,
                state: RecyclerView.State
            ) {
                val position = parent.getChildAdapterPosition(view)
                val size = parent.adapter!!.itemCount

                val top = if (position == 0) mediumMargin else 0
                val bottom = if (position == size - 1) mediumMargin else 0

                outRect.run {
                    this.top = top
                    this.bottom = bottom
                }
            }
        })
    }

    override fun onResume() {
        super.onResume()

        mViewModel.addObserver(this)

        this.mTournament = mViewModel.getTournament()

        mViewModel.downLoadTournamentTeams()
//        if (args.mode == MODE_MULTI_SELECTION) mViewModel.downloadTeams()
//        else {
//            mViewModel.downLoadTournamentTeams()
//            listTeams.clear()
//            listTeams.addAll(this.mTournament!!.teams.filter { it.image != null && it.image.isNotEmpty() })
//            adapter.notifyDataSetChanged()
//            validateList()
//        }
    }

    override fun onPause() {
        super.onPause()

        mViewModel.deleteObserver(this)
    }

    override fun onTeamsLoaded(list: List<Team>) {
        listTeams.clear()
        listTeams.addAll(list.filter { it.image != null && it.image.isNotEmpty() })

        val mList = mViewModel.getTeams()
        if (mList != null) {
            for (team in listTeams) {
                for (mTeam in mList) {
                    if (team.id == mTeam.id) {
                        team.selected = true
                    }
                }
            }
        }

        adapter.notifyDataSetChanged()

        validateList()
    }

    private fun validateList() {
        if (listTeams.isEmpty()) {
            binding.buttonCreateTeam.visibility = View.VISIBLE
            binding.textMessage.visibility = View.VISIBLE
            binding.imageView.visibility = View.VISIBLE
        }
    }

    override fun onTeamsFailed(message: Int) {
        showToast(message)
    }

    override fun onClick(v: View?) {
        findNavController().navigate(R.id.action_teamsListFragment_to_createTeamFragment)
    }

    override fun onTeamSelected(team: Team, isSelected: Boolean) {
//        if(args.mode == MODE_MULTI_SELECTION) {
//            if (isSelected) {
//                mViewModel.addTeam(team)
//            } else {
//                mViewModel.removeTeam(team)
//            }
//        } else {
        mViewModel.setMatchTeam(args.teamNumber, team)
        findNavController().navigate(R.id.action_teamsListFragment_to_matchFragment)
//        }
    }

    override fun onDeleteTeam(team: Team) {
        val action = TeamsListFragmentDirections.actionTeamsListFragmentToLoadDialog()
        action.message = getString(R.string.label_deleting_team)
        findNavController().navigate(action)

        mViewModel.deleteTeam(team)
    }

}