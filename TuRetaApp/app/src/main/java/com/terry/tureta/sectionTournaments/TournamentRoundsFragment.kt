package com.terry.tureta.sectionTournaments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import com.terry.tureta.customViews.CustomFragment
import com.terry.tureta.databinding.FragmentTournamentRoundsBinding
import com.terry.tureta.extensions.setStyle
import com.terry.tureta.sectionTournaments.adapters.RoundFragmentsAdapter
import com.terry.tureta.sectionTournaments.viewModel.TournamentViewModel

class TournamentRoundsFragment : CustomFragment(), RoundFragmentsAdapter.IRoundFragmentsAdapter {

    private lateinit var binding: FragmentTournamentRoundsBinding

    private val mViewModel by lazy {
        ViewModelProvider(requireActivity())[TournamentViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTournamentRoundsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.viewPager.adapter = RoundFragmentsAdapter(mViewModel.mNumberOfRounds, childFragmentManager, this)
        binding.viewPager.addOnPageChangeListener(object: ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {            }

            override fun onPageSelected(position: Int) {
                mViewModel.roundNumberSelected = position + 1
            }

        })

        mViewModel.roundNumberSelected = binding.viewPager.currentItem + 1

        binding.tabLayout.setupWithViewPager(binding.viewPager)
        binding.tabLayout.setStyle()
    }

    override fun onRoundSelected(position: Int) {
//        mViewModel.roundNumberSelected = position
    }

}