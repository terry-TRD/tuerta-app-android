package grid.apps.supplies_admin.Extensions

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.view.View
import android.view.ViewAnimationUtils
import android.view.ViewGroup
import android.view.animation.*
import androidx.constraintlayout.widget.ConstraintLayout

fun View.setMargins(left: Int, top: Int = 0, right: Int = 0, bottom: Int = 0) {
    (this.layoutParams as ViewGroup.MarginLayoutParams)
        .setMargins(left, top, right, bottom)
}

fun View.showAnimation() {
    this.visibility = View.VISIBLE
    val scale = ScaleAnimation(0f, 1f, 0f, 1f, this.pivotX, this.pivotY)
    scale.duration = 200
    this.startAnimation(scale)
}

fun View.hideAnimation() {
    val scale = ScaleAnimation(1f, 0f, 1f, 0f, this.pivotX, this.pivotY)
    scale.duration = 200
    scale.setAnimationListener(object : Animation.AnimationListener {
        override fun onAnimationRepeat(animation: Animation?) {

        }

        override fun onAnimationEnd(animation: Animation?) {
            this@hideAnimation.visibility = View.GONE
        }

        override fun onAnimationStart(animation: Animation?) {

        }
    })
    this.startAnimation(scale)
}

fun View.collapseAnimation() {
    if (this.visibility == View.GONE) return

    val anim = TranslateAnimation(0f, 0f, 0f, -this.height.toFloat())
    anim.interpolator = AccelerateDecelerateInterpolator()
    anim.duration = 200
    anim.setAnimationListener(object : Animation.AnimationListener {
        override fun onAnimationRepeat(animation: Animation?) {

        }

        override fun onAnimationEnd(animation: Animation?) {
            this@collapseAnimation.visibility = View.GONE
        }

        override fun onAnimationStart(animation: Animation?) {

        }
    })
    this.startAnimation(anim)
}

fun View.expandAnimation() {
    if (this.visibility == View.VISIBLE) return

    this.visibility = View.VISIBLE
    val anim = TranslateAnimation(0f, 0f, -this.height.toFloat(), 0f)
    anim.interpolator = AccelerateDecelerateInterpolator()
    anim.duration = 200
    this.startAnimation(anim)
}

fun View.circularReveal() {
    if(this.visibility == View.VISIBLE) return

    this.visibility = View.VISIBLE
    val cx = this.measuredWidth / 2
    val cy = this.measuredHeight / 2

    val radius = this.width.coerceAtLeast(this.height) / 2f

    val anim = ViewAnimationUtils.createCircularReveal(this, cx, cy, 0f, radius)

    anim.start()
}

fun View.circularHide() {
    if(this.visibility == View.GONE) return

    val cx = this.measuredWidth / 2
    val cy = this.measuredHeight / 2

    val radius = this.width.coerceAtLeast(this.height) / 2f

    val anim = ViewAnimationUtils.createCircularReveal(this, cx, cy, radius, 0f)

    anim.addListener(object: Animator.AnimatorListener{
        override fun onAnimationRepeat(animation: Animator?) {

        }

        override fun onAnimationEnd(animation: Animator?) {
            this@circularHide.visibility = View.GONE
        }

        override fun onAnimationCancel(animation: Animator?) {

        }

        override fun onAnimationStart(animation: Animator?) {

        }

    })

    anim.start()
}

fun View.fadeIn() {
    if (this.visibility == View.VISIBLE) return

    this.visibility = View.VISIBLE
    val fade = AlphaAnimation(0f, 1f)
    fade.duration = 200
    this.startAnimation(fade)
}

fun View.fadeOut(callback: Animation.AnimationListener? = null) {
    if (this.visibility == View.INVISIBLE) {
        callback?.onAnimationEnd(null)
        return
    }

    val scale = AlphaAnimation(1f, 0f)
    scale.duration = 200
    scale.setAnimationListener(object : Animation.AnimationListener {
        override fun onAnimationRepeat(animation: Animation?) {

        }

        override fun onAnimationEnd(animation: Animation?) {
            this@fadeOut.visibility = View.INVISIBLE
            callback?.onAnimationEnd(animation)
        }

        override fun onAnimationStart(animation: Animation?) {

        }
    })
    this.startAnimation(scale)
}