package com.terry.tureta.sectionTournaments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import com.terry.tureta.customViews.CustomFragment
import com.terry.tureta.databinding.FragmentTournamentUserBinding
import com.terry.tureta.extensions.setStyle
import com.terry.tureta.mediumFont
import com.terry.tureta.sectionTournaments.adapters.TournamentFragmentsAdapter
import com.terry.tureta.sectionTournaments.viewModel.TournamentViewModel
import grid.apps.supplies_admin.Extensions.setToolbar

class TournamentUserFragment : CustomFragment() {

    private lateinit var binding: FragmentTournamentUserBinding

    private val mViewModel by lazy {
        ViewModelProvider(requireActivity())[TournamentViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTournamentUserBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.toolbar.title = mViewModel.getTournament()!!.name
        binding.toolbar.setToolbar(mediumFont, true)

        binding.viewPager.adapter =
            TournamentFragmentsAdapter(requireContext(), childFragmentManager)

        binding.tabLayout.setupWithViewPager(binding.viewPager)
        binding.tabLayout.setStyle()
    }

}