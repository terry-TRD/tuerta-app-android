package com.terry.tureta.sectionEstablishments.viewModel

import android.app.Activity
import android.util.Log
import androidx.appcompat.widget.SwitchCompat
import androidx.lifecycle.ViewModel
import com.terry.tureta.api.ApiUtils
import com.terry.tureta.api.AsyncResult
import com.terry.tureta.R
import com.terry.tureta.model.Establishment
import com.terry.tureta.model.Field
import com.terry.tureta.model.SearchResult
import com.terry.tureta.model.Tournament
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.http.Part
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

class EstablishmentViewModel : ViewModel() {

    private lateinit var mActivity: Activity

    private lateinit var mEstablishment: Establishment
    private var mSelectedField: Field? = null

    private var mUserId: Int = -1
    private val mTournaments: ArrayList<Tournament> = ArrayList()

    private var selectedDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
    private var selectedMonth = Calendar.getInstance().get(Calendar.MONTH)
    private var selectedYear = Calendar.getInstance().get(Calendar.YEAR)

    var gameTypeSelected: Int = -1
    var grassTypeSelected: Int = -1

    private lateinit var detailsDisposable: Disposable
    private lateinit var availableDisposable: Disposable
    private lateinit var favoriteDisposable: Disposable
    private lateinit var fieldDisposable: Disposable

    private val mObservers: ArrayList<IEstablishmentObserver> = ArrayList()

    fun initializeDetails() {
        if (!mEstablishment.detailsDownloaded) {
            getEstablishmentDetails(mEstablishment.id, object : AsyncResult {
                override fun onSuccess(any: Any, code: Int) {
                    getResult(any as SearchResult<Establishment>)
                }

                override fun onFailure(message: String?, code: Int) {
                    for (observer in mObservers) {
                        observer.onDetailsFailed(R.string.message_details_failed)
                    }
                }

            })
        }
    }

    private fun getEstablishmentDetails(id: Int, callback: AsyncResult) {
        detailsDisposable = ApiUtils.apiService
            .getEstablishmentDetails(id)
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    callback.onSuccess(result, 0)
                    detailsDisposable.dispose()
                },
                { error ->
                    callback.onFailure(error.message, 0)
                    detailsDisposable.dispose()
                },
                { Log.d("TAG", "completed") }
            )
    }

    private fun getResult(court: SearchResult<Establishment>) {
        this.mEstablishment.detailsDownloaded = true
        this.mEstablishment.reservationMessage = court.data!!.reservationMessage
        this.mEstablishment.schedules = court.data!!.schedules
        this.mEstablishment.services = court.data!!.services
        this.mEstablishment.fields = court.data!!.fields

        for (observer in mObservers) {
            observer.onUpdateView()
        }
    }

    fun setFavorite(isFavorite: Boolean) {
        favoriteDisposable = if (isFavorite)
            ApiUtils.apiUser
                .addFavoriteField(mUserId, mEstablishment.id)
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { result ->
                        for (observer in mObservers) {
                            observer.onFavoriteFinished(true)
                        }

                        favoriteDisposable.dispose()
                    },
                    { error ->
                        for (observer in mObservers) {
                            observer.onFavoriteFinished(false)
                        }
                        favoriteDisposable.dispose()
                    },
                    { Log.d("TAG", "completed") }
                )
        else ApiUtils.apiUser
            .deleteFavoriteField(mUserId, mEstablishment.id)
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    for (observer in mObservers) {
                        observer.onFavoriteFinished(true)
                    }

                    favoriteDisposable.dispose()
                },
                { error ->
                    for (observer in mObservers) {
                        observer.onFavoriteFinished(false)
                    }
                    favoriteDisposable.dispose()
                },
                { Log.d("TAG", "completed") }
            )
    }

    fun clearTournaments() {
        mTournaments.clear()
    }

    fun setNewDate(year: Int, month: Int, day: Int) {
        selectedDay = day
        selectedMonth = month
        selectedYear = year

        availableDisposable = ApiUtils.apiService
            .getEstablishmentAvailability(mEstablishment.id, getDateFormatted())
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    var hasDates = false
                    for (field in result.data!!) {
                        if (field.availability.isNotEmpty()) {
                            hasDates = true
                            break
                        }
                    }

                    try {
                        for (count in 0 until result.data!!.size) {
                            for (date in result.data!![count].availability) {
                                date.isEnabled = true
                                mEstablishment.fields[count].availability.add(date)
                            }
                        }
                    } catch (e: Exception) {
                        Log.e("ERROR", e.message, e)
                    }
                    for (observer in mObservers) {
                        observer.onUpdateDates(hasDates)
                    }

                    availableDisposable.dispose()
                },
                { error ->
                    for (observer in mObservers) {
                        observer.onUpdateDates(false)
                    }
                    availableDisposable.dispose()
                },
                { Log.d("TAG", "completed") }
            )
    }

    fun createField(
        name: String,
        gameTypes: Int,
        grassType: Int,
        isActive: Boolean,
        hourPrice: Int,
        halfHourPrice: Int
    ) {
        val active = if (isActive) 1 else 0
        fieldDisposable = ApiUtils.apiService
            .createField(
                mEstablishment.id,
                name,
                gameTypes,
                grassType,
                active,
                gameTypes,
                hourPrice,
                halfHourPrice
            )
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    for (observer in mObservers) {
                        observer.onFieldCreated(true, result.data)
                    }
                    fieldDisposable.dispose()
                },
                { error ->
                    for (observer in mObservers) {
                        observer.onFieldCreated(false, null)
                    }
                    fieldDisposable.dispose()
                },
                { Log.d("TAG", "completed") }
            )
    }

    fun updateField(
        field: Field,
        name: String? = null,
        gameTypes: String? = null,
        grassType: String? = null,
        isActive: Boolean? = null,
        priceType: String? = null,
        hourPrice: String? = null,
        halfHourPrice: String? = null,
        switch: SwitchCompat? = null
    ) {
        val active = if (isActive != null) {
            if (isActive) 1 else 0
        } else {
            null
        }
        fieldDisposable = ApiUtils.apiService
            .updateField(
                mEstablishment.id,
                field.id,
                name,
                gameTypes,
                grassType,
                active,
                priceType,
                hourPrice,
                halfHourPrice
            )
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    if (switch == null) {
                        for (observer in mObservers) {
                            observer.onFieldEdited(true, result.data)
                        }
                    } else {
                        switch.isEnabled = true

                        field.isActive = switch.isChecked
                    }

                    fieldDisposable.dispose()
                },
                { error ->
                    if (switch == null) {
                        for (observer in mObservers) {
                            observer.onFieldEdited(false, null)
                        }
                    } else {
                        switch.isEnabled = true
                        switch.isChecked = !isActive!!
                    }
                    fieldDisposable.dispose()
                },
                { Log.d("TAG", "completed") }
            )
    }

    fun deleteField() {
        fieldDisposable = ApiUtils.apiService
            .deleteField(mEstablishment.id, mSelectedField!!.id)
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    for (observer in mObservers) {
                        observer.onFieldDeleted(true, mSelectedField)
                    }

                    fieldDisposable.dispose()
                },
                { error ->
                    for (observer in mObservers) {
                        observer.onFieldDeleted(true, null)
                    }
                    fieldDisposable.dispose()
                },
                { Log.d("TAG", "completed") }
            )
    }

    fun getDay(): Int = selectedDay
    fun getYear(): Int = selectedYear
    fun getMonth(): Int = selectedMonth

    private fun getDateFormatted(): String {
        val month = if (selectedMonth < 9) "0${selectedMonth + 1}"
        else "${selectedMonth + 1}"
        val day = if (selectedDay < 10) "0$selectedDay"
        else "$selectedDay"
        return "$selectedYear-$month-$day"
    }

    fun getDateLabel(): String =
        mActivity.resources.getStringArray(R.array.date_months)[selectedMonth] + " $selectedDay, $selectedYear"

    fun getScheduleList(): List<Establishment.Schedules> = mEstablishment.schedules

    fun setActivity(activity: Activity): EstablishmentViewModel {
        this.mActivity = activity
        return this
    }

    fun setUserId(userId: Int): EstablishmentViewModel {
        this.mUserId = userId
        return this
    }

    fun setEstablishment(establishment: Establishment) {
        this.mEstablishment = establishment
    }

    fun getEstablishment(): Establishment = mEstablishment

    fun addObserver(observer: IEstablishmentObserver): EstablishmentViewModel {
        for (mObserver in mObservers) {
            if (mObserver == observer) return this
        }
        mObservers.add(observer)
        return this
    }

    fun deleteObserver(observer: IEstablishmentObserver) {
        mObservers.remove(observer)
    }

    fun setField(field: Field?) {
        this.mSelectedField = field
    }

    fun getField(): Field? = this.mSelectedField

    override fun onCleared() {
        super.onCleared()

        mObservers.clear()
    }

    interface IEstablishmentObserver {
        fun onUpdateView()
        fun onUpdateDates(datesFounded: Boolean)
        fun onDetailsFailed(message: Int)
        fun onFavoriteFinished(isSuccessful: Boolean)
        fun onFieldDeleted(isSuccessful: Boolean, field: Field?)
        fun onFieldCreated(isSuccessful: Boolean, field: Field?)
        fun onFieldEdited(isSuccessful: Boolean, field: Field?)
        fun onFieldAvailableFailed()
    }
}