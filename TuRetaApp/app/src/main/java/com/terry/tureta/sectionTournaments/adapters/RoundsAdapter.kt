package com.terry.tureta.sectionTournaments.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.terry.tureta.R
import com.terry.tureta.databinding.ItemTextBinding

class RoundsAdapter(
    private val context: Context,
    private val size: Int,
    private val callback: IRoundsAdapter
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VHItem(
            ItemTextBinding.inflate(
                LayoutInflater.from(context), parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is VHItem) holder.bindData(position)
    }

    inner class VHItem(private val binding: ItemTextBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.text.setLightMode()
            binding.text.setEndIcon(ContextCompat.getDrawable(context, R.drawable.ic_arrow_right))
        }

        fun bindData(position: Int) {
            binding.text.updateText(context.getString(R.string.label_round_number, position + 1))
            binding.text.setEndIconTint(R.color.white)
            binding.text.setOnClickListener { callback.onRoundClicked(position + 1) }
        }

    }

    interface IRoundsAdapter {
        fun onRoundClicked(round: Int)
    }

}