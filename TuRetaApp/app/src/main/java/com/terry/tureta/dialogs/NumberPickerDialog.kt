package com.terry.tureta.dialogs

import android.graphics.Paint
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.PagerSnapHelper
import com.terry.tureta.*
import com.terry.tureta.adapters.SimpleTextAdapter
import com.terry.tureta.customViews.CustomDialog
import com.terry.tureta.customViews.CustomLayoutManager
import com.terry.tureta.databinding.DialogNumberPickerBinding
import com.terry.tureta.sectionTournaments.viewModel.TournamentViewModel
import grid.apps.supplies_admin.Extensions.mediumRegular
import grid.apps.supplies_admin.Extensions.setMargins

class NumberPickerDialog : CustomDialog(), CustomLayoutManager.OnItemSelectedListener{

    companion object {
        const val MODE_WON = 100
        const val MODE_TIE = 200
        const val MODE_LOSE = 300
        const val MODE_ROUNDS = 400
    }

    private var selectedPosition: Int = 0

    private lateinit var binding: DialogNumberPickerBinding
    private val args: NumberPickerDialogArgs by navArgs()

    private lateinit var adapter: SimpleTextAdapter

    private val mViewModel by lazy {
        ViewModelProvider(requireActivity())[TournamentViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogNumberPickerBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.cardView.radius = smallMargin.toFloat()

        binding.recyclerView.setMargins(0, mediumMargin)
        PagerSnapHelper().attachToRecyclerView(binding.recyclerView)

        binding.buttonCancel.setMargins(mediumMargin, mediumMargin, mediumMargin)
        binding.buttonCancel.setOnClickListener(this)

        binding.buttonContinue.setMargins(mediumMargin, mediumMargin, mediumMargin, mediumMargin)
        binding.buttonContinue.setOnClickListener(this)

        binding.textTitle.mediumRegular(R.color.black)
        binding.textTitle.setMargins(mediumMargin, mediumMargin, mediumMargin)

        binding.textGuide.mediumRegular()

        binding.separator1.setMargins(mediumMargin, 0, mediumMargin)
        binding.separator2.setMargins(mediumMargin, 0, mediumMargin)
    }

    override fun onResume() {
        super.onResume()

        when (args.mode) {
            MODE_WON -> {
                binding.textTitle.setText(R.string.label_match_won_points)
                adapter = SimpleTextAdapter(requireContext(), 6, this)
            }
            MODE_TIE -> {
                binding.textTitle.setText(R.string.label_match_tie_points)
                adapter = SimpleTextAdapter(requireContext(), 6, this)
            }
            MODE_LOSE -> {
                binding.textTitle.setText(R.string.label_match_lose_points)
                adapter = SimpleTextAdapter(requireContext(), 6, this)
            }
            MODE_ROUNDS -> {
                binding.textTitle.setText(R.string.label_tournament_rounds)
                adapter = SimpleTextAdapter(requireContext(), 101, this)
            }
        }

        binding.recyclerView.adapter = adapter
        binding.recyclerView.layoutManager =
            CustomLayoutManager(context, binding.recyclerView, this)
        adapter.notifyDataSetChanged()

        binding.recyclerView.post {
            calculateRecyclerPadding()
            scrollToSelectedPosition()
        }
    }

    private fun calculateRecyclerPadding() {
        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.typeface = poppinsRegular
        paint.textSize = spToPx(mediumFont)
        var textSize = paint.fontMetrics.descent - paint.fontMetrics.ascent
        textSize *= 0.6f
        val padding = binding.recyclerView.height / 2 - textSize.toInt()

        binding.recyclerView.setPadding(0, padding, 0, padding)
    }

    private fun scrollToSelectedPosition() {
        when (args.mode) {
            MODE_WON -> {
                binding.recyclerView.smoothScrollToPosition(mViewModel.mMatchWonPoints)
            }
            MODE_LOSE -> {
                binding.recyclerView.smoothScrollToPosition(mViewModel.mMatchLosePoints)
            }
            MODE_TIE -> {
                binding.recyclerView.smoothScrollToPosition(mViewModel.mMatchTiePoints)
            }
            MODE_ROUNDS -> {
                binding.recyclerView.smoothScrollToPosition(mViewModel.mNumberOfRounds)
            }
        }
    }

    private fun spToPx(sp: Float): Float {
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_SP,
            sp,
            requireContext().resources.displayMetrics
        )
    }

    override fun onClick(v: View?) {
        v ?: return
        when (v) {
            binding.buttonContinue -> {
                when (args.mode) {
                    MODE_WON -> {
                        mViewModel.mMatchWonPoints = selectedPosition
                    }
                    MODE_LOSE -> {
                        mViewModel.mMatchLosePoints = selectedPosition
                    }
                    MODE_TIE -> {
                        mViewModel.mMatchTiePoints = selectedPosition
                    }
                    MODE_ROUNDS -> {
                        mViewModel.mNumberOfRounds = selectedPosition
                    }
                }
                findNavController().navigate(R.id.action_numberPickerDialog_to_tournamentRulesFragment)
            }
            binding.buttonCancel -> {
                findNavController().navigate(R.id.action_numberPickerDialog_to_tournamentRulesFragment)
            }
            else -> {
                val position = binding.recyclerView.getChildAdapterPosition(v)
                binding.recyclerView.smoothScrollToPosition(position)
            }
        }
    }

    override fun onItemSelected(layoutPosition: Int) {
        selectedPosition = layoutPosition
    }

}