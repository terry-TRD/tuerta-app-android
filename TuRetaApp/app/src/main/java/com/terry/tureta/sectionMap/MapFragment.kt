package com.terry.tureta.sectionMap

import android.annotation.SuppressLint
import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.terry.tureta.*
import com.terry.tureta.customViews.CustomFragment
import com.terry.tureta.databinding.FragmentMapBinding
import com.terry.tureta.adapters.EstablishmentsAdapter
import com.terry.tureta.extensions.bitmapFromVector
import com.terry.tureta.model.Establishment
import com.terry.tureta.sectionEstablishments.viewModel.EstablishmentViewModel
import com.terry.tureta.sectionMain.viewModel.MainViewModel
import grid.apps.supplies_admin.Extensions.setMargins
import java.lang.Exception

class MapFragment : CustomFragment(), OnMapReadyCallback,
    EstablishmentsAdapter.IEstablishmentsAdapter {

    private lateinit var mMap: GoogleMap
    private lateinit var mMapFragment: SupportMapFragment

    private lateinit var binding: FragmentMapBinding

    private val mSearchList = ArrayList<Establishment>()
    private val searchAdapter by lazy {
        EstablishmentsAdapter(
            requireContext(),
            mSearchList,
            this,
            EstablishmentsAdapter.STYLE_SEARCH
        )
    }

    private val mViewModel by lazy {
        activity?.run {
            val model = ViewModelProvider(this)[MainViewModel::class.java]
            model.setActivity(this)
            model
        }
    }

    private val mEstablishmentViewModel by lazy {
        activity?.run {
            val model = ViewModelProvider(this)[EstablishmentViewModel::class.java]
            model.setActivity(this)
            model
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentMapBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mMapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mMapFragment.getMapAsync(this)

        binding.cardSearch.radius = smallMargin.toFloat()
        binding.cardSearch.elevation = elevationHeight
        binding.cardSearch.setMargins(mediumMargin, 0, mediumMargin)

        binding.textSearch.typeface = poppinsRegular
        binding.textSearch.addTextChangedListener(this)

        binding.cardSearchList.radius = smallMargin.toFloat()
        binding.cardSearchList.elevation = elevationHeight
        binding.cardSearchList.setMargins(mediumMargin, mediumMargin, mediumMargin)

        binding.recyclerSearch.layoutManager = LinearLayoutManager(context)
        binding.recyclerSearch.adapter = searchAdapter
    }

    override fun onResume() {
        super.onResume()

        mViewModel?.addObserver(this)
    }

    override fun onPause() {
        super.onPause()

        mViewModel?.deleteObserver(this)
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        mViewModel?.onSearchSubmitted(s.toString().trim())
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        if (googleMap == null) return

        mMap = googleMap
        mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(requireContext(), R.raw.map_style))
        mMap.setPadding(smallMargin, mediumMargin, smallMargin, mediumMargin)

        if (mViewModel == null) return

        goToLocation(mViewModel!!.getLastLocation(), false)

        mMap.setOnMapClickListener {
            hideKeyboard()
        }

        mMap.setOnCameraMoveListener {
            //search new courts based on the camera view
            mViewModel?.onCameraMoved(
                mMap.cameraPosition.target,
                mMap.projection.visibleRegion.nearLeft
            )
        }

        mMap.setOnMarkerClickListener {
            val latLng = "${it.position.latitude}${it.position.longitude}"
            if (mViewModel!!.getVisibleEstablishments().containsKey(latLng)) {
//                loadCourt(courtsVisible[latLng]!!.first)
                openEstablishment(mViewModel!!.getVisibleEstablishments()[latLng]!!.first)
            }

            return@setOnMarkerClickListener true
        }

        for (item in mViewModel!!.getVisibleEstablishments().values) {
            printMarker(item.first)
        }

        val locationButton = (mMapFragment.requireView()
            .findViewById<View>(Integer.parseInt("1")).parent as ViewGroup)
            .findViewById<View>(Integer.parseInt("2"))

        val params = locationButton.layoutParams as RelativeLayout.LayoutParams
        params.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)

        mViewModel?.initializeLocation()
    }

    private fun openEstablishment(establishment: Establishment) {
        mViewModel?.setLastLocation(LatLng(establishment.lat, establishment.lng))
        mEstablishmentViewModel?.setEstablishment(establishment)
        findNavController().navigate(R.id.action_mapFragment_to_establishmentFragment)
    }

    private fun goToLocation(latLng: LatLng, animate: Boolean = true) {
        try {
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng))
            val time = if (animate) 2000 else 1
            mMap.animateCamera(CameraUpdateFactory.zoomTo(15f), time, null)
        } catch (e: Exception) {
        }
    }

    // Location permission is already granted by this point
    @SuppressLint("MissingPermission")
    override fun onLocationLoaded(location: Location) {
        val newLoc = LatLng(location.latitude, location.longitude)
        goToLocation(newLoc, false)
        mMap.isMyLocationEnabled = true
    }

    override fun onLocationEnabled(isEnabled: Boolean) {
        if (isEnabled) {
            mViewModel?.initializeLocation()
        } else {
            goToLocation(mViewModel!!.getLastLocation())
        }
    }

    override fun onCameraResult(list: List<Establishment>) {
        if (this.isVisible) {
            for (court in list) {
                val marker = printMarker(court)
                mViewModel?.addEstablishment(Pair(court, marker))
            }
        }
    }

    private fun printMarker(court: Establishment): Marker {
        return mMap.addMarker(
            MarkerOptions()
                .position(
                    LatLng(
                        court.lat,
                        court.lng
                    )
                )
                .title(court.name)
                .icon(bitmapFromVector(requireContext(), R.drawable.ic_location))
        )
    }

    override fun onEstablishmentSelected(establishment: Establishment) {
        openEstablishment(establishment)
    }

    override fun onSearchResult(list: List<Establishment>) {
        mSearchList.clear()
        if (list.size <= 3) mSearchList.addAll(list)
        else {
            for (count in 0 until 3) {
                mSearchList.add(list[count])
            }
        }

        searchAdapter.notifyDataSetChanged()
    }

    override fun onKeyboardVisibilityChanged(isVisible: Boolean) {
        if(!isVisible){
            onSearchResult(listOf())
            binding.textSearch.clearFocus()
        }
    }

}