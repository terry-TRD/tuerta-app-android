package com.terry.tureta.sectionFields.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.widget.SwitchCompat
import androidx.recyclerview.widget.RecyclerView
import com.terry.tureta.*
import com.terry.tureta.databinding.ItemFieldAdminBinding
import com.terry.tureta.model.Field
import grid.apps.supplies_admin.Extensions.mediumBold
import grid.apps.supplies_admin.Extensions.setMargins

class FieldsAdapter(
    private val context: Context,
    private val list: List<Field>,
    private val callback: IFieldsAdapter
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VHFieldAdmin(
            ItemFieldAdminBinding.inflate(LayoutInflater.from(context), parent, false)
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as VHFieldAdmin).bindData(list[position])
    }

    inner class VHFieldAdmin(
        private val binding: ItemFieldAdminBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.textName.setMargins(mediumMargin, mediumMargin, mediumMargin, mediumMargin)
            binding.textName.mediumBold(R.color.black)
            binding.switchEnabled.setMargins(0, 0, mediumMargin)
            binding.cardItem.radius = smallMargin.toFloat()
        }

        fun bindData(field: Field) {
            binding.textName.text = field.name
            binding.switchEnabled.isChecked = field.isActive
            binding.cardItem.setOnClickListener { callback.onFieldSelected(field) }
            binding.switchEnabled.setOnClickListener {
                binding.switchEnabled.isEnabled = false
                callback.onFieldEnabled(
                    field,
                    it.isEnabled,
                    it as SwitchCompat
                )
            }
        }

    }

    interface IFieldsAdapter {
        fun onFieldSelected(field: Field)
        fun onFieldEnabled(field: Field, isEnabled: Boolean, switch: SwitchCompat)
    }
}