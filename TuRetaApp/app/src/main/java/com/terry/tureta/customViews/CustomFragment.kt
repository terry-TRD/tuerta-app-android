package com.terry.tureta.customViews

import android.app.Activity
import android.location.Location
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.AnimationSet
import android.view.animation.TranslateAnimation
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.terry.tureta.model.*
import com.terry.tureta.poppinsRegular
import com.terry.tureta.sectionEstablishments.viewModel.EstablishmentViewModel
import com.terry.tureta.sectionLogin.viewModel.LoginViewModel
import com.terry.tureta.sectionMain.viewModel.MainViewModel
import com.terry.tureta.sectionReservations.viewModel.ReservationsViewModel
import com.terry.tureta.sectionTournaments.viewModel.TournamentViewModel
import java.util.*

open class CustomFragment : Fragment(), View.OnClickListener, TextWatcher,
    MainViewModel.IMainObserver, EstablishmentViewModel.IEstablishmentObserver,
    ReservationsViewModel.IReservationsObserver, LoginViewModel.ILoginObserver,
    TournamentViewModel.ITournamentsObserver {

    fun setEditText(layout: TextInputLayout, text: TextInputEditText) {
        layout.typeface = poppinsRegular
        text.typeface = poppinsRegular
    }

    fun showToast(text: Int, duration: Int = Toast.LENGTH_LONG) {
        Toast.makeText(context, text, duration).show()
    }

    fun showToast(text: String, duration: Int = Toast.LENGTH_LONG) {
        Toast.makeText(context, text, duration).show()
    }

    fun hideKeyboard() {
        if (activity == null) return

        val imm =
            requireActivity().getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var newView = requireActivity().currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (newView == null) {
            newView = View(context)
        }
        imm.hideSoftInputFromWindow(newView.windowToken, 0)
    }

    override fun onClick(v: View?) {}

    fun setStartAnimation(view: View) {
        val translate = TranslateAnimation(0f, 0f, -200f, 0f)
        translate.duration = 300

        val alpha = AlphaAnimation(0f, 1f)
        alpha.duration = 300

        val set = AnimationSet(true)
        set.addAnimation(alpha)
        set.addAnimation(translate)

        view.startAnimation(set)
        view.visibility = View.VISIBLE
    }

    //region text watchers
    override fun afterTextChanged(s: Editable?) {}

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
    //endregion text watchers

    // Main observer
    override fun onLogout(isSuccess: Boolean, message: Int?) {}

    override fun onLocationEnabled(isEnabled: Boolean) {}

    override fun onLocationLoaded(location: Location) {}

    override fun onSearchResult(list: List<Establishment>) {}

    override fun onCameraResult(list: List<Establishment>) {}

    override fun onFavoritesResult(list: List<Establishment>?) {}

    override fun onNameUpdated(isSuccess: Boolean) {}

    override fun onColorUpdated(isSuccess: Boolean) {}

    override fun onKeyboardVisibilityChanged(isVisible: Boolean) {}

    override fun onUpdateView() {}

    override fun onUpdateDates(datesFounded: Boolean) {}

    override fun onDetailsFailed(message: Int) {}

    override fun onFavoriteFinished(isSuccessful: Boolean) {}

    override fun onFieldDeleted(isSuccessful: Boolean, field: Field?) {}

    override fun onFieldCreated(isSuccessful: Boolean, field: Field?) {}

    override fun onFieldEdited(isSuccessful: Boolean, field: Field?) {}

    override fun onFieldAvailableFailed() {}

    override fun onReservationResult(isSuccessful: Boolean) {}

    override fun onReservationsSuccess(list: List<Reservation>) {}

    override fun onReservationsFailed(message: Int) {}

    override fun onReservationUpdated(isSuccessful: Boolean, message: Int, isOwner: Boolean) {}

    override fun onFilterReservations(text: String) {}

    override fun onUpdateFilterText(text: String) {}

    //region login callbacks
    override fun onLoggedIn(user: User) {}

    override fun goToWalkthrough() {}

    override fun onLoginFailed(message: Int?) {}

    override fun onRegisterResult(isSuccess: Boolean, message: Int?) {}

    override fun onRestorePasswordResult(isSuccess: Boolean, message: Int) {}

    override fun onRequestPermissionResult(requestCode: Int, isGranted: Boolean) {}
    //endregion login callbacks

    //region tournament callbacks
    override fun onTournamentsLoaded(list: List<Tournament>) {}

    override fun onTournamentsFailed(message: Int) {}

    override fun onTeamsLoaded(list: List<Team>) {}

    override fun onTeamsFailed(message: Int) {}

    override fun onTeamCreated(isSuccess: Boolean) {}

    override fun onTournamentFinished(isSuccess: Boolean, message: Int) {}

    override fun onMatchListSuccess(list: List<Match>) {}

    override fun onMatchListFailed(message: Int) {}

    override fun onDateUpdated(date: Calendar) {}

    override fun onMatchFinished(isSuccess: Boolean, message: Int) {}

    override fun onTournamentStatsSuccess(list: List<TournamentStats>) {}

    override fun onTournamentStatsFailed(message: Int) {}

    override fun onTeamDeleted(isSuccess: Boolean, message: Int) {}
    //endregion tournament callbacks
}