package com.terry.tureta.sectionMain.viewModel

import android.annotation.SuppressLint
import android.app.Activity
import android.location.Location
import android.os.Handler
import android.util.Log
import androidx.lifecycle.ViewModel
import com.facebook.AccessToken
import com.facebook.login.LoginManager
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationSettingsResponse
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.tasks.OnSuccessListener
import com.terry.tureta.api.ApiUtils
import com.terry.tureta.R
import com.terry.tureta.helpers.SharedPreferences
import com.terry.tureta.model.Establishment
import com.terry.tureta.model.SearchResult
import com.terry.tureta.model.User
import com.terry.tureta.sectionAdmin.NotificationsController
import com.terry.tureta.sectionMap.LocationProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.lang.Exception

class MainViewModel : ViewModel(), OnSuccessListener<LocationSettingsResponse> {

    private var threadAlive = true
    private val searchMapList = ArrayList<Pair<LatLng, LatLng>>()
    private val searchTextList = ArrayList<Pair<LatLng, String>>()
    private var favoritesList: ArrayList<Establishment>? = null

    private lateinit var searchMapThread: Thread
    private lateinit var searchTextThread: Thread

    private var searchDisposable: Disposable? = null
    private var cameraMovedDisposable: Disposable? = null
    private var logoutDisposable: Disposable? = null
    private var favoritesDisposable: Disposable? = null
    private lateinit var profileDisposable: Disposable

    private val mObservers: ArrayList<IMainObserver> = ArrayList()
    private val mVisibleEstablishments = HashMap<String, Pair<Establishment, Marker>>()

    private lateinit var mUser: User
    private lateinit var mActivity: Activity

    private var mMyLocation: LatLng = LatLng(20.677271, -103.346982)
    private var mLastLocation: LatLng = LatLng(20.677271, -103.346982)
    private val mLocationProvider by lazy {
        LocationProvider(mActivity)
    }

    // On location listener
    fun initializeLocation() {
        if (mLastLocation.latitude == mMyLocation.latitude
            && mLastLocation.longitude == mLastLocation.longitude
        ) mLocationProvider.initializeLocation(this)
    }

    // On location listener
    // Location permission is already granted by this point
    @SuppressLint("MissingPermission")
    override fun onSuccess(p0: LocationSettingsResponse?) {
        mLocationProvider.getClient()
            .requestLocationUpdates(
                mLocationProvider.getRequest(),
                object : LocationCallback() {
                    override fun onLocationResult(locationResult: LocationResult?) {

                        locationResult ?: return

                        for (location in locationResult.locations) {
                            for (observer in mObservers) {
                                observer.onLocationLoaded(location)
                                mLastLocation = LatLng(location.latitude, location.latitude)
                                mMyLocation = LatLng(location.latitude, location.latitude)
                            }
                        }

                        mLocationProvider.stopLocationUpdates(this)
                    }
                }, null
            )
    }

    fun onLocationEnabled(isEnabled: Boolean) {
        Handler().postDelayed({
            for (observer in mObservers) {
                observer.onLocationEnabled(isEnabled)
            }
        }, 500)
    }

    fun onCameraMoved(center: LatLng, border: LatLng) {
        searchMapList.add(Pair(center, border))

        if (!::searchMapThread.isInitialized) {
            searchMapThread = Thread(::runCameraSearch)
            searchMapThread.start()
        }
    }

    fun onSearchSubmitted(query: String) {
        if (query.isNotEmpty()) {
            searchTextList.add(Pair(mMyLocation, query))

            if (!::searchTextThread.isInitialized) {
                searchTextThread = Thread(::runTextSearch)
                searchTextThread.start()
            }
        } else {
            searchTextList.clear()
            for (observer in mObservers) {
                observer.onSearchResult(ArrayList())
            }
        }
    }

    private fun runTextSearch() {
        while (threadAlive) {
            if (searchTextList.size > 0) {
                try {
                    if (searchTextList.size == 1) {
                        searchDisposable = ApiUtils.apiUser
                            .getEstablishments(
                                searchTextList[0].first.latitude,
                                searchTextList[0].first.longitude,
                                search = searchTextList[0].second
                            )
                            .subscribeOn(Schedulers.io())
                            .unsubscribeOn(Schedulers.computation())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                { result ->
                                    val res = (result as SearchResult<ArrayList<Establishment>>)
                                    for (observer in mObservers) {
                                        observer.onSearchResult(res.data!!)
                                    }
                                },
                                { error ->
                                },
                                { Log.d("TAG", "completed") }
                            )
                    } else Thread.sleep(100)
                    searchTextList.removeAt(0)
                } catch (e: Exception) {

                }
            } else Thread.sleep(500)
        }
    }

    private fun runCameraSearch() {
        while (threadAlive) {
            if (searchMapList.size > 0) {
                try {
                    if (searchMapList.size == 1) {
                        cameraMovedDisposable = ApiUtils.apiUser
                            .getEstablishments(
                                searchMapList[0].first.latitude,
                                searchMapList[0].first.longitude,
                                searchMapList[0].second.latitude,
                                searchMapList[0].second.longitude
                            )
                            .subscribeOn(Schedulers.io())
                            .unsubscribeOn(Schedulers.computation())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                { result ->
                                    val res = (result as SearchResult<ArrayList<Establishment>>)
                                    for (observer in mObservers) {
                                        observer.onCameraResult(res.data!!)
                                    }
                                },
                                { error ->
                                },
                                { Log.d("TAG", "completed") }
                            )
                    } else Thread.sleep(50)
                    searchMapList.removeAt(0)
                } catch (e: Exception) {

                }
            } else Thread.sleep(500)
        }
    }


    fun getFavorites() {
        if (favoritesList == null) {
            favoritesList = ArrayList()
        }

        favoritesDisposable = ApiUtils.apiUser
            .getFavorites(mUser.id)
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    favoritesList!!.clear()
                    favoritesList!!.addAll(result.data!!)

                    for (observer in mObservers) {
                        observer.onFavoritesResult(result.data!!)
                    }
                },
                { error ->
                    for (observer in mObservers) {
                        observer.onFavoritesResult(null)
                    }
                },
                { Log.d("TAG", "completed") }
            )
    }

    fun logOut() {
        logoutDisposable = ApiUtils.apiService
            .logOut()
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    if (AccessToken.getCurrentAccessToken() != null) {
                        LoginManager.getInstance().logOut()
                        AccessToken.setCurrentAccessToken(null)
                    }
                    NotificationsController.unsubscribeFromNotifications(mUser)
                    SharedPreferences(mActivity).clearSharedPreference()

                    for (observer in mObservers) {
                        observer.onLogout(true)
                    }

                    logoutDisposable!!.dispose()
                },
                { error ->
                    for (observer in mObservers) {
                        observer.onLogout(false, R.string.message_logout_error)
                    }
                    logoutDisposable!!.dispose()
                },
                { Log.d("TAG", "completed") }
            )
    }

    fun updateProfile(name: String? = null, color: Int? = null) {

        val colorString =
            if (color != null) "#" + Integer.toHexString(color).substring(2)
            else null

        profileDisposable = ApiUtils.apiService
            .updateProfile(name, colorString)
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    if (name != null) {
                        mUser.name = name

                        for (observer in mObservers) {
                            observer.onNameUpdated(true)
                        }
                    }
                    if (colorString != null) {
                        mUser.color = colorString

                        for (observer in mObservers) {
                            observer.onColorUpdated(true)
                        }
                    }

                    profileDisposable.dispose()
                },
                { error ->
                    if (name != null) {
                        for (observer in mObservers) {
                            observer.onNameUpdated(false)
                        }
                    }
                    if (colorString != null) {
                        for (observer in mObservers) {
                            observer.onColorUpdated(false)
                        }
                    }
                    profileDisposable.dispose()
                },
                { Log.d("TAG", "completed") }
            )
    }

    fun updateFavorites(establishment: Establishment){
        mVisibleEstablishments.forEach {
            if(it.value.first.id == establishment.id){
                it.value.first.isFavorite = establishment.isFavorite
            }
        }
    }

    fun addEstablishment(pair: Pair<Establishment, Marker>) {
        mVisibleEstablishments["${pair.first.lat}${pair.first.lng}"] = pair
    }

    fun getVisibleEstablishments(): HashMap<String, Pair<Establishment, Marker>> =
        mVisibleEstablishments

    fun setActivity(activity: Activity) {
        this.mActivity = activity
    }

    fun setUser(user: User) {
        this.mUser = user
    }

    fun getUser(): User =
        this.mUser

    fun isOwner(): Boolean = mUser.isOwner()

    fun getLastLocation(): LatLng = mLastLocation

    fun setLastLocation(latLng: LatLng) {
        this.mLastLocation = latLng
    }

    fun keyboardVisibilityChanged(isVisible: Boolean){
        for(observer in mObservers){
            observer.onKeyboardVisibilityChanged(isVisible)
        }
    }

    fun addObserver(observer: IMainObserver) {
//        for (mObserver in mObservers) {
//            if (mObserver == observer) return
//        }
        if (!mObservers.contains(observer))
            mObservers.add(observer)
    }

    fun deleteObserver(observer: IMainObserver) {
        mObservers.remove(observer)
    }

    override fun onCleared() {
        super.onCleared()

        mObservers.clear()
        threadAlive = false
    }

    interface IMainObserver {
        fun onLogout(isSuccess: Boolean, message: Int? = null)
        fun onLocationEnabled(isEnabled: Boolean)
        fun onLocationLoaded(location: Location)
        fun onSearchResult(list: List<Establishment>)
        fun onCameraResult(list: List<Establishment>)
        fun onFavoritesResult(list: List<Establishment>?)
        fun onNameUpdated(isSuccess: Boolean)
        fun onColorUpdated(isSuccess: Boolean)
        fun onKeyboardVisibilityChanged(isVisible: Boolean)
    }
}