package com.terry.tureta.sectionEstablishments

import android.app.DatePickerDialog
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.terry.tureta.R

import com.terry.tureta.customViews.CustomFragment
import com.terry.tureta.databinding.FragmentEstablishmentInfoBinding
import com.terry.tureta.dialogs.CustomDatePicker
import com.terry.tureta.extensions.bitmapFromVector
import com.terry.tureta.mediumMargin
import com.terry.tureta.model.Establishment
import com.terry.tureta.model.Field
import com.terry.tureta.sectionEstablishments.viewModel.EstablishmentViewModel
import com.terry.tureta.sectionEstablishments.adapters.ServicesAdapter
import com.terry.tureta.smallMargin
import grid.apps.supplies_admin.Extensions.*
import kotlinx.android.synthetic.main.fragment_establishment_map.*

class EstablishmentInfoFragment : CustomFragment(), OnMapReadyCallback {

    private lateinit var binding: FragmentEstablishmentInfoBinding

    private val servicesList = ArrayList<Establishment.Services>()

    private lateinit var mMap: GoogleMap

    private val servicesAdapter by lazy {
        ServicesAdapter(
            requireContext(),
            servicesList
        )
    }

    private val mViewModel by lazy {
        activity?.run {
            val model = ViewModelProvider(this)[EstablishmentViewModel::class.java]
            model.setActivity(this)
            model
        }
    }

    private lateinit var establishment: Establishment

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentEstablishmentInfoBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.textDescription.smallRegular()
        binding.textDescription.setMargins(mediumMargin, mediumMargin, mediumMargin)

        binding.recyclerServices.setMargins(mediumMargin, smallMargin)
        binding.recyclerServices.layoutManager = GridLayoutManager(requireContext(), 2)
        binding.recyclerServices.adapter = servicesAdapter

        binding.separator1.setMargins(mediumMargin, mediumMargin)

        binding.cardLocation.setMargins(mediumMargin, smallMargin, mediumMargin)
        binding.cardLocation.radius = mediumMargin.toFloat()

        binding.labelServices.mediumRegular()
        binding.labelServices.setMargins(mediumMargin, mediumMargin)

        binding.separator1.setMargins(mediumMargin)

        binding.buttonPickDate.setOnClickListener {
            val action =
                EstablishmentFragmentDirections.actionEstablishmentFragmentToCustomDatePicker(
                    CustomDatePicker.MODE_SEARCH_AVAILABILITY
                )
            findNavController().navigate(action)
        }

        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        binding.cardLocation.setOnClickListener {
            findNavController().navigate(R.id.action_establishmentFragment_to_establishmentMapFragment)
        }
    }

    override fun onResume() {
        super.onResume()

        mViewModel?.addObserver(this)

        if (mViewModel == null) return

        mViewModel?.initializeDetails()

        this.establishment = mViewModel!!.getEstablishment()
        binding.textDescription.text = establishment.description
        binding.labelLocation.updateText(establishment.address)
        binding.labelPhone.updateText(establishment.phone)
        if (establishment.phone.isNullOrEmpty()) {
            binding.labelPhone.visibility = View.GONE
        }
        binding.labelWebSite.visibility = View.GONE
        binding.labelPrice.updateText(getString(R.string.label_price_from, establishment.price))
    }

    override fun onPause() {
        super.onPause()

        mViewModel?.deleteObserver(this)
    }

    override fun onUpdateView() {
        servicesList.clear()
        servicesList.addAll(establishment.services)
        servicesAdapter.notifyDataSetChanged()
        binding.labelSchedule.updateText(establishment.getSchedule())
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        if (googleMap == null) return

        mMap = googleMap
        mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(requireContext(), R.raw.map_style))

        mMap.uiSettings.setAllGesturesEnabled(false)

        if (mViewModel == null) return

        val newLoc = LatLng(
            mViewModel!!.getEstablishment().lat,
            mViewModel!!.getEstablishment().lng
        )

        mMap.addMarker(
            MarkerOptions()
                .position(newLoc)
                .title(mViewModel!!.getEstablishment().name)
                .icon(bitmapFromVector(requireContext(), R.drawable.ic_location))
        )

        mMap.moveCamera(CameraUpdateFactory.newLatLng(newLoc))
        mMap.animateCamera(CameraUpdateFactory.zoomTo(14f), 1, null)
    }

}
