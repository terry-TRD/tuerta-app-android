package com.terry.tureta.sectionProfile.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.terry.tureta.R
import com.terry.tureta.databinding.ItemProfileColorBinding
import com.terry.tureta.largeFont
import com.terry.tureta.smallMargin
import grid.apps.supplies_admin.Extensions.largeTitleStyle
import grid.apps.supplies_admin.Extensions.setMargins

class ProfileColorAdapter(
    private val context: Context,
    private val list: IntArray,
    private val name: String,
    private var selectedColor: Int
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VHItem(
            ItemProfileColorBinding.inflate(LayoutInflater.from(context), parent, false)
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as VHItem).bindData(list[position])
    }

    fun getColor(): Int = selectedColor

    inner class VHItem(private val binding: ItemProfileColorBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.imageShirt.setMargins(smallMargin, smallMargin, smallMargin, smallMargin)
            binding.textInitials.text = name
            binding.textInitials.largeTitleStyle(R.color.primaryDark, largeFont)
        }

        fun bindData(color: Int) {
            if(color == selectedColor){
                binding.cardStroke.setCardBackgroundColor(ContextCompat.getColor(context, R.color.accent))
            } else {
                binding.cardStroke.setCardBackgroundColor(ContextCompat.getColor(context, R.color.grayText))
            }
            binding.imageShirt.setColorFilter(color)

            binding.cardItem.setOnClickListener {
                selectedColor = color
                notifyDataSetChanged()
            }
        }

    }

}