package com.terry.tureta.model

import android.content.Context
import com.google.gson.annotations.SerializedName
import com.terry.tureta.R
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class Match {

    var id: Int = 0
    var date: String = ""

    @SerializedName("round_number")
    var roundNumber: Int = 0

    @SerializedName("team_1_annotations")
    var team1Annotations: Int = 0

    @SerializedName("team_2_annotations")
    var team2Annotations: Int = 0
    var teams: ArrayList<Team> = ArrayList()
    var field: Field? = null

    var showSeparator = true

    fun getDate(context: Context): String {
        val date = date.split(' ')
        val parts = date[0].split('-')
        val monthIndex = parts[1].toInt()
        val month = context.resources.getStringArray(R.array.date_months)[monthIndex]

        val calendar = Calendar.getInstance()

        val dateFix = "${parts[0]}-${monthIndex + 1}-${parts[2]} ${date[1]}"

        calendar.time = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateFix)

        val dayNumber = calendar.get(Calendar.DAY_OF_WEEK)
        val monthNumber = calendar.get(Calendar.MONTH)
        val year = calendar.get(Calendar.YEAR)

        val day =
            context.resources.getStringArray(R.array.day_of_the_week)[calendar.get(Calendar.DAY_OF_WEEK) - 1]

        return "$day ${parts[2]} de $month"
    }
}