package com.terry.tureta.sectionFields

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.terry.tureta.R
import com.terry.tureta.adapters.SelectTextAdapter
import com.terry.tureta.customViews.CustomFragment
import com.terry.tureta.databinding.FragmentTypesListBinding
import com.terry.tureta.mediumMargin
import com.terry.tureta.sectionEstablishments.viewModel.EstablishmentViewModel
import com.terry.tureta.sectionTournaments.viewModel.TournamentViewModel
import grid.apps.supplies_admin.Extensions.mediumBold
import grid.apps.supplies_admin.Extensions.setMargins

class TypesListFragment : CustomFragment(), SelectTextAdapter.ITextAdapter {

    companion object {
        const val MODE_GRASS_FIELD = 100
        const val MODE_PLAYERS_FIELD = 200
        const val MODE_PLAYERS_TOURNAMENT = 300
    }

    private lateinit var binding: FragmentTypesListBinding

    private val args: TypesListFragmentArgs by navArgs()

    private val mFieldViewModel by lazy {
        ViewModelProvider(requireActivity())[EstablishmentViewModel::class.java]
    }

    private val mTournamentViewModel by lazy {
        ViewModelProvider(requireActivity())[TournamentViewModel::class.java]
    }

    private val adapter by lazy {
        val list = if (args.mode == MODE_GRASS_FIELD)
            resources.getStringArray(R.array.grass_types).toList()
        else resources.getStringArray(R.array.game_types).toList()

        val positionSelected = when (args.mode) {
            MODE_GRASS_FIELD -> mFieldViewModel.grassTypeSelected
            MODE_PLAYERS_FIELD -> mFieldViewModel.gameTypeSelected
            else -> mTournamentViewModel.getGameType()
        }

        SelectTextAdapter(requireContext(), list, this, positionSelected, true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTypesListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recyclerSelection.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerSelection.setMargins(0, mediumMargin, 0, mediumMargin)
        binding.recyclerSelection.adapter = adapter

        binding.buttonConfirmSelection.setOnClickListener(this)
        binding.buttonConfirmSelection.setMargins(mediumMargin, 0, mediumMargin, mediumMargin)
    }

    override fun onClick(v: View?) {
        v ?: return

        when (args.mode) {
            MODE_GRASS_FIELD -> {
                if (mFieldViewModel.grassTypeSelected == -1) {
                    showToast(R.string.message_grass_type_empty)
                } else {
                    findNavController().navigate(R.id.action_typesListFragment_to_fieldFragment)
                }
            }
            MODE_PLAYERS_FIELD -> {
                if (mFieldViewModel.gameTypeSelected == -1) {
                    showToast(R.string.message_game_type_empty)
                } else {
                    findNavController().navigate(R.id.action_typesListFragment_to_fieldFragment)
                }
            }
            MODE_PLAYERS_TOURNAMENT -> {
                if (mTournamentViewModel.getGameType() == -1) {
                    showToast(R.string.message_game_type_empty)
                } else {
//                    findNavController().navigate(R.id.action_typesListFragment_to_editTournamentFragment)
                    activity?.onBackPressed()
                }
            }
        }
    }

    override fun onTextClick(text: String, position: Int) {
        when (args.mode) {
            MODE_GRASS_FIELD -> {
                mFieldViewModel.grassTypeSelected = position
            }
            MODE_PLAYERS_FIELD -> {
                mFieldViewModel.gameTypeSelected = position
            }
            MODE_PLAYERS_TOURNAMENT -> {
                mTournamentViewModel.setGameType(position)
            }
        }
    }

}