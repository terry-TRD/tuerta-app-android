package com.terry.tureta.sectionMap

import android.Manifest
import android.app.Activity
import android.content.IntentSender
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnSuccessListener

class LocationProvider(private val activity: Activity) {

    companion object {
        const val CODE_LOCATION_PERMISSION = 100
        const val CODE_ENABLE_LOCATION = 200
    }

    private var mLocationRequest = LocationRequest.create().apply {
        interval = 10000
        fastestInterval = 5000
        priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }
    private val mLocationClient by lazy { LocationServices.getFusedLocationProviderClient(activity) }

    fun initializeLocation(callback: OnSuccessListener<LocationSettingsResponse>) {
        if (ActivityCompat.checkSelfPermission(
                activity,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                activity,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                CODE_LOCATION_PERMISSION
            )
        } else {

            val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest)

            LocationServices.getSettingsClient(activity)
                .checkLocationSettings(builder.build())
                .addOnSuccessListener(callback)
                .addOnFailureListener {
                    if (it is ResolvableApiException) {
                        // Location settings are not satisfied, but this can be fixed
                        // by showing the user a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            it.startResolutionForResult(activity, CODE_ENABLE_LOCATION)
                        } catch (sendEx: IntentSender.SendIntentException) {
                            // Ignore the error.
                        }
                    }
                }
        }
    }

    fun getClient(): FusedLocationProviderClient = mLocationClient

    fun getRequest(): LocationRequest = mLocationRequest

    fun stopLocationUpdates(callback: LocationCallback) {
        mLocationClient.removeLocationUpdates(callback)
    }

}