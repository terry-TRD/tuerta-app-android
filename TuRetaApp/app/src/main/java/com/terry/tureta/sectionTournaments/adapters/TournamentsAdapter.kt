package com.terry.tureta.sectionTournaments.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.terry.tureta.R
import com.terry.tureta.databinding.ItemCardButtonBinding
import com.terry.tureta.databinding.ItemTextViewBinding
import com.terry.tureta.mediumMargin
import com.terry.tureta.model.Tournament
import grid.apps.supplies_admin.Extensions.largeBold
import grid.apps.supplies_admin.Extensions.setMargins

class TournamentsAdapter(
    private val context: Context,
    private val list: List<Tournament>,
    private val callback: ITournamentAdapter
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var isOwner = false

    companion object {
        private const val TYPE_HEADER = 100
        private const val TYPE_ITEM = 200
    }

    override fun getItemViewType(position: Int): Int {
        return if (!isOwner && position == 0) TYPE_HEADER else TYPE_ITEM
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == TYPE_HEADER)
            VHHeader(
                ItemTextViewBinding.inflate(
                    LayoutInflater.from(context), parent, false
                )
            )
        else VHTournament(
            ItemCardButtonBinding.inflate(
                LayoutInflater.from(context), parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return if (isOwner) list.size else if (list.isNotEmpty()) list.size + 1 else 0
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is VHTournament) {
            if (isOwner) holder.bindData(list[position])
            else holder.bindData(list[position - 1])
        }
    }

    fun isOwner(isOwner: Boolean) {
        this.isOwner = isOwner
        notifyDataSetChanged()
    }

    inner class VHHeader(binding: ItemTextViewBinding) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.textView.largeBold()
            binding.textView.setText(R.string.label_pick_a_tournament)
            binding.textView.setMargins(mediumMargin)
        }

    }

    inner class VHTournament(private val binding: ItemCardButtonBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindData(tournament: Tournament) {
            binding.button.updateText(tournament.name)
            binding.button.setOnClickListener { callback.onTournamentSelected(tournament) }
            binding.button.setEndIconTint(R.color.white)
        }

    }

    interface ITournamentAdapter {
        fun onTournamentSelected(tournament: Tournament)
    }

}