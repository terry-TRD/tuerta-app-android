package com.terry.tureta.helpers

import com.terry.tureta.model.Establishment

object UtilsSchedule {

    fun getOpenHours(schedule: Establishment.Schedules): String =
        schedule.from.split(" ")[1]

    fun getCloseHours(schedule: Establishment.Schedules): String = schedule.to.split(" ")[1]

    fun getDate(schedule: String): String = schedule.split(" ")[0]

}