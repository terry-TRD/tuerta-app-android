package com.terry.tureta

import android.content.ContentValues.TAG
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class NotificationService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {

        if(remoteMessage.data.isNotEmpty()){
            Log.d(TAG, "New message: " + remoteMessage.data)
        }

    }

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)

        Log.d(TAG, "New token: $p0")
    }

}
