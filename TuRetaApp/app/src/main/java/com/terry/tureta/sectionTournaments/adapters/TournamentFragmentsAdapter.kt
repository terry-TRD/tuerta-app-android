package com.terry.tureta.sectionTournaments.adapters

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.terry.tureta.R
import com.terry.tureta.sectionTournaments.TournamentRoundsFragment
import com.terry.tureta.sectionTournaments.TournamentStatsFragment

class TournamentFragmentsAdapter(private val context: Context, manager: FragmentManager) :
    FragmentStatePagerAdapter(
        manager,
        BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
    ) {

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> context.getString(R.string.label_calendar)
            else -> context.getString(R.string.label_statistics)
        }
    }

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> TournamentRoundsFragment()
            else -> TournamentStatsFragment()
        }
    }

    override fun getCount(): Int {
        return 2
    }

}