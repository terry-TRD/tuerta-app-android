package com.terry.tureta.sectionTournaments.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.terry.tureta.sectionMatches.MatchesListFragment

class RoundFragmentsAdapter(
    private val roundNumber: Int,
    manager: FragmentManager,
    private val callback: IRoundFragmentsAdapter
) :
    FragmentStatePagerAdapter(
        manager,
        BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
    ) {

    override fun getPageTitle(position: Int): CharSequence? {
        return "J" + (position + 1)
    }

    override fun getItem(position: Int): Fragment {
        callback.onRoundSelected(position + 1)
        return MatchesListFragment()
    }

    override fun getCount(): Int {
        return roundNumber
    }

    interface IRoundFragmentsAdapter {
        fun onRoundSelected(position: Int)
    }

}