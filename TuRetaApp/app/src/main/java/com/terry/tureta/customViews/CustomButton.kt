package com.terry.tureta.customViews

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.content.res.Resources
import android.graphics.*
import android.graphics.drawable.*
import android.util.AttributeSet
import android.util.TypedValue
import android.view.MotionEvent
import android.view.View
import android.view.ViewTreeObserver
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.graphics.drawable.toBitmap
import com.terry.tureta.*
import com.terry.tureta.extensions.resize
import kotlin.collections.ArrayList

class CustomButton @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    companion object {
        const val NORMAL = 0
        const val TEXT_BUTTON = 1
        const val TEXT_BUTTON_SMALL = 2
        const val MULTILINE = 3
        const val ICON = 4
        const val CHIP = 5
        const val NORMAL_SMALL = 6

        const val BOLD = 0
        const val LIGHT = 1
    }

    private var mRect = RectF()

    private val mPaintBackground = Paint(Paint.ANTI_ALIAS_FLAG)
    private val mPaintText = Paint(Paint.ANTI_ALIAS_FLAG)

    private var mStyle = 0
    private var mTextStyle = 0
    private var mTextColor = -1
    private var mIconTint = -1

    private var mIsSelectable = false

    private var mText = ""
    private var mTextParts: ArrayList<String> = ArrayList()

    private var mBackground: LinearGradient? = null
    private var mIconDrawable: Drawable? = null
    private var mIcon: Bitmap? = null
    private var mMaskPath: Path? = null

    private var mButtonHeight: Double = 0.0
    private var mButtonWidth: Double = 0.0

    private var mIsSelected: Boolean = false

    init {
        if (attrs != null) {
            val ta = context.obtainStyledAttributes(
                attrs,
                R.styleable.CustomButton
            )

            mStyle = ta.getInt(R.styleable.CustomButton_style, 0)
            mTextStyle = ta.getInt(R.styleable.CustomButton_text_style, 0)
            mTextColor = ta.getColor(
                R.styleable.CustomButton_text_color,
                ContextCompat.getColor(context, R.color.white)
            )

            mIconTint = ta.getColor(
                R.styleable.CustomButton_icon_tint,
                ContextCompat.getColor(context, R.color.white)
            )

            mIsSelectable = ta.getBoolean(R.styleable.CustomButton_is_selectable, false)

            if (mStyle == NORMAL || mStyle == ICON || mStyle == CHIP || mStyle == NORMAL_SMALL)
                mMaskPath = Path()

            val textInt = ta.getResourceId(R.styleable.CustomButton_text, -1)

            if (textInt != -1) {
                mText = context.getString(textInt)
            } else if (ta.getString(R.styleable.CustomButton_text) != null) {
                mText = ta.getString(R.styleable.CustomButton_text)!!
            }

            mIconDrawable = ta.getDrawable(R.styleable.CustomButton_icon)
            setIcon()

            mPaintText.color = mTextColor
            if (mStyle == TEXT_BUTTON_SMALL || mStyle == NORMAL_SMALL) {
                mPaintText.textSize = spToPx(smallFont)
                mPaintText.typeface = poppinsRegular
            } else {
                mPaintText.textSize = spToPx(mediumFont)
                mPaintText.typeface = poppinsSemiBold
            }

            setButtonMeasures()

            val startColor = ta.getColor(
                R.styleable.CustomButton_button_color,
                ContextCompat.getColor(context, R.color.accent)
            )

            mBackground = LinearGradient(
                0f,
                0f,
                mButtonWidth.toFloat(),
                0f,
                startColor,
                startColor,
                Shader.TileMode.CLAMP
            )

            ta.recycle()

            viewTreeObserver.addOnGlobalLayoutListener(object :
                ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    viewTreeObserver.removeOnGlobalLayoutListener(this)

                    if (mIcon != null && height > 0)
                        mIcon = mIcon!!.resize(
                            height.toFloat() - (dpToPx(smallMargin.toFloat()) * 2),
                            height.toFloat() - (dpToPx(smallMargin.toFloat()) * 2)
                        )
                }

            })
        }
    }

    private fun setIcon() {
        if (mIconDrawable != null) {
            if (!mIsSelectable) {
                DrawableCompat.setTint(
                    DrawableCompat.wrap(mIconDrawable!!),
                    mIconTint
                )
            } else {
                DrawableCompat.setTint(
                    DrawableCompat.wrap(mIconDrawable!!),
                    mTextColor
                )
            }
            mIcon = mIconDrawable!!.toBitmap(
                mIconDrawable!!.intrinsicWidth * 2,
                mIconDrawable!!.intrinsicWidth * 2
            )
            if (height > 0)
                mIcon = mIcon!!.resize(
                    height.toFloat() - (dpToPx(smallMargin.toFloat()) * 2),
                    height.toFloat() - (dpToPx(smallMargin.toFloat()) * 2)
                )
        }
    }

    fun updateText(text: String) {
        mText = text
        setButtonMeasures()
        postInvalidate()
    }

    fun updateText(text: Int) {
        mText = context.getString(text)
        setButtonMeasures()
        postInvalidate()
    }

    fun updateSelected(selected: Boolean) {
        mIsSelected = selected
        postInvalidate()
    }

    private fun setButtonMeasures() {
        var textRect = Rect()

        val buttonHeight = mPaintText.fontMetrics.descent - mPaintText.fontMetrics.ascent

        if (mStyle == NORMAL) {

            mButtonWidth = if (mIcon != null)
                (mPaintText.measureText(mText)
                        + dpToPx(mediumMargin.toFloat()) * 2
                        + dpToPx(smallMargin.toFloat())
                        + buttonHeight).toDouble()
            else (mPaintText.measureText(mText) + dpToPx(mediumMargin.toFloat()) * 2).toDouble()
            mButtonHeight = (buttonHeight + dpToPx(smallMargin.toFloat()) * 2).toDouble()

        } else if (mStyle == TEXT_BUTTON || mStyle == TEXT_BUTTON_SMALL) {

            mButtonWidth = if (mIcon != null)
                (mPaintText.measureText(mText)
                        + dpToPx(smallMargin.toFloat()) * 2
                        + buttonHeight).toDouble()
            else mPaintText.measureText(mText).toDouble()
            mButtonHeight = (buttonHeight + dpToPx(smallMargin.toFloat()) * 3).toDouble()

        } else if ((mStyle == ICON) && mIcon != null) {

            if (mStyle == ICON) mText = "A"
            mButtonHeight = (buttonHeight + dpToPx(smallMargin.toFloat()) * 3).toDouble()
            mButtonWidth = mButtonHeight

        } else if (mStyle == CHIP || mStyle == NORMAL_SMALL) {
            if (mStyle == CHIP) mPaintText.textSize = spToPx(mediumFont)

            mPaintText.getTextBounds(mText, 0, mText.length, textRect)
            mButtonWidth = textRect.width() + dpToPx(mediumMargin.toFloat()) * 1.5

            mButtonHeight = (dpToPx(smallMargin.toFloat()) * 3).toDouble()

        } else {

            mTextParts.clear()
            mTextParts.addAll(mText.split(" "))
            for (part in mTextParts) {
                val rect = Rect()
                mPaintText.getTextBounds(part, 0, part.length, rect)
                if (rect.width() > textRect.width()) {
                    textRect = rect
                    mButtonWidth =
                        (mPaintText.measureText(part) + dpToPx(smallMargin.toFloat()) * 4).toDouble()
                }
            }
            mButtonHeight =
                ((buttonHeight * mTextParts.size) + dpToPx(smallMargin.toFloat()) * 3).toDouble()

        }
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (event == null) return super.onTouchEvent(event)

        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                val scaleDownX = ObjectAnimator.ofFloat(
                    this,
                    "scaleX",
                    0.9f
                )
                val scaleDownY = ObjectAnimator.ofFloat(
                    this,
                    "scaleY",
                    0.9f
                )
                scaleDownX.duration = 100
                scaleDownY.duration = 100

                val animator = AnimatorSet()
                animator.play(scaleDownX).with(scaleDownY)

                animator.start()
            }
            MotionEvent.ACTION_UP -> scaleUpAnimation()
            MotionEvent.ACTION_CANCEL -> scaleUpAnimation()
        }
        return super.onTouchEvent(event)
    }

    private fun scaleUpAnimation() {
        val scaleDownX = ObjectAnimator.ofFloat(
            this,
            "scaleX",
            1f
        )
        val scaleDownY = ObjectAnimator.ofFloat(
            this,
            "scaleY",
            1f
        )
        scaleDownX.duration = 500
        scaleDownY.duration = 500

        val animator = AnimatorSet()
        animator.play(scaleDownX).with(scaleDownY)

        animator.start()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        setMeasuredDimension(
            measureDimension(mButtonWidth.toInt() + 1, widthMeasureSpec),
            measureDimension(mButtonHeight.toInt() + 1, heightMeasureSpec)
        )
    }

    private fun measureDimension(desiredSize: Int, measureSpec: Int): Int {
        var result: Int
        val specMode = MeasureSpec.getMode(measureSpec)
        val specSize = MeasureSpec.getSize(measureSpec)
        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize
        } else {
            result = desiredSize
            if (specMode == MeasureSpec.AT_MOST) {
                result = result.coerceAtMost(specSize)
            }
        }
        return result
    }

    private fun dpToPx(dp: Float): Float {
        val density = Resources.getSystem().displayMetrics.density
        return (dp * density)
    }

    private fun spToPx(sp: Float): Float {
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_SP,
            sp,
            context.resources.displayMetrics
        )
    }

    override fun onDraw(canvas: Canvas?) {
        if (canvas == null) return

        val width = width.toFloat()
        val height = height.toFloat()

        val textWidth = mPaintText.measureText(mText)
        val fm = mPaintText.fontMetrics
        val textHeight = fm.descent - fm.ascent

        mRect.set(0f, 0f, width, height)

        when (mStyle) {
            NORMAL -> onDrawNormalStyle(canvas, height, textWidth, width)
            TEXT_BUTTON, TEXT_BUTTON_SMALL -> onDrawTextStyle(canvas, height, textHeight)
            MULTILINE -> onDrawMultilineStyle(canvas, textHeight)
            ICON -> onDrawIconStyle(canvas, height)
            CHIP, NORMAL_SMALL -> onDrawChipStyle(canvas, height, width, textWidth)
        }
    }

    private fun onDrawNormalStyle(
        canvas: Canvas,
        height: Float,
        textWidth: Float,
        width: Float
    ) {
        mPaintBackground.shader = mBackground

        canvas.drawRoundRect(
            mRect,
            smallMargin.toFloat(),
            smallMargin.toFloat(),
            mPaintBackground
        )

        if (mIcon != null) {
            val margin = (width - mIcon!!.height - textWidth - dpToPx(smallMargin.toFloat())) / 2
            canvas.drawBitmap(
                mIcon!!,
                dpToPx(smallMargin.toFloat()),
                dpToPx(smallMargin.toFloat()),
                null
            )

            canvas.drawText(
                mText,
                mIcon!!.width + dpToPx(smallMargin.toFloat()) + margin,
                height / 2 + mPaintText.fontMetrics.descent,
                mPaintText
            )
        } else {
            canvas.drawText(
                mText,
                width / 2 - textWidth / 2,
                height / 2 + mPaintText.fontMetrics.descent,
                mPaintText
            )
        }

        mMaskPath!!.addRoundRect(
            mRect,
            smallMargin.toFloat(),
            smallMargin.toFloat(),
            Path.Direction.CW
        )
        canvas.clipPath(mMaskPath!!)
    }

    private fun onDrawTextStyle(canvas: Canvas, height: Float, textHeight: Float) {
        mPaintBackground.color = ContextCompat.getColor(context, R.color.transparent)
//        mPaintBackground.color = ContextCompat.getColor(context, R.color.red)

        canvas.drawRect(mRect, mPaintBackground)

        var startX = 0f
        if (mIcon != null) startX += dpToPx(smallMargin.toFloat()) + textHeight

        canvas.drawText(
            mText,
            startX,
            height / 2 + mPaintText.fontMetrics.descent,
            mPaintText
        )

        if (mIcon != null) {
            canvas.drawBitmap(
                mIcon!!, 0f,
                dpToPx(smallMargin.toFloat()),
                null
            )
        }
    }

    private fun onDrawMultilineStyle(canvas: Canvas, textHeight: Float) {
        mPaintBackground.shader = mBackground

        canvas.drawRect(mRect, mPaintBackground)

        val margin = (height - (textHeight * mTextParts.size)) / 2
        for ((count, part) in mTextParts.withIndex()) {
            canvas.drawText(
                part,
                dpToPx(smallMargin.toFloat()) * 2,
                margin + (textHeight * 0.75).toFloat() + (textHeight * count),
                mPaintText
            )
        }
    }

    private fun onDrawIconStyle(canvas: Canvas, height: Float) {
        mPaintBackground.shader = mBackground

        canvas.drawRoundRect(
            mRect,
            height / 2,
            height / 2,
            mPaintBackground
        )

        if (mIcon != null) {
            canvas.drawBitmap(
                mIcon!!,
                dpToPx(smallMargin.toFloat()),
                dpToPx(smallMargin.toFloat()),
                null
            )
        }

        mMaskPath!!.addRoundRect(
            mRect,
            height / 2,
            height / 2,
            Path.Direction.CW
        )
        canvas.clipPath(mMaskPath!!)
    }

    private fun onDrawChipStyle(canvas: Canvas, height: Float, width: Float, textWidth: Float) {
        mPaintBackground.shader = mBackground
        if (mIsSelected) {
            mPaintText.color = ContextCompat.getColor(context, R.color.white)
            mPaintBackground.style = Paint.Style.FILL
            mRect.set(0f, 0f, width, height)

            canvas.drawRoundRect(
                mRect,
                height / 2,
                height / 2,
                mPaintBackground
            )
        } else {
            mPaintText.color = mTextColor
            mPaintBackground.style = Paint.Style.STROKE
            mPaintBackground.strokeWidth = 2f
            mRect.set(2f, 2f, width - 2f, height - 2f)

            canvas.drawRoundRect(
                mRect,
                (height - 2f) / 2,
                (height - 2f) / 2,
                mPaintBackground
            )
        }

        canvas.drawText(
            mText,
            width / 2 - textWidth / 2,
            height / 2 + mPaintText.fontMetrics.descent,
            mPaintText
        )

        mMaskPath!!.addRoundRect(
            mRect,
            height / 2,
            height / 2,
            Path.Direction.CW
        )
        canvas.clipPath(mMaskPath!!)
    }

}