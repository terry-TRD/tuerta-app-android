package grid.apps.supplies_admin.Extensions

import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.terry.tureta.R
import com.terry.tureta.largeFont
import com.terry.tureta.poppinsSemiBold


fun Toolbar.setToolbar(
    size: Float = largeFont,
    isCentered: Boolean = false,
    background: Int = R.color.accent
) {
    this.setTitleTextColor(ContextCompat.getColor(context, R.color.white))
    this.contentInsetStartWithNavigation = 0
    this.setBackgroundColor(ContextCompat.getColor(context, background))
    for (i in 0 until this.childCount) {
        val view = this.getChildAt(i)
        if (view is AppCompatTextView) {
            view.textSize = size
            view.typeface = poppinsSemiBold
            if (isCentered)
                view.textAlignment = TextView.TEXT_ALIGNMENT_CENTER
            else view.textAlignment = TextView.TEXT_ALIGNMENT_TEXT_START

            view.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
            break
        }
    }
}