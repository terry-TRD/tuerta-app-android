package com.terry.tureta.sectionReservations

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.terry.tureta.R
import com.terry.tureta.databinding.FragmentReservationResultBinding
import com.terry.tureta.dialogs.LoadDialogArgs
import com.terry.tureta.mediumMargin
import grid.apps.supplies_admin.Extensions.largeTitleStyle
import grid.apps.supplies_admin.Extensions.mediumRegular
import grid.apps.supplies_admin.Extensions.setMargins

class ReservationResultFragment : Fragment() {

    private lateinit var binding: FragmentReservationResultBinding
    private val args: ReservationResultFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentReservationResultBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.textTitle.largeTitleStyle()
        binding.button.setMargins(mediumMargin, 0, mediumMargin)
        binding.textMessage.mediumRegular()
        binding.textMessage.setMargins(mediumMargin, 0, mediumMargin)

        binding.button.setOnClickListener {
            if(args.isSuccessful){
                findNavController().navigate(R.id.action_reservationResultFragment_to_mapFragment)
            } else {
                findNavController().navigate(
                    ReservationResultFragmentDirections.actionReservationResultFragmentToCreateReservationFragment(args.hasDates)
                )
            }
        }
    }

    override fun onResume() {
        super.onResume()

        if(args.isSuccessful){
            binding.textTitle.text = getString(R.string.label_success)
            binding.textMessage.text = getString(R.string.label_reservation_successful_message)
            binding.button.updateText(R.string.label_accept)
            binding.imageView.setImageDrawable(
                ContextCompat.getDrawable(requireContext(), R.drawable.ic_successful)
            )
        } else {
            binding.textTitle.text = getString(R.string.label_error)
            binding.textMessage.text = getString(R.string.label_reservation_error_message)
            binding.button.updateText(R.string.label_retry)
            binding.imageView.setImageDrawable(
                ContextCompat.getDrawable(requireContext(), R.drawable.ic_whistle)
            )
        }
    }
}