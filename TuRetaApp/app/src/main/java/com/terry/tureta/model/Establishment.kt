package com.terry.tureta.model

import com.google.gson.annotations.SerializedName
import com.terry.tureta.R
import java.io.Serializable
import java.text.DecimalFormat

class Establishment : Serializable {

    var id: Int = 0
    var name: String = ""
    var lat: Double = 0.0
    var lng: Double = 0.0
    var description: String = ""
    var address: String = ""
    var phone: String? = null

    @SerializedName("profile_picture_url")
    var image: String = ""
    var distance: Int = 0

    @SerializedName("reservation_message")
    var reservationMessage: String? = null
    var schedules: ArrayList<Schedules> = ArrayList()
    var services: ArrayList<Services> = ArrayList()
    var fields: ArrayList<Field> = ArrayList()
    var rate: Int = 0
    var price: String = ""

    @SerializedName("is_favorite")
    var isFavorite: Boolean = false

    var detailsDownloaded = false

    inner class Schedules : Serializable {
        var from: String = ""
        var to: String = ""
        var gameTypes: String = ""

        @SerializedName("day_number")
        var dayNumber: Int = 0

        var selected: Boolean = false
        var isEnabled: Boolean = true
        var position: Int = 0

        fun getFromHour(): String = from.split(" ")[1] + " hrs"

        fun getBackColor(): Int {
            return when {
                selected && isEnabled -> R.color.accent
                isEnabled -> R.color.accentLight
                else -> R.color.grayText
            }
        }
    }

    inner class Services : Serializable {
        var id: Int = 0
        var name: String = ""

        @SerializedName("icon_url")
        var icon: String = ""

        var selected: Boolean = false
    }

    fun getFields(): List<Field> = fields

    fun getHourLabel(field: Int, type: Int): String {
        val price = fields[field].prices[type].perHour
        return "$$price MX"
    }

    fun getHourPrice(field: Int, type: Int): Float {
        return fields[field].prices[type].perHour.toFloat()
    }

    fun getPlayers(position: Int): List<String> {
        val list = ArrayList<String>()

        val field = fields[position]

        val parts = field.gameTypes.split(",")
        for (part in parts) {
            list.add(part)
        }

        return list
    }

    fun getSchedule(): String =
        schedules[0].from + " HRS - " + schedules[schedules.size - 1].to + " HRS"

}