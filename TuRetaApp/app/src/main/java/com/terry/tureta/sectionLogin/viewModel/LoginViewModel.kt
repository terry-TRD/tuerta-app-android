package com.terry.tureta.sectionLogin.viewModel

import android.app.Activity
import android.content.ContentValues.TAG
import android.content.Context
import android.os.Handler
import android.util.Log
import androidx.lifecycle.ViewModel
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.firebase.iid.FirebaseInstanceId
import com.terry.tureta.api.ApiUtils
import com.terry.tureta.R
import com.terry.tureta.helpers.SharedPreferences
import com.terry.tureta.model.User
import com.terry.tureta.model.UserResponse
import com.terry.tureta.sectionAdmin.NotificationsController
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException

class LoginViewModel : ViewModel(), FacebookCallback<LoginResult> {

    //    private var callback: ILoginActivity? = null
    private var context: Context? = null
    private lateinit var disposable: Disposable
    private val callbackManager by lazy { CallbackManager.Factory.create() }

    private val mObservers = ArrayList<ILoginObserver>()

    private var mEmail: String = ""
    private var mPassword: String = ""

    init {
        LoginManager.getInstance().registerCallback(callbackManager, this)
    }

    fun initializeUserSession(context: Context) {
        this.context = context
        val token = SharedPreferences(context).getString(R.string.key_token)
        if (token.isEmpty()) {

            for (observer in mObservers) {
                observer.goToWalkthrough()
            }
        } else getProfileWS(token)
    }

    private fun getProfileWS(token: String) {
        ApiUtils.token = token

        disposable = ApiUtils.apiService.getProfileWS()
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    for (observer in mObservers) {
                        observer.onLoggedIn(result.data!!)
                    }
                },
                { error ->
                    if (error is HttpException)
                        Log.e(TAG, error.message())

                    for (observer in mObservers) {
                        observer.goToWalkthrough()
                    }
                },
                { Log.d("TAG", "completed") }
            )
    }

    fun login(email: String, password: String) {

        FirebaseInstanceId.getInstance()
            .instanceId
            .addOnCompleteListener {
                if (!it.isSuccessful || it.result == null) {
                    return@addOnCompleteListener
                }

                disposable = ApiUtils.apiService.loginWS(email, password, it.result!!.token)
                    .subscribeOn(Schedulers.io())
                    .unsubscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                        { result ->
                            result.data!!.user.deviceId = it.result!!.token
                            getResult(result.data!!)
                        },
                        { error ->
                            if (error is HttpException) {
                                Log.e(TAG, error.message())
                            }
                            for (observer in mObservers) {
                                observer.onLoginFailed(R.string.message_login_error)
                            }
                        },
                        { Log.d("TAG", "completed") }
                    )
            }

    }

    fun loginWithFacebook(activity: Activity) {
        LoginManager.getInstance()
            .logInWithReadPermissions(activity, listOf("public_profile", "email"))
    }

    fun register(email: String, password: String, name: String, username: String) {
        FirebaseInstanceId.getInstance()
            .instanceId
            .addOnCompleteListener {
                if (!it.isSuccessful || it.result == null) {
                    return@addOnCompleteListener
                }

                disposable = ApiUtils.apiService.registerWS(
                    email,
                    password,
                    password,
                    it.result!!.token,
                    name,
                    username
                )
                    .subscribeOn(Schedulers.io())
                    .unsubscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                        { result ->
                            result.data!!.user.deviceId = it.result!!.token
                            getResult(result.data!!)
                        },
                        { error ->
                            if (error is HttpException) {
                                Log.e(TAG, error.message())
                            }
                            for (observer in mObservers) {
                                observer.onRegisterResult(false, R.string.message_register_error)
                            }
//                            callback?.onFailed(R.string.message_register_error)
                        },
                        { Log.d("TAG", "completed") }
                    )
            }
    }

    fun restorePassword(email: String) {
        disposable = ApiUtils.apiUser
            .restorePassword(email)
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    for (observer in mObservers) {
                        observer.onRestorePasswordResult(true, R.string.message_email_sended)
                    }
                },
                {
                    for (observer in mObservers) {
                        observer.onRestorePasswordResult(false, R.string.message_error_send_email)
                    }
                },
                {
                    Log.d("TAG", "completed")
                    disposable.dispose()
                }
            )
    }

    private fun getResult(result: UserResponse) {
        this.mEmail = ""
        this.mPassword = ""

        val token = result.access_token
        val user = result.user

        ApiUtils.token = token

        SharedPreferences(context!!).save(R.string.key_token, token)

        NotificationsController.subscribeToNotifications(user)

        for (observer in mObservers) {
            observer.onLoggedIn(user)
        }
    }

    override fun onCleared() {
        super.onCleared()

        if (::disposable.isInitialized && !disposable.isDisposed)
            disposable.dispose()
//        callback = null
        context = null
        mObservers.clear()
    }

    //region Facebook callbacks
    override fun onSuccess(result: LoginResult?) {
        var email = ""
        var name = ""

        GraphRequest.GraphJSONObjectCallback { `object`, _ ->
            email = `object`.getString("email")
            name = `object`.getString("name")
        }

        if (result != null) {
            disposable = ApiUtils.apiService
                .loginWS(
                    email,
                    "",
                    context!!.getString(R.string.device_id, name),
                    context!!.getString(R.string.grant_type_social),
                    access_token = result.accessToken.token
                )
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { mResult -> getResult(mResult.data!!) },
                    { error ->
                        if (error is HttpException) {
                            Log.e(TAG, error.message!!)
                        }
//                        callback?.onFailed(R.string.message_facebook_error)
                        for (observer in mObservers) {
                            observer.onLoginFailed(R.string.message_facebook_error)
                        }
                    },
                    { Log.d("TAG", "completed") }
                )
        } else {
            for (observer in mObservers) {
                observer.onLoginFailed(R.string.message_facebook_error)
            }
//            callback?.onFailed(R.string.message_facebook_error)
        }
    }

    override fun onCancel() {
        Handler().postDelayed({
            for (observer in mObservers) {
                observer.onLoginFailed(null)
            }
        }, 300)
    }

    override fun onError(error: FacebookException?) {
        for (observer in mObservers) {
            observer.onLoginFailed(R.string.message_facebook_error)
        }
//        callback?.onFailed(R.string.message_facebook_error)
    }

    fun addObserver(observer: ILoginObserver) {
        mObservers.add(observer)
    }

    fun deleteObserver(observer: ILoginObserver) {
        mObservers.remove(observer)
    }

    fun setEmail(email: String) {
        this.mEmail = email
    }

    fun getEmail(): String = this.mEmail

    fun setPassword(password: String) {
        this.mPassword = password
    }

    fun getPassword(): String = this.mPassword

    fun getFacebookCallback(): CallbackManager = callbackManager
    //endregion Facebook callbacks

    interface ILoginObserver {
        fun onLoggedIn(user: User)
        fun goToWalkthrough()
        fun onLoginFailed(message: Int?)
        fun onRegisterResult(isSuccess: Boolean, message: Int? = null)
        fun onRestorePasswordResult(isSuccess: Boolean, message: Int)
    }

}