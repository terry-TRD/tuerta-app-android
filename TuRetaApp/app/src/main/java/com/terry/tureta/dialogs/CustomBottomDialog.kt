package com.terry.tureta.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.terry.tureta.R
import com.terry.tureta.databinding.DialogCustomBottomBinding
import com.terry.tureta.mediumMargin
import com.terry.tureta.sectionEstablishments.viewModel.EstablishmentViewModel
import com.terry.tureta.sectionTournaments.viewModel.TournamentViewModel
import com.terry.tureta.sectionMain.viewModel.MainViewModel
import com.terry.tureta.smallMargin
import grid.apps.supplies_admin.Extensions.largeBold
import grid.apps.supplies_admin.Extensions.mediumRegular
import grid.apps.supplies_admin.Extensions.setMargins

class CustomBottomDialog : BottomSheetDialogFragment() {

    companion object {
        const val MODE_LOGOUT = "logout"
        const val MODE_PROFILE_RESULT = "profile_result"
        const val MODE_DELETE_FIELD = "delete_field"
        const val MODE_DELETE_TOURNAMENT = "delete_tournament"
    }

    private lateinit var binding: DialogCustomBottomBinding
    private val args: CustomBottomDialogArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //assign style with transparent background
        setStyle(STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DialogCustomBottomBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.textTitle.largeBold(R.color.black)
        binding.textTitle.setMargins(mediumMargin, mediumMargin, mediumMargin)

        binding.textMessage.mediumRegular(R.color.black)
        binding.textMessage.setMargins(mediumMargin, smallMargin, mediumMargin)

        binding.imageView.setMargins(0, smallMargin)

        binding.buttonCancel.setMargins(0, 0, 0, mediumMargin)

        binding.cardBackground.radius = smallMargin.toFloat()
        binding.buttonAccept.updateText(R.string.label_accept)

        when (args.mode) {
            MODE_LOGOUT -> {
                binding.buttonAccept.setMargins(mediumMargin, mediumMargin, mediumMargin)
                binding.textTitle.setText(R.string.label_log_out)
                binding.textMessage.setText(R.string.label_log_out_confirm)
                binding.buttonCancel.updateText(R.string.label_cancel)
                binding.imageView.setImageDrawable(
                    ContextCompat.getDrawable(requireContext(), R.drawable.ic_whistle)
                )
                binding.buttonAccept.setOnClickListener {
                    val action = CustomBottomDialogDirections.actionCustomBottomDialogToLoadDialog()
                    action.message = getString(R.string.label_logging_out)
                    findNavController().navigate(action)

                    ViewModelProvider(requireActivity())[MainViewModel::class.java].logOut()
                }
                binding.buttonCancel.setOnClickListener { findNavController().navigate(R.id.action_customBottomDialog_to_profileFragment) }
            }
            MODE_PROFILE_RESULT -> {
                binding.buttonCancel.visibility = View.GONE
                binding.textTitle.setText(R.string.label_profile_updated)
                binding.textMessage.setText(R.string.label_profile_updated_message)
                binding.buttonAccept.setMargins(
                    mediumMargin,
                    mediumMargin,
                    mediumMargin,
                    mediumMargin
                )
                binding.buttonAccept.setOnClickListener { findNavController().navigate(R.id.action_customBottomDialog_to_profileFragment) }
                binding.imageView.setImageDrawable(
                    ContextCompat.getDrawable(requireContext(), R.drawable.ic_successful)
                )
            }
            MODE_DELETE_FIELD -> {
                binding.buttonAccept.setMargins(mediumMargin, mediumMargin, mediumMargin)
                binding.textTitle.setText(R.string.label_delete_field)
                binding.textMessage.setText(R.string.label_delete_field_confirm)
                binding.buttonCancel.updateText(R.string.label_cancel)
                binding.imageView.setImageDrawable(
                    ContextCompat.getDrawable(requireContext(), R.drawable.ic_whistle)
                )
                binding.buttonAccept.setOnClickListener {
                    val action = CustomBottomDialogDirections.actionCustomBottomDialogToLoadDialog()
                    action.message = getString(R.string.label_deleting_field)
                    findNavController().navigate(action)

                    ViewModelProvider(requireActivity())[EstablishmentViewModel::class.java].deleteField()
                }
                binding.buttonCancel.setOnClickListener {
                    findNavController().navigate(R.id.action_customBottomDialog_to_fieldFragment)
                }
            }
            MODE_DELETE_TOURNAMENT -> {
                binding.buttonAccept.setMargins(mediumMargin, mediumMargin, mediumMargin)
                binding.textTitle.setText(R.string.label_delete_tournament)
                binding.textMessage.setText(R.string.label_delete_tournament_confirm)
                binding.buttonCancel.updateText(R.string.label_cancel)
                binding.imageView.setImageDrawable(
                    ContextCompat.getDrawable(requireContext(), R.drawable.ic_whistle)
                )
                binding.buttonAccept.setOnClickListener {
                    val action = CustomBottomDialogDirections.actionCustomBottomDialogToLoadDialog()
                    action.message = getString(R.string.label_deleting_tournament)
                    findNavController().navigate(action)

                    ViewModelProvider(requireActivity())[TournamentViewModel::class.java].deleteTournament()
                }
                binding.buttonCancel.setOnClickListener {
                    findNavController().navigate(R.id.action_customBottomDialog_to_tournamentFragment)
                }
            }
        }
    }

}