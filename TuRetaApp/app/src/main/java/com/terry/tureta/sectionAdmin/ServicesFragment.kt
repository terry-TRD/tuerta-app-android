package com.terry.tureta.sectionAdmin

import android.graphics.Rect
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.terry.tureta.api.ApiUtils
import com.terry.tureta.R
import com.terry.tureta.customViews.CustomFragment
import com.terry.tureta.databinding.FragmentServicesBinding
import com.terry.tureta.mediumMargin
import com.terry.tureta.model.Establishment
import com.terry.tureta.sectionEstablishments.adapters.ServicesAdapter
import com.terry.tureta.sectionMain.viewModel.MainViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class ServicesFragment : CustomFragment(), ServicesAdapter.IServicesAdapter {

    private val list: ArrayList<Establishment.Services> = ArrayList()
    private var disposable: Disposable? = null
    private lateinit var adapter: ServicesAdapter

    private lateinit var binding: FragmentServicesBinding

    private val mViewModel by lazy {
        activity?.run {
            ViewModelProvider(this)[MainViewModel::class.java]
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentServicesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter =
            ServicesAdapter(
                requireContext(),
                list,
                true,
                mViewModel!!.getUser().getEstablishment().id,
                this
            )
        binding.recyclerServices.adapter = adapter
        binding.recyclerServices.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerServices.addItemDecoration(
            object : RecyclerView.ItemDecoration() {
                override fun getItemOffsets(
                    outRect: Rect,
                    view: View,
                    parent: RecyclerView,
                    state: RecyclerView.State
                ) {
                    val position = parent.getChildAdapterPosition(view)
                    val size = parent.adapter!!.itemCount

                    outRect.run {
                        if (position == 0) {
                            top = mediumMargin
                        }

                        if (position == size - 1) {
                            bottom = mediumMargin
                        }
                    }
                }
            }
        )
    }

    override fun onResume() {
        super.onResume()

        if (list.size == 0) {
            disposable = ApiUtils.apiService
                .getServices()
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { result ->
                        updateList(result.data!!)
                        disposable!!.dispose()
                    },
                    { error ->
                        Log.e("ERROR", error.message!!)
                        Toast.makeText(context, R.string.message_services_error, Toast.LENGTH_LONG)
                            .show()
                        disposable!!.dispose()
                    },
                    { Log.d("TAG", "completed") }
                )
        }
    }

    private fun updateList(list: ArrayList<Establishment.Services>) {
        val establishment = mViewModel!!.getUser().getEstablishment()
        for (service: Establishment.Services in establishment.services) {
            for (mService: Establishment.Services in list) {
                if (service.id == mService.id) {
                    mService.selected = true
                    break
                }
            }
        }

        this.list.addAll(list)
        adapter.notifyDataSetChanged()
    }

    override fun onServiceEnabled(isEnable: Boolean, service: Establishment.Services) {
        val establishment = mViewModel!!.getUser().getEstablishment()
        if(isEnable){
            establishment.services.add(service)
        } else {
            establishment.services.removeAll { it.id == service.id }
        }
//        for (mService: Establishment.Services in establishment.services) {
//            if (mService.id == service.id) {
//                mService.selected = isEnable
//                break
//            }
//        }
    }

}