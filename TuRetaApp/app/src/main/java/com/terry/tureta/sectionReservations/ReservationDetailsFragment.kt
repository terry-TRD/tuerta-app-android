package com.terry.tureta.sectionReservations

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.squareup.picasso.Picasso
import com.terry.tureta.R
import com.terry.tureta.databinding.FragmentReservationDetailsBinding
import com.terry.tureta.mediumMargin
import com.terry.tureta.model.Reservation
import com.terry.tureta.poppinsRegular
import com.terry.tureta.sectionReservations.viewModel.ReservationsViewModel
import com.terry.tureta.smallMargin
import grid.apps.supplies_admin.Extensions.largeBold
import grid.apps.supplies_admin.Extensions.setMargins
import grid.apps.supplies_admin.Extensions.smallRegular

class ReservationDetailsFragment : BottomSheetDialogFragment(),
    ReservationsViewModel.IReservationsObserver {

    private lateinit var binding: FragmentReservationDetailsBinding
    private var newRate = 1

    private val mViewModel by lazy {
        activity?.run {
            ViewModelProvider(this)[ReservationsViewModel::class.java]
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //assign style with transparent background
        setStyle(STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentReservationDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.textFieldName.setMargins(mediumMargin, mediumMargin, mediumMargin)
        binding.textFieldName.largeBold(R.color.black)

        binding.cardBackground.radius = smallMargin.toFloat()

        binding.star1.setMargins(mediumMargin)

        binding.cardImage.setMargins(mediumMargin, smallMargin, mediumMargin)
        binding.cardImage.radius = smallMargin.toFloat()

        binding.labelLocation.setMargins(0, smallMargin)
        binding.labelResume.setMargins(0, smallMargin)

        setLabels(
            0, mediumMargin,
            binding.labelHourCost,
            binding.labelTotalHours,
            binding.labelTotal
        )

        setLabels(
            mediumMargin, 0,
            binding.textHourCost,
            binding.textTotalHours,
            binding.textTotal
        )

        binding.buttonRate.setMargins(mediumMargin, 0, mediumMargin, mediumMargin)
        binding.buttonRate.setOnClickListener { saveReview() }

        binding.layoutComments.setMargins(mediumMargin, mediumMargin, mediumMargin, mediumMargin)
        binding.layoutComments.typeface = poppinsRegular
        binding.textComments.typeface = poppinsRegular

        binding.buttonStar1.setOnClickListener { onStarClicked(it) }
        binding.buttonStar2.setOnClickListener { onStarClicked(it) }
        binding.buttonStar3.setOnClickListener { onStarClicked(it) }
        binding.buttonStar4.setOnClickListener { onStarClicked(it) }
        binding.buttonStar5.setOnClickListener { onStarClicked(it) }
    }

    override fun onResume() {
        super.onResume()

        mViewModel ?: return

        mViewModel!!.addObserver(this)
        val reservation = mViewModel!!.getReservation()

        if (mViewModel!!.isOwner())
            reservation.establishment = mViewModel!!.getUser().getEstablishment()

        binding.textFieldName.text = reservation.establishment.name
        setRating(reservation.establishment.rate)

        if (reservation.establishment.image.isNotEmpty())
            Picasso.get().load(reservation.establishment.image).into(binding.imageField)

        binding.labelLocation.updateText(reservation.establishment.address)
        binding.labelDate.updateText(reservation.getDate(requireContext()))
        binding.labelHour.updateText(reservation.getHours())
        binding.labelFieldType.updateText(reservation.getGrassType(requireContext()))
        val text = reservation.gameType
        binding.labelPlayers.updateText("$text jugadores")

        validateManual(reservation)

        validateStatus(reservation)
    }

    override fun onPause() {
        super.onPause()

        mViewModel?.deleteObserver(this)
    }

    private fun validateManual(reservation: Reservation){
        binding.textTotal.text = reservation.getTotalPrice()

        if(reservation.isManual()){
            binding.textHourCost.visibility = View.GONE
            binding.labelHourCost.visibility = View.GONE

            binding.textTotalHours.visibility = View.GONE
            binding.labelTotalHours.visibility = View.GONE
        } else {
            binding.textHourCost.text = reservation.getHourPrice()
            binding.textTotalHours.text = reservation.getHoursNumber()
        }
    }

    private fun setLabels(
        rightMargin: Int, bottomMargin: Int, vararg textViews: AppCompatTextView
    ) {
        for (textView in textViews) {
            textView.setMargins(mediumMargin, smallMargin, rightMargin, bottomMargin)
            textView.smallRegular(R.color.black)
        }
    }

    private fun setRating(rate: Int) {
        val gray = ContextCompat.getColor(requireContext(), R.color.grayText)
        val green = ContextCompat.getColor(requireContext(), R.color.accent)
        when (rate) {
            0 -> {
                binding.star1.setColorFilter(gray)
                binding.star2.setColorFilter(gray)
                binding.star3.setColorFilter(gray)
                binding.star4.setColorFilter(gray)
                binding.star5.setColorFilter(gray)
            }
            1 -> {
                binding.star1.setColorFilter(green)
                binding.star2.setColorFilter(gray)
                binding.star3.setColorFilter(gray)
                binding.star4.setColorFilter(gray)
                binding.star5.setColorFilter(gray)
            }
            2 -> {
                binding.star1.setColorFilter(green)
                binding.star2.setColorFilter(green)
                binding.star3.setColorFilter(gray)
                binding.star4.setColorFilter(gray)
                binding.star5.setColorFilter(gray)
            }
            3 -> {
                binding.star1.setColorFilter(green)
                binding.star2.setColorFilter(green)
                binding.star3.setColorFilter(green)
                binding.star4.setColorFilter(gray)
                binding.star5.setColorFilter(gray)
            }
            4 -> {
                binding.star1.setColorFilter(green)
                binding.star2.setColorFilter(green)
                binding.star3.setColorFilter(green)
                binding.star4.setColorFilter(green)
                binding.star5.setColorFilter(gray)
            }
            5 -> {
                binding.star1.setColorFilter(green)
                binding.star2.setColorFilter(green)
                binding.star3.setColorFilter(green)
                binding.star4.setColorFilter(green)
                binding.star5.setColorFilter(green)
            }
        }
    }

    private fun validateStatus(reservation: Reservation) {
        binding.motionLayout.getTransition(R.id.transition_reservation).setEnable(false)
        when (reservation.status) {
            1 -> {
                if (mViewModel!!.isOwner()) {
                    binding.buttonGood.updateText(R.string.label_accept)
                    binding.buttonGood.setMargins(mediumMargin, 0, smallMargin, mediumMargin)
                    binding.buttonGood.setOnClickListener { approveReservation() }

                    binding.buttonBad.updateText(R.string.label_reject)
                    binding.buttonBad.setMargins(smallMargin, 0, mediumMargin, mediumMargin)
                    binding.buttonBad.setOnClickListener { rejectReservation() }
                } else {
                    binding.buttonBad.updateText(R.string.label_cancel)
                    binding.buttonBad.setMargins(mediumMargin, 0, mediumMargin, mediumMargin)
                    binding.buttonBad.setOnClickListener { cancelReservation() }

                    binding.buttonGood.visibility = View.GONE
                }
            }
            2 -> {
                binding.buttonGood.visibility = View.GONE

                if (mViewModel!!.isOwner()) {
                    binding.buttonBad.updateText(R.string.label_cancel)
                    binding.buttonBad.setMargins(mediumMargin, 0, mediumMargin, mediumMargin)
                    binding.buttonBad.setOnClickListener { cancelReservation() }
                } else {
                    binding.buttonBad.visibility = View.GONE
                }
            }
            5 -> {
                binding.buttonBad.visibility = View.GONE

                val review = reservation.getReview(mViewModel!!.getUser().id)
                if (review != null) {
                    binding.buttonGood.visibility = View.GONE

                    binding.labelRate.visibility = View.VISIBLE
                    binding.labelRate.largeBold(R.color.black)
                    binding.labelRate.setMargins(mediumMargin)

                    setMyRating(review.rate)
                } else {
                    binding.labelRate1.setMargins(mediumMargin)
                    binding.labelRate1.largeBold(R.color.black)

                    binding.motionLayout.getTransition(R.id.transition_reservation).setEnable(true)
                    binding.buttonGood.updateText(R.string.label_go_rate)
                    binding.buttonGood.setMargins(mediumMargin, 0, mediumMargin, mediumMargin)
                    binding.buttonGood.setOnClickListener {
                        binding.textComments.isFocusable = true
                        binding.textComments.isFocusableInTouchMode = true
                        binding.motionLayout.transitionToEnd()
                    }
                }
            }
            3, 4, 6, 7 -> {
                binding.buttonGood.visibility = View.GONE
                binding.buttonBad.visibility = View.GONE
            }
        }
    }

    private fun setMyRating(rate: Int) {
        binding.reviewStar1.visibility = View.VISIBLE
        binding.reviewStar2.visibility = View.VISIBLE
        binding.reviewStar3.visibility = View.VISIBLE
        binding.reviewStar4.visibility = View.VISIBLE
        binding.reviewStar5.visibility = View.VISIBLE

        binding.reviewStar1.setMargins(0, 0, 0, mediumMargin)

        val gray = ContextCompat.getColor(requireContext(), R.color.grayText)
        val green = ContextCompat.getColor(requireContext(), R.color.accent)

        when (rate) {
            0 -> {
                binding.reviewStar1.setColorFilter(gray)
                binding.reviewStar2.setColorFilter(gray)
                binding.reviewStar3.setColorFilter(gray)
                binding.reviewStar4.setColorFilter(gray)
                binding.reviewStar5.setColorFilter(gray)
            }
            1 -> {
                binding.reviewStar1.setColorFilter(green)
                binding.reviewStar2.setColorFilter(gray)
                binding.reviewStar3.setColorFilter(gray)
                binding.reviewStar4.setColorFilter(gray)
                binding.reviewStar5.setColorFilter(gray)
            }
            2 -> {
                binding.reviewStar1.setColorFilter(green)
                binding.reviewStar2.setColorFilter(green)
                binding.reviewStar3.setColorFilter(gray)
                binding.reviewStar4.setColorFilter(gray)
                binding.reviewStar5.setColorFilter(gray)
            }
            3 -> {
                binding.reviewStar1.setColorFilter(green)
                binding.reviewStar2.setColorFilter(green)
                binding.reviewStar3.setColorFilter(green)
                binding.reviewStar4.setColorFilter(gray)
                binding.reviewStar5.setColorFilter(gray)
            }
            4 -> {
                binding.reviewStar1.setColorFilter(green)
                binding.reviewStar2.setColorFilter(green)
                binding.reviewStar3.setColorFilter(green)
                binding.reviewStar4.setColorFilter(green)
                binding.reviewStar5.setColorFilter(gray)
            }
            5 -> {
                binding.reviewStar1.setColorFilter(green)
                binding.reviewStar2.setColorFilter(green)
                binding.reviewStar3.setColorFilter(green)
                binding.reviewStar4.setColorFilter(green)
                binding.reviewStar5.setColorFilter(green)
            }
        }
    }

    private fun approveReservation() {
        openLoadDialog(getString(R.string.label_approving_reservation))

        mViewModel?.approveReservation()
    }

    private fun rejectReservation() {
        openLoadDialog(getString(R.string.label_rejecting_reservation))

        mViewModel?.rejectReservation()
    }

    private fun cancelReservation() {
        openLoadDialog(getString(R.string.label_canceling_reservation))

        mViewModel?.cancelReservation()
    }

    private fun saveReview() {
        openLoadDialog(getString(R.string.label_saving_review))

        mViewModel?.saveReview(newRate)
    }

    private fun openLoadDialog(message: String) {
        val action =
            ReservationDetailsFragmentDirections.actionReservationDetailsFragmentToLoadDialog()
        action.message = message
        findNavController().navigate(action)
    }

    private fun onConfirmedClicked() {
//        val id = if (user.establishments.size > 0) user.establishments[0].id
//        else reservation.establishment.id
//
//        updateButtons(false)
//        updateReservation(
//            id,
//            reservation.id,
//            APPROVE_RESERVATION,
//            APPROVE_CODE
//        )
    }

    private fun onRejectClicked() {
//        val id = if (user.establishments.size > 0) user.establishments[0].id
//        else reservation.establishment.id
//
//        updateButtons(false)
//        updateReservation(
//            id,
//            reservation.id,
//            REJECT_RESERVATION,
//            REJECT_CODE
//        )
    }

    private fun updateButtons(enabled: Boolean) {
//        view!!.buttonConfirm.isEnabled = enabled
//        view!!.buttonCancel.isEnabled = enabled
    }

    override fun onReservationsSuccess(list: List<Reservation>) {}

    override fun onReservationsFailed(message: Int) {}

    override fun onReservationUpdated(isSuccessful: Boolean, message: Int, isOwner: Boolean) {}

    override fun onFilterReservations(text: String) {}

    override fun onUpdateFilterText(text: String) {}

    override fun onReservationResult(isSuccessful: Boolean) {}

//    override fun onSuccess(any: Any, code: Int) {
//        updateButtons(true)
//        when (code) {
//            APPROVE_CODE -> {
//                reservation.status = 2
//            }
//            REJECT_CODE -> {
//                reservation.status = 3
//            }
//            SAVE_REVIEW -> {
//                Toast.makeText(context!!, R.string.message_review_sended, Toast.LENGTH_LONG).show()
//                view!!.layoutRate.visibility = View.GONE
//            }
//        }
//        callbackMain.onReservationUpdated()
//    }

//    override fun onFailure(message: String?, code: Int) {
//        updateButtons(true)
//        when (code) {
//            APPROVE_CODE -> {
//
//            }
//            REJECT_CODE -> {
//
//            }
//            SAVE_REVIEW -> {
//                Toast.makeText(context!!, message, Toast.LENGTH_LONG).show()
//            }
//        }
//    }

    private fun onStarClicked(it: View) {
        val gray = ContextCompat.getColor(requireContext(), R.color.grayText)
        val green = ContextCompat.getColor(requireContext(), R.color.accent)
        when (it.id) {
            R.id.buttonStar1 -> {
                newRate = 1
                binding.buttonStar1.setColorFilter(green)
                binding.buttonStar2.setColorFilter(gray)
                binding.buttonStar3.setColorFilter(gray)
                binding.buttonStar4.setColorFilter(gray)
                binding.buttonStar5.setColorFilter(gray)
            }
            R.id.buttonStar2 -> {
                newRate = 2
                binding.buttonStar1.setColorFilter(green)
                binding.buttonStar2.setColorFilter(green)
                binding.buttonStar3.setColorFilter(gray)
                binding.buttonStar4.setColorFilter(gray)
                binding.buttonStar5.setColorFilter(gray)
            }
            R.id.buttonStar3 -> {
                newRate = 3
                binding.buttonStar1.setColorFilter(green)
                binding.buttonStar2.setColorFilter(green)
                binding.buttonStar3.setColorFilter(green)
                binding.buttonStar4.setColorFilter(gray)
                binding.buttonStar5.setColorFilter(gray)
            }
            R.id.buttonStar4 -> {
                newRate = 4
                binding.buttonStar1.setColorFilter(green)
                binding.buttonStar2.setColorFilter(green)
                binding.buttonStar3.setColorFilter(green)
                binding.buttonStar4.setColorFilter(green)
                binding.buttonStar5.setColorFilter(gray)
            }
            R.id.buttonStar5 -> {
                newRate = 5
                binding.buttonStar1.setColorFilter(green)
                binding.buttonStar2.setColorFilter(green)
                binding.buttonStar3.setColorFilter(green)
                binding.buttonStar4.setColorFilter(green)
                binding.buttonStar5.setColorFilter(green)
            }
        }
    }

}
