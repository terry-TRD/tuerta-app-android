package com.terry.tureta.sectionEstablishments.adapters

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.terry.tureta.R
import com.terry.tureta.sectionEstablishments.EstablishmentInfoFragment
import com.terry.tureta.sectionReservations.ReservationsListFragment
import com.terry.tureta.sectionTournaments.TournamentsListFragment

class EstablishmentFragmentsAdapter(private val context: Context, manager: FragmentManager) :
    FragmentStatePagerAdapter(
        manager,
        BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
    ) {

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> context.getString(R.string.label_info)
            else -> context.getString(R.string.label_tournaments_and_statistics)
        }
    }

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> EstablishmentInfoFragment()
            else -> TournamentsListFragment()
        }
    }

    override fun getCount(): Int {
        return 2
    }

}