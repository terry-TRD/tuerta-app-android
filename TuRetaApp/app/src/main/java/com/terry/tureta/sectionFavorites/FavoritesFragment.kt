package com.terry.tureta.sectionFavorites

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.terry.tureta.api.AsyncResult

import com.terry.tureta.R
import com.terry.tureta.customViews.CustomFragment
import com.terry.tureta.databinding.FragmentFavoritesBinding
import com.terry.tureta.model.Establishment
import com.terry.tureta.sectionEstablishments.viewModel.EstablishmentViewModel
import com.terry.tureta.adapters.EstablishmentsAdapter
import com.terry.tureta.sectionMain.viewModel.MainViewModel
import grid.apps.supplies_admin.Extensions.largeTitleStyle
import grid.apps.supplies_admin.Extensions.setToolbar
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.fragment_favorites.view.*

class FavoritesFragment : CustomFragment(), EstablishmentsAdapter.IEstablishmentsAdapter,
    AsyncResult {

    private val list = ArrayList<Establishment>()
    private val adapter by lazy {
        EstablishmentsAdapter(
            requireContext(),
            list,
            this,
            EstablishmentsAdapter.STYLE_FAVORITE
        )
    }

    private val mViewModel by lazy {
        activity?.run {
            val model = ViewModelProvider(this)[MainViewModel::class.java]
            model.setActivity(this)
            model
        }
    }

    private var favoritesDisposable: Disposable? = null

    private lateinit var binding: FragmentFavoritesBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentFavoritesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.recycler.layoutManager = LinearLayoutManager(context)
        view.recycler.adapter = adapter

        view.textMessage.largeTitleStyle()

//        favoritesDisposable = CourtsController.getFavorites(idUser, FAVORITES_CODE, this)
        binding.toolbar.setToolbar(background = R.color.primaryDark)
    }

    override fun onResume() {
        super.onResume()

        mViewModel?.addObserver(this)
        mViewModel?.getFavorites()
    }

    override fun onPause() {
        super.onPause()

        mViewModel?.deleteObserver(this)
    }

    override fun onEstablishmentSelected(establishment: Establishment) {
        ViewModelProvider(requireActivity())[EstablishmentViewModel::class.java].setEstablishment(
            establishment
        )
        findNavController().navigate(R.id.action_favoritesFragment_to_establishmentFragment)
    }

    override fun onFavoritesResult(list: List<Establishment>?) {
        if (list != null) {
            this.list.clear()
            this.list.addAll(list)
            adapter.notifyDataSetChanged()

            if (this.list.isEmpty()) {
                binding.imageView.visibility = View.VISIBLE
                binding.textMessage.visibility = View.VISIBLE
            }

        } else {
            showToast(R.string.message_favorites_error)
        }
    }

    override fun onSuccess(any: Any, code: Int) {
        list.clear()
        list.addAll(any as ArrayList<Establishment>)
        adapter.notifyDataSetChanged()
    }

    override fun onFailure(message: String?, code: Int) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    override fun onDestroy() {
        super.onDestroy()

        if (favoritesDisposable != null) favoritesDisposable!!.dispose()
    }

}
