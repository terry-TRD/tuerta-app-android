package com.terry.tureta.sectionFields

import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SwitchCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.terry.tureta.R
import com.terry.tureta.customViews.CustomFragment
import com.terry.tureta.databinding.FragmentAdminMainBinding
import com.terry.tureta.mediumMargin
import com.terry.tureta.model.Field
import com.terry.tureta.sectionFields.adapters.FieldsAdapter
import com.terry.tureta.sectionEstablishments.viewModel.EstablishmentViewModel
import com.terry.tureta.sectionMain.viewModel.MainViewModel
import grid.apps.supplies_admin.Extensions.largeTitleStyle
import grid.apps.supplies_admin.Extensions.setMargins

class FieldsListFragment : CustomFragment(), FieldsAdapter.IFieldsAdapter {

    private lateinit var binding: FragmentAdminMainBinding

    private val list by lazy { ArrayList<Field>() }
    private val adapter by lazy {
        FieldsAdapter(
            requireContext(),
            list,
            this
        )
    }

    private val mViewModel by lazy {
        activity?.run {
            ViewModelProvider(this)[EstablishmentViewModel::class.java]
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentAdminMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recyclerAdmin.adapter = adapter
        binding.recyclerAdmin.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerAdmin.addItemDecoration(object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(
                outRect: Rect,
                view: View,
                parent: RecyclerView,
                state: RecyclerView.State
            ) {
                val position = parent.getChildAdapterPosition(view)
                val size = parent.adapter!!.itemCount

                var top = 0

                if (position == 0) {
                    top = mediumMargin
                }

                outRect.run {
                    left = mediumMargin
                    this.top = top
                    right = mediumMargin
                    bottom = mediumMargin
                }
            }
        })

        binding.buttonCreateField.setOnClickListener {
            mViewModel?.setField(null)
            findNavController().navigate(R.id.action_adminMainFragment_to_fieldFragment)
        }
        binding.buttonCreateField.setMargins(mediumMargin, 0, mediumMargin, mediumMargin)

        binding.textMessage.largeTitleStyle()
        binding.textMessage.setMargins(mediumMargin, 0, mediumMargin)
    }

    override fun onResume() {
        super.onResume()

        val user = ViewModelProvider(requireActivity())[MainViewModel::class.java].getUser()
        mViewModel?.setEstablishment(user.getEstablishment())
        setList(user.getEstablishment().fields)
    }

    private fun setList(list: List<Field>) {
        this.list.clear()
        this.list.addAll(list)
        this.adapter.notifyDataSetChanged()

        if (this.list.isEmpty()) {
            binding.buttonCreateField.visibility = View.VISIBLE
            binding.imageView.visibility = View.VISIBLE
            binding.textMessage.visibility = View.VISIBLE
        }
    }

    override fun onFieldSelected(field: Field) {
        mViewModel?.setField(field)
        findNavController().navigate(R.id.action_adminMainFragment_to_fieldFragment)
    }

    override fun onFieldEnabled(field: Field, isEnabled: Boolean, switch: SwitchCompat) {
        mViewModel?.updateField(field, isActive = isEnabled, switch = switch)
    }

}
