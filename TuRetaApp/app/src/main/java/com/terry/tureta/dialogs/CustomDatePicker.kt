package com.terry.tureta.dialogs

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.widget.DatePicker
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.terry.tureta.model.Field
import com.terry.tureta.sectionEstablishments.viewModel.EstablishmentViewModel
import com.terry.tureta.sectionTournaments.viewModel.TournamentViewModel
import java.util.*

class CustomDatePicker : DialogFragment(), DatePickerDialog.OnDateSetListener,
    EstablishmentViewModel.IEstablishmentObserver {

    companion object {
        const val MODE_SEARCH_AVAILABILITY = 100
        const val MODE_SELECT_DATE = 200
    }

    val args: CustomDatePickerArgs by navArgs()

    var allowClose = true

    private val mEstablishmentViewModel by lazy {
        ViewModelProvider(requireActivity())[EstablishmentViewModel::class.java]
    }

    private val mTournamentViewModel by lazy {
        ViewModelProvider(requireActivity())[TournamentViewModel::class.java]
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        if (args.mode == MODE_SEARCH_AVAILABILITY) {
            mEstablishmentViewModel.setNewDate(year, month, dayOfMonth)
            allowClose = false
        } else {
            val date = Calendar.getInstance()
            date.set(year, month, dayOfMonth)
            mTournamentViewModel.setDateSelected(date)
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val year: Int
        val month: Int
        val day: Int

        if (args.mode == MODE_SEARCH_AVAILABILITY) {
            year = mEstablishmentViewModel.getYear()
            month = mEstablishmentViewModel.getMonth()
            day = mEstablishmentViewModel.getDay()
        } else {
            if (mTournamentViewModel.getDateSelected() == null)
                mTournamentViewModel.setDateSelected(Calendar.getInstance())

            year = mTournamentViewModel.getDateSelected()!!.get(Calendar.YEAR)
            month = mTournamentViewModel.getDateSelected()!!.get(Calendar.MONTH)
            day = mTournamentViewModel.getDateSelected()!!.get(Calendar.DAY_OF_MONTH)
        }

        return object : DatePickerDialog(requireContext(), this, year, month, day) {

            override fun onBackPressed() {
                allowClose = true
                super.onBackPressed()
            }

            override fun dismiss() {
                if (allowClose) super.dismiss()
            }

            override fun onClick(dialog: DialogInterface, which: Int) {
                when (which) {
                    DialogInterface.BUTTON_POSITIVE -> allowClose = args.mode != MODE_SEARCH_AVAILABILITY
                    DialogInterface.BUTTON_NEGATIVE -> allowClose = true
                }
                super.onClick(dialog, which)
            }
        }
    }

    override fun onResume() {
        super.onResume()

        mEstablishmentViewModel.addObserver(this)
    }

    override fun onPause() {
        super.onPause()

        mEstablishmentViewModel.deleteObserver(this)
    }

    override fun onUpdateView() {}

    override fun onUpdateDates(datesFounded: Boolean) {
        allowClose = true
        findNavController().navigate(
            CustomDatePickerDirections.actionCustomDatePickerToCreateReservationFragment(
                datesFounded
            )
        )
    }

    override fun onDetailsFailed(message: Int) {}

    override fun onFavoriteFinished(isSuccessful: Boolean) {}

    override fun onFieldDeleted(isSuccessful: Boolean, field: Field?) {}

    override fun onFieldCreated(isSuccessful: Boolean, field: Field?) {}

    override fun onFieldEdited(isSuccessful: Boolean, field: Field?) {}

    override fun onFieldAvailableFailed() {}

}