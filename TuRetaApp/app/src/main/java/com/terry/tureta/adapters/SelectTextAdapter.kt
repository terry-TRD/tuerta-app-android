package com.terry.tureta.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.terry.tureta.R
import com.terry.tureta.databinding.ItemTextBinding

class SelectTextAdapter(
    private val context: Context,
    private val list: List<String>,
    private val callback: ITextAdapter,
    private var selectedPosition: Int,
    private var isLight: Boolean
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VHText(
            ItemTextBinding.inflate(
                LayoutInflater.from(context), parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as VHText).bindData(list[position], position, itemCount)
    }

    inner class VHText(private val binding: ItemTextBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindData(text: String, position: Int, size: Int) {

            if(isLight){
                binding.text.setLightMode()
            }

            binding.text.isSeparatorVisible(position != size - 1)
            binding.text.updateText(text)
            binding.text.setOnClickListener {
                val temp = selectedPosition

                selectedPosition = position
                callback.onTextClick(text, position)

                notifyItemChanged(temp)
                notifyItemChanged(selectedPosition)
            }
            if (position == selectedPosition) {
                binding.text.setEndIcon(
                    ContextCompat.getDrawable(context, R.drawable.ic_check)
                )
            } else {
                binding.text.setEndIcon(null)
            }
        }

    }

    interface ITextAdapter {
        fun onTextClick(text: String, position: Int)
    }

}